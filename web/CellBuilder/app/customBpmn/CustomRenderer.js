import BaseRenderer from 'diagram-js/lib/draw/BaseRenderer';
import {
  getLabel
} from 'bpmn-js/lib/features/label-editing/LabelUtil';

import {
    append as svgAppend,
    attr as svgAttr,
    create as svgCreate,
    classes as svgClasses,
    remove as svgRemove
  } from 'tiny-svg';

import {
  getRoundRectPath
} from 'bpmn-js/lib/draw/BpmnRenderUtil';

import { is } from 'bpmn-js/lib/util/ModelUtil';
import { isAny } from 'bpmn-js/lib/features/modeling/util/ModelingUtil';
import {
    isObject,
    assign,
    forEach,
    reduce
  } from 'min-dash';
import Text from './Text';
const HIGH_PRIORITY = 15000,
      TASK_BORDER_RADIUS = 2;


      var DEFAULT_FONT_SIZE = 12;
      var LINE_HEIGHT_RATIO = 1.2;
      
      var MIN_TEXT_ANNOTATION_HEIGHT = 30;
export default class CustomRenderer extends BaseRenderer {
  constructor(eventBus, bpmnRenderer, textRenderer, config) {
    super(eventBus, HIGH_PRIORITY);

    this.bpmnRenderer = bpmnRenderer;

    const defaultStyle = this.defaultStyle = assign({
        fontFamily: 'Fira Code',
        //fontFamily: 'Arial, sans-serif',
        fontSize: DEFAULT_FONT_SIZE,
        fontWeight: 'normal',
        lineHeight: LINE_HEIGHT_RATIO
      }, config && config.defaultStyle || {});
    
    const fontSize = parseInt(defaultStyle.fontSize, 10) - 1;

    const externalStyle = this.externalStyle = assign({}, defaultStyle, {
        fontSize: fontSize
    }, config && config.externalStyle || {});

    const textUtil = this.textUtil = new Text({
        style: defaultStyle
    });
    textRenderer.getExternalLabelBounds = function(bounds, text) {
        var layoutedDimensions = textUtil.getDimensions(text, {
          box: {
            width: bounds.width<150 ? 150 : bounds.width, //was hardcoded to 90
            height: bounds.height<60 ? 60 : bounds.height, //was hardcoded to 30
            x: bounds.width / 2 + bounds.x,
            y: bounds.height / 2 + bounds.y
          },
          style: externalStyle
        });
    
        // resize label shape to fit label text
        return {
          x: Math.round(bounds.x + bounds.width / 2 - layoutedDimensions.width / 2),
          y: Math.round(bounds.y),
          width: Math.ceil(layoutedDimensions.width),
          height: Math.ceil(layoutedDimensions.height)
        };
    
      };
  }

  canRender(element) {
    return isAny(element, [ 'bpmn:SequenceFlow','bpmn:Gateway','bpmn:IntermediateCatchEvent','bpmn:EndEvent','bpmn:StartEvent' ]) && element.labelTarget;
  }

  renderExternalLabel(parentGfx, element) {
    var box = {
      width: element.width <150 ? 150  : element.width, //was hardcoded to 90
      height: element.height <60 ? 60  : element.height, //was hardcoded to 30
      x: element.width / 2 + element.x,
      y: element.height / 2 + element.y
    };

    var svgLabel =  this._renderLabel(parentGfx, getLabel(element), {
      box: box,
      fitBox: true,
      style: assign(
        {},
        this.externalStyle,
        {
          fill: 'var(--LSH-fg)'
        }
      )
    });
    let parCount = 0;
    let braCount = 0;
    [...svgLabel.children].forEach((dom) => {
      dom.innerHTML = dom.textContent.replace(/(this->|->|\.|,)|(&&|\|\||<=|>=|>|<|==|!=|=|\+\+|--|\+|-|!)|(\w+)\(|(\w+)::|(\()|(\))|(\[)|(\])|(?<![\w_]+)(\d+\.?\d*)|(true|false)/g, (match,  acces,token,funcCall,enumT,openPar,closePar,openBra,closeBra,number,bool)=>{
        if(token)         return `¤tspan class="LSH_t"£${token}¤/tspan£`
        else if(acces)    return `¤tspan class="LSH_a"£${acces}¤/tspan£`
        else if(funcCall) return `¤tspan class="LSH_f"£${funcCall}¤/tspan£¤tspan class="LSH_p${++parCount%7}"£(¤/tspan£`
        else if(enumT)    return `¤tspan class="LSH_f"£${enumT}¤/tspan£::`
        else if(openPar)  return `¤tspan class="LSH_p${++parCount%7}"£(¤/tspan£`
        else if(closePar) return `¤tspan class="LSH_p${parCount--%7}"£)¤/tspan£`
        else if(openBra)  return `¤tspan class="LSH_p${++braCount%7}"£[¤/tspan£`
        else if(closeBra) return `¤tspan class="LSH_p${braCount--%7}"£]¤/tspan£`
        else if(number) return `¤tspan class="LSH_n"£${number}¤/tspan£`
        else if(bool) return `¤tspan class="LSH_n"£${bool}¤/tspan£`
      }).replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("¤", "<").replaceAll("£", ">")
    })
    return svgLabel
  }
  drawShape(parentNode, element) {
    return this.renderExternalLabel(parentNode,element)
  }
  _renderLabel(parentGfx, label, options) {

    options = assign({
      size: {
        width: 1000
      }
    }, options);

    var text = this.textUtil.layoutText(label || '', options).element;

    svgClasses(text).add('djs-label');

    svgAppend(parentGfx, text);

    return text;
  }
}

CustomRenderer.$inject = [ 'eventBus', 'bpmnRenderer','textRenderer','config.textRenderer' ]; 

// copied from https://github.com/bpmn-io/diagram-js/blob/master/lib/core/GraphicsFactory.js
function prependTo(newNode, parentNode, siblingNode) {
  parentNode.insertBefore(newNode, siblingNode || parentNode.firstChild);
}