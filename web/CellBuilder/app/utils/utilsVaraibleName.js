import FunctionManager from "../manager/functionManager";

let ID = -1;

function cleanString(input){
    return input.replaceAll(/^\s+|\s+$/gi, '').
    replaceAll(/è|é|ê|ë/gi, 'e')
    .replaceAll(/â|à|ä/gi, 'a')
    .replaceAll(/ï|î/gi, 'i')
    .replaceAll(/ü|û|ù/gi, 'u')
    .replaceAll(/_|\-|\s+/gi, ' ')
    .replaceAll(/@|#|~|\&|\||\{|\}|\[|\]|=|\^|\+|\*|\/|\?|\.|\;|\:|\!|,|'|\$|\(|\)/gi, '');
}

function toCamelCase(input) {
    var inputArray = cleanString(input.replaceAll(/([A-Z])/g," $1")).split(' ');
    var result = "";
    for(var i = 0 , len = inputArray.length; i < len; i++) {
      var currentStr = inputArray[i];
      var tempStr = currentStr.toLowerCase();
      if(i != 0) {
          tempStr = tempStr.substr(0, 1).toUpperCase() + tempStr.substr(1);
       }
       result +=tempStr;
    }
    return result;
}

function toSnakeCase(input){
    return cleanString(input).toUpperCase().replace(' ','_')
}


function getNewID(){
    ID+=1;
    return ID;
}

function callBackCheckIntegrity(item, newValue){
    let regex = /(?:(?<value>.+)_(?<count>\d+$))|(?<value2>.+)/;
    newValue = newValue.replace('()', '').replaceAll(/[^\w\d_]/g, '_');
    let newValInfo = newValue.match(regex).groups;
    newValInfo.value = newValInfo.value ? newValInfo.value : newValInfo.value2;
    let retValue = newValue;

    let nameExists = false;
    let copyCount = (newValInfo.count !== undefined) ? Number(newValInfo.count) - 1 : -1;
    if (newValue !== item.valueText)
        for (let other of item.parent.items) {
            if (item !== other) {
                let itemValue = other.valueText.replace('()', '');
                let itemInfo = itemValue.match(regex).groups;
                itemInfo.value = itemInfo.value ? itemInfo.value : itemInfo.value2;
                nameExists |= (itemValue === newValue);
                if (itemInfo.value === newValInfo.value) {
                    let count = (itemInfo.count == undefined) ? 0 : Number(itemInfo.count);
                    copyCount = Math.max(copyCount, count);
                }
            }
        }
    copyCount++;

    if (nameExists && copyCount > 0) {
        retValue = newValInfo.value + "_" + copyCount;
    }

    return retValue;
}

function getTitleUserFonction(idFunc) {
    let funcInfo = FunctionManager.getInfo(idFunc)
    return funcInfo.returnType + ' ' + funcInfo.functionName + '(' + funcInfo.params + ')';
}

function getTitleCoreFonction(info) {
    if (info.functionName === undefined) return undefined;
    return info.returnType + ' ' + (info.completion_prefix !== undefined ? info.completion_prefix : '') + info.functionName + '(' + info.params + ')\n' +
        (info.description ? info.description : '*No description found for this item.');
}


function isCppCodeValid(input){
    //TODO Complete rework probably
    // if(input !== '' | input !== undefined | input !== null){
    //     var analyzer = require("node-c-analyzer");
    //     var code = input.replaceAll(/(\/\/\ *)(.*)(?=\n)/g,'/*$2*/') // replace single line comments
    //     code = code.replaceAll(/[a-zA-Z0-9]*::[a-zA-Z0-9]*/g,'int') //replace types containing :: with simple int
    //     code = code.replaceAll(/([^;|{|}|(|)]*(<<|>>).*;)|(<.*>)/g, '') // remove cpp stuff
    //     return analyzer.parser.parse(analyzer.parser.lexer.lexUnit.tokenize(code)) !== null
    // }
    // else
    //     return true;
    return true;
}

export {cleanString, toCamelCase, toSnakeCase, getNewID, isCppCodeValid, callBackCheckIntegrity, getTitleUserFonction, getTitleCoreFonction}