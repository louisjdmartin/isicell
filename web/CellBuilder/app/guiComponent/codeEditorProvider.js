import * as monaco from 'monaco-editor';
import { CPP_custom, PYTHON_custom, themeDark, themeLight } from '../utils/MonacoUtils'

monaco.languages.setMonarchTokensProvider('cpp', CPP_custom)
monaco.languages.setMonarchTokensProvider('python', PYTHON_custom)

//https://github.com/brijeshb42/monaco-themes
monaco.editor.defineTheme('monokai', themeDark);
monaco.editor.defineTheme('IDLE', themeLight);
class AI_Completion {
    constructor(){
        this.apiKey = undefined
        this.id_session = ((e=4, t='hex', r=8) => {
            let n = new Uint8Array(e);
            for (let i = 0; i < e; i += 65536)
                n.set(crypto.getRandomValues(new Uint8Array(Math.min(65536, e - i))), i);
            return [...n].map(e=>e.toString(16).padStart(2, "0")).join("").slice(0, r)
        })()
        window.updateCompletionAPI = (url,apiKey)=>{this.url=url,this.apiKey=apiKey}
    }

    async getCompletion(text,cursorOffset,language){
        if(!this.url || !this.apiKey) return {completionItems:[]}
        try {
            return await (await fetch(this.url, {
                "method": "POST",
                "credentials": "same-origin",
                "redirect": "error",
                "mode": "cors",
            headers: {"Content-Type": "application/json",
                    'Authorization': `Basic ${this.apiKey}-${this.id_session}`},
            body: JSON.stringify({
                "metadata": {
                    "ideName": "playground",
                    "ideVersion": "playground",
                    "extensionName": "playground",
                    "extensionVersion": "monaco",
                    "apiKey": this.apiKey,
                    "sessionId": this.id_session
                },
                "document": {
                    text,
                    "editorLanguage": language,
                    "language": "LANGUAGE_"+language.toUpperCase(),
                    cursorOffset,
                    "lineEnding": "\n"
                },
                "editorOptions": {
                    "tabSize": "4",
                    "insertSpaces": true
                }
            })
            })).json()
        } catch {
            return {completionItems:[]}
        }
    }
}

const aic = new AI_Completion()




function backtrackValidePosition(model, position, hover=false) {
    const startCol = hover?model.getWordAtPosition(position)?.endColumn:position.column
    if(!startCol) return [false,false]
    const code = model.getValueInRange({startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: 0, endColumn: startCol})
    let sequenceWord = ''
    let pC = 0;
    let i;
    for(i=code.length-1;i>=0;i--){
        const c = code[i]
        if(c==')') pC--;
        else if(pC===0 && !/\w|\./.test(c)) break;
        else if(c=='(') pC++;
        else if(pC===0) sequenceWord = c+sequenceWord;
    }
    return [sequenceWord,{startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: i+2, endColumn: startCol}]
}
class CodeEditorProvider {

    // Instance and attributes initialisation
    constructor() {

        this.completionProvider = {} // All completions
        this.currentCompletion = {} // All available completions fetched
        this.listVariable = 0;
        this.monaco = {}; // List of the editors
        this.callBackMonolineEnter = {} // The function to execute on Enter for each editor
        this.registerCompletion();

    }


    /**
     * Registers a completion provider
     * @param {*} name Name of the completion category
     * @param {*} provider Function returning a list of {label,insertText,documentation}
     */
    addCompletionProvider(name, provider) {
        this.completionProvider[name] = provider;
    }

    /**
     * Remove a completion provider
     * @param {*} name Name of the completion category
     */
    removeCompletionProvider(name) {
        delete this.completionProvider[name];
    }
    /**
     * Affects completion categories to an editor
     * @param {*} name Name of the editor
     * @param {*} providers List of the completion providers category
     */
    setEditorCompletionProvider(name, providers) {
        this.monaco[name].providers = providers 
    }

    /**
     * Returns the content of an editor
     * @param {*} name The name of the editor
     */
    getValue(name) {
        return this.monaco[name].editor.getValue()
    }

    /**
     * Sets the value of an editor
     * @param {*} name The name of the editor
     * @param {*} value The value to set
     */
    setValue(name, value) {
        this.monaco[name].editor.setValue(value)
    }

    setDiffValue(name, newValue, oldValue,language) {
        this.monaco[name].editor.setModel({
            original: monaco.editor.createModel(oldValue,language),
            modified: monaco.editor.createModel(newValue,language),
        })
    }
    removeEditor(idContainer){
        this.monaco[idContainer].editor.dispose()
        delete this.monaco[idContainer]
    }

    /**
     * Creates a new editor
     * @param {*} idContainer The id of the container in which the editor is created
     * @param {*} options The editor options {lineNumbers:"on"/"off",monoline:true/false,providers:['prov1','prov2']}
     */
    createEditor(idContainer, options = {}) {
        this.callBackMonolineEnter[idContainer] = () => { } // Default
        var containerDom = document.getElementById(idContainer)

        // Sets the providers if passed in param
        this.monaco[idContainer] = {
            providers: options.providers ? options.providers : []
        };

        // Sets the proper theme
        document.getElementById('theme-switch').onchange = (e) => {
            if (e.target.checked) {
                localStorage.setItem('theming', 'dark')
                this.monaco[idContainer].editor.updateOptions({
                    theme: "monokai"
                });
            } else {
                localStorage.setItem('theming', 'light')
                this.monaco[idContainer].editor.updateOptions({
                    theme: "IDLE"
                });
            }
        }

        // Updates the available completion a focus of the editor
        containerDom.addEventListener('focusin', () => {
            this.updateCurrentCompletion(this.monaco[idContainer].providers)
        })

        // Applies the on Enter callback if the editor is monoline
        if (options.monoline) {
            containerDom.addEventListener("keydown", (event) => {
                if (event.key === 'Enter' && event.target.getAttribute("aria-activedescendant") === null) {
                    event.preventDefault();
                    event.stopPropagation();
                    this.callBackMonolineEnter[idContainer](this.monaco[idContainer].editor.getValue());
                }
            })
        }

        const optionsEditor = {
            value: '',
            language: options.language ? options.language : 'cpp',
            fontLigatures: true,
            readOnly: options.readOnly ? options.readOnly : false,
            fontFamily: 'Fira Code',
            minimap: {
                enabled: options.minimap ? options.minimap : false
            },
            lineNumbers: options.lineNumbers ? options.lineNumbers : 'off',
            glyphMargin: false,
            folding: false,
            fontSize: 12,
            lineHeight: options.monoline?25:18,
            padding: options.padding ? options.padding :(options.monoline ? { top: 0, bottom: 0 } : { top: 10, bottom: 10 }),
            renderWhitespace: "boundary",
            scrollBeyondLastLine: true,
            overviewRulerLanes: 0,
            overviewRulerBorder: false,
            hideCursorInOverviewRuler: true,
            automaticLayout: true,
            "bracketPairColorization.enabled": true,
            scrollBeyondLastLine: false,
            scrollbar: {
                useShadows: true,
                verticalScrollbarSize: 10,
                horizontalScrollbarSize: 10,
                vertical: options.scrollVertical?options.scrollVertical:"auto",
                horizontal: "auto",
                handleMouseWheel: true,
                alwaysConsumeMouseWheel: false
            },
            renderSideBySide: options.renderSideBySide? options.renderSideBySide:false,
            theme: (document.getElementById('theme-switch').checked) ? 'monokai' : 'IDLE'
        }

        // Creates the editor in the containerDom with a bunch a params
        if(options.diff) this.monaco[idContainer].editor = monaco.editor.createDiffEditor(containerDom, optionsEditor);
        else this.monaco[idContainer].editor = monaco.editor.create(containerDom, optionsEditor);

        if(optionsEditor.language === 'python'){
            this.monaco[idContainer].editor.addAction({
                // An unique identifier of the contributed action.
                id: "comment-multi-lines",

                // A label of the action that will be presented to the user.
                label: "comment multi lines",
                keybindings: [
                    monaco.KeyMod.CtrlCmd | monaco.KeyMod.Shift | monaco.KeyCode.Slash,
                    monaco.KeyMod.CtrlCmd | monaco.KeyCode.Slash
                ],
                precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
                run: (ed) => {
                    let range = ed.getSelection()
                    if(range.endLineNumber-range.startLineNumber>0){
                        range = {startLineNumber: range.startLineNumber,endLineNumber: range.endLineNumber+1,startColumn: 1,endColumn: 1}
                        let text = ed.getModel().getValueInRange(range)
                        if(/^\s*#/.test(text)) text = text.replaceAll(/^(\s*)(#\s?)/gm,'$1')
                        else text = '#'+text.replaceAll(/^(?!\s*\n|\s*#)/gm,'#')
                        ed.executeEdits("comment", [{identifier: { major: 1, minor: 1 }, range, text, forceMoveMarkers: true}]);
                    }
                },
            });
        }
        if(options.callbackGlobalSearch){
            this.monaco[idContainer].editor.addAction({
                // An unique identifier of the contributed action.
                id: "global-search-notebook",

                // A label of the action that will be presented to the user.
                label: "global search notebook",
                keybindings: [
                    monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyF
                ],
                precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
                run: (ed) => {
                    let range = ed.getSelection()
                    if(range.startLineNumber===range.endLineNumber)options.callbackGlobalSearch(range)
                },
            });
            containerDom.addEventListener('focusout', () => {
                this.monaco[idContainer].editor.setSelection(new monaco.Selection(0, 0, 0, 0));
            })
        }
        this.monaco[idContainer].editor.addAction({
            // An unique identifier of the contributed action.
            id: "code-completion",

            // A label of the action that will be presented to the user.
            label: "code completion",
            keybindings: [
                monaco.KeyMod.Alt | monaco.KeyCode.KeyC
            ],
            precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
            run: (ed) => {
                let range = ed.getSelection()
                if(range.startLineNumber===range.endLineNumber) this.monaco[idContainer].editor.trigger('', 'editor.action.inlineSuggest.trigger', '');
            },
        });

        return this.monaco[idContainer].editor
    }

    /**
     * Registers the proper completion to the monaco editors using currentCompletion
     */
    registerCompletion() {
        if (Object.keys(this.monaco).length === 0) {
            monaco.languages.registerInlineCompletionsProvider('cpp',{
                freeInlineCompletions: ()=>{},
                provideInlineCompletions: async (document, position,context) => {
                    if(context.triggerKind==0) return {items:[]}
                    const offset = document.getValueLengthInRange({startLineNumber: 0,endLineNumber: position.lineNumber,startColumn: 0,endColumn: position.column})
                    const completions = (await aic.getCompletion(document.getValue(),offset,'cpp')).completionItems || []
                    const final = {items:completions.map(c=>({ insertText: c.completion.text.substring(position.column-1) }))}
                    console.log(final)
                    return final
                },
            })
            monaco.languages.registerCompletionItemProvider('cpp', {
                provideCompletionItems: (model, position) => {
                    const backtrackValidePosition = (start) => {
                        let range = {
                            startLineNumber: position.lineNumber,
                            endLineNumber: position.lineNumber,
                            startColumn: start - 2,
                            endColumn: start
                        };
                        let newPosition = { lineNumber: position.lineNumber, column: start }
                        let token = model.getValueInRange(range);
                        if (token[1] === '.') newPosition.column = newPosition.column - 1
                        if (token === '->' || token === '::') newPosition.column = newPosition.column - 2
                        return newPosition
                    }

                    let wordEnd = model.getWordUntilPosition(position);
                    let startColumn = wordEnd.startColumn;
                    let newPosition = backtrackValidePosition(startColumn)

                    let wordFirst = wordEnd;
                    while (newPosition.column !== startColumn) {
                        wordFirst = model.getWordUntilPosition(newPosition)
                        startColumn = wordFirst.startColumn;
                        newPosition = backtrackValidePosition(startColumn)
                    }
                    const range = {
                        startLineNumber: position.lineNumber,
                        endLineNumber: position.lineNumber,
                        startColumn: startColumn,
                        endColumn: position.column
                    };
                    let suggestions = wordFirst.word?this.currentCompletion.filter(w => w.insertText.startsWith(wordFirst.word)):[]

                    if (wordFirst === wordEnd)
                        suggestions = suggestions.map(w => { let w2 = { ...w }; Object.keys(w2).forEach(k => { let t = (typeof w2[k]==='string')?w2[k].match(/(.*?(\.|::|\->))/):null; w2[k]=t?t[0]:t  }); return w2; }).filter((v, i, a) => a.findIndex(v2=>v2.insertText === v.insertText) === i);
                    
                    var suggest = {
                        suggestions: suggestions.map((item) => {
                            item.range = range
                            item.insertTextRules = monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet
                            if(item.insertText.endsWith(')'))
                                item.kind = 3
                            else if (item.insertText.endsWith('::'))
                                item.kind = monaco.languages.CompletionItemKind.Enum
                            else if (item.insertText.includes('::'))
                                item.kind = monaco.languages.CompletionItemKind.EnumMember
                            else if (item.insertText.startsWith('this->'))
                                item.kind = 0
                            else item.kind = monaco.languages.CompletionItemKind.Text
                            return item
                        })
                    }
                    return suggest;
                }
            })

            monaco.languages.registerInlineCompletionsProvider('python',{
                freeInlineCompletions: ()=>{},
                provideInlineCompletions: async (document, position,context) => {
                    if(context.triggerKind==0) return {items:[]}
                    const offset = document.getValueLengthInRange({startLineNumber: 0,endLineNumber: position.lineNumber,startColumn: 0,endColumn: position.column})
                    const completions = (await aic.getCompletion(document.getValue(),offset,'python')).completionItems || []
                    const final = {items:completions.map(c=>({ insertText: c.completion.text.substring(position.column-1) }))}
                    console.log(final)
                    return final
                },
            })
            monaco.languages.registerHoverProvider('python', {
                provideHover: (model, position) => {
                    let [sequenceWord,range] = backtrackValidePosition(model, position,true)
                    if(sequenceWord===false) return {contents:[]}
                    if(this.currentCompletion[0]?.notebook){
                        sequenceWord = sequenceWord.split('.')
                        let lastToken = sequenceWord.pop()
                        if(sequenceWord.length===0){
                            const variables = Object.keys(this.currentCompletion[0].variables).filter(v=>v.startsWith(lastToken))
                            return {contents:variables.map(v=>{
                                const type = this.currentCompletion[0].variables[v]
                                if(typeof type === 'string'){
                                    const info = this.currentCompletion[0].variablesType[type]?.current
                                    return info?.documentation
                                } else {
                                    return type?.documentation
                                }
                            })}
                        }
                        let type = this.currentCompletion[0].variables[sequenceWord[0]]
                        if(typeof type === 'object')type = type.return_type
                        let i = 1
                        while(i<sequenceWord.length && type){
                            type = this.currentCompletion[0].variablesType[type]?.members.find((m)=>m.label===sequenceWord[i])?.return_type
                            i++
                        }
                        if(!type) return {range,contents:[]}
                        //const prefix = sequenceWord.length>0?sequenceWord.join('.')+'.':''
                        //return {suggestions:JSON.parse(JSON.stringify(this.currentCompletion[0].variablesType[type].members.filter((m)=>m.label.startsWith(lastToken)))).map(m=>{m.insertText=prefix+m.insertText;return m})}
                        return {contents:[this.currentCompletion[0].variablesType[type]?.members.find((m)=>m.label === lastToken)?.documentation]}

                    } else {
                        const depth = sequenceWord.split('.').length
                        let suggestions = this.currentCompletion.length?this.currentCompletion.filter(w => w.insertText.replace('()','')===sequenceWord && w.depth === depth):[]
                        const tmp = {
                            range: range,
                            contents: suggestions.map(v=>(v.documentation))
                        };
                        //console.log(sequenceWord,depth,tmp)
                        return tmp
                    }
                }
            });
            monaco.languages.registerCompletionItemProvider('python', {
                provideCompletionItems: (model, position) => {

                    let [sequenceWord,range] = backtrackValidePosition(model, position)
                    if(sequenceWord===false) return {suggestions:[]}
                    sequenceWord = sequenceWord.split('.')
                    if(this.currentCompletion[0]?.notebook){
                        let lastToken = sequenceWord.pop()
                        if(sequenceWord.length===0){
                            const variables = Object.keys(this.currentCompletion[0].variables).filter(v=>v.startsWith(lastToken))
                            return {suggestions:variables.map(v=>{
                                const type = this.currentCompletion[0].variables[v]
                                if(typeof type === 'string'){
                                    const info = this.currentCompletion[0].variablesType[type]?.current
                                    return {label:v,insertText:v,documentation:info?.documentation,kind:info?.kind}
                                } else {
                                    return type
                                }
                            })}
                        }
                        let type = this.currentCompletion[0].variables[sequenceWord[0]]
                        if(typeof type === 'object')type = type.return_type
                        //console.log(sequenceWord,lastToken)
                        let i = 1;
                        while(i<sequenceWord.length && type){
                            type = this.currentCompletion[0].variablesType[type]?.members.find((m)=>m.label===sequenceWord[i])?.return_type
                            i++
                        }
                        if(!type) return {suggestions:[]}
                        let suggestions = this.currentCompletion[0].variablesType[type]?.members.filter((m)=>m.label.startsWith(lastToken))
                        suggestions = suggestions?JSON.parse(JSON.stringify(suggestions)):[]
                        //console.log(suggestions)
                        return {suggestions}                        
                    } else {
                        sequenceWord = sequenceWord.join('.')
                        //console.log(sequenceWord,' / ',depth)
                        let suggestions = this.currentCompletion.filter(w => w.insertText.startsWith(sequenceWord))
                        //console.log(suggestions)
                        var suggest = {
                            suggestions: suggestions.map((item) => {
                                //console.log(item)
                                item.range = range
                                item.insertTextRules = monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet
                                if(!item.kind)
                                    if (item.insertText?.endsWith(')'))
                                        item.kind = monaco.languages.CompletionItemKind.Function
                                    else if (item.insertText?.includes('.'))
                                        item.kind = monaco.languages.CompletionItemKind.Property
                                    else item.kind = monaco.languages.CompletionItemKind.Variable
                                return item
                            })
                        }
                        return suggest;
                    }
                }
            })
        }
    }

    /**
     * Returns an editor
     * @param {*} id The id of the editor
     */
    getEditor(id) {
        return this.monaco[id].editor
    }

    /**
     * Parses providers as a list of completions in currentCompletion
     * @param {*} providers The available providers
     */
    updateCurrentCompletion(providers) {
        this.currentCompletion = providers.flatMap((provider) => {
            return this.completionProvider[provider]()
        })
    }

    /**
     * Sets the callback function on Enter for an editor
     * @param {*} id The editor id
     * @param {*} callback The callback function with the editor content as a parameter
     */
    setCallBackMonolineEnter(id, callback) {
        this.callBackMonolineEnter[id] = callback;
    }

    /**
     * Sets the callback function on Enter for an editor
     * @param {*} id The editor id
     * @param {*} callback The callback function with the editor content as a parameter
     */
    TriggerCallBackMonolineEnter(id, value = undefined) {
        this.callBackMonolineEnter[id](value === undefined ? this.monaco[id].editor.getValue() : value);
    }
}

const codeEditorProvider = new CodeEditorProvider()
export default codeEditorProvider;