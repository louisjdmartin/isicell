import CodeEditorProvider from './codeEditorProvider'
import ansiToHtml from 'ansi-to-html';
import { downloadFile, fetchW, streamJson } from '../utils/misc';
import Parameters from '../manager/ParametersManager';
import '../css/notebook.css'
import { getSelectionSearchString } from 'monaco-editor/esm/vs/editor/contrib/find/browser/findController';
import * as strings from 'monaco-editor/esm/vs/base/common/strings.js';
import { FindDecorations } from 'monaco-editor/esm/vs/editor/contrib/find/browser/findDecorations';
import Plotly from 'plotly.js-dist'
import { renderMarkdown } from 'monaco-editor/esm/vs/base/browser/markdownRenderer';
let idCell = 0



const defaultNoteBook = {
    cells: [{
        cell_type: 'code', source: [
            `import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import random
import numpy as np
from multiprocessing import Pool
from tqdm import tqdm

maxStep = params['input']['Scenario']['maxStep']
dt = params['input']['Scenario']['dt']

def run_simu(id):
    simu = Simu(params, random.randrange(2147483647))
    stateNames = simu.getStateNames()
    stateNames = dict(zip(range(len(stateNames)),stateNames))
    statesData = []
    for i in (range(maxStep) if id>0 else tqdm(range(maxStep))):
        statesData.append(pd.Series(simu.getStates(),dtype='uint8').value_counts().to_dict())
        simu.update()
    statesData = pd.DataFrame(statesData).fillna(0).rename(columns=stateNames)
    statesData['time (h)'] = np.arange(maxStep) * dt / 3600
    return statesData.melt(id_vars='time (h)',var_name='states',value_name='number of cells')

with Pool(12) as p:
    res = pd.concat(p.imap_unordered(run_simu,range(12)),ignore_index=True)

sns.lineplot(data=res, hue='states', x='time (h)', y='number of cells', errorbar='sd')
plt.show()`], metadata: {}, execution_count: null, outputs: []
    }],
    metadata: {
        kernelspec: { language: "python", name: "python3", display_name: "Python 3 (isikernel)" },
        language_info: { codemirror_mode: { name: "ipython", version: 3 }, name: "Python" },
        orig_nbformat: 4
    },
    nbformat: 4,
    nbformat_minor: 5
}

const ansiToHtmlConverter = new ansiToHtml({ colors: { 0: '#222222', 1: '#fc618d', 2: '#7bd88f', 11: '#fd9353', 5: '#5ad4e6', 4: '#948ae3', 6: '#847ac3', 7: '#AAA', 8: '#888', 9: '#fd698f', 10: '#7edf9f', 3: '#fce566', 12: '#9f8fef', 13: '#9f6fef', 14: '#5fdfef', 15: '#F7F1FF' } });

function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}
function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
    );
}

class WidgetGlobalFind {
    constructor(notebook){
        this.notebook = notebook;
        this.notebook.container.classList.add('monaco-editor')
        this.notebook.container.insertAdjacentHTML('beforeend',`<div class="editor-widget global find-widget" aria-hidden="false" widgetid="editor.contrib.findWidget"
        style="width: 419px; height: 33px; position: absolute; max-width: 939px; top: 57px; right: 20px;">
        <div title="Toggle Replace" tabindex="0" class="button toggle left codicon codicon-find-expanded" role="button"
            aria-label="Toggle Replace" aria-expanded="true" aria-disabled="false"></div>
        <div class="find-part">
            <div class="monaco-findInput">
                <div class="monaco-scrollable-element " role="presentation" style="position: relative; overflow: hidden;">
                    <div class="monaco-inputbox idle" data-keybinding-context="17"
                        style="overflow: hidden; background-color: var(--vscode-input-background); color: var(--vscode-input-foreground); border: 1px solid var(--vscode-input-border, transparent);">
                        <div class="ibwrapper"><textarea class="input" autocorrect="off" autocapitalize="off"
                                spellcheck="false" wrap="off" aria-label="Find" placeholder="Find" title="Find"
                                style="background-color: inherit; color: var(--vscode-input-foreground); width: calc(100% - 66px); height: 23px;"></textarea>
                            <div class="mirror" style="white-space: pre; overflow-wrap: initial; padding-right: 66px;">
                            </div>
                        </div>
                    </div>
                    <div role="presentation" aria-hidden="true" class="invisible scrollbar horizontal"
                        style="position: absolute; width: 0px; height: 10px; left: 0px; bottom: 0px;">
                        <div class="slider"
                            style="position: absolute; top: 0px; left: 0px; height: 10px; transform: translate3d(0px, 0px, 0px); contain: strict; width: 0px;">
                        </div>
                    </div>
                    <div role="presentation" aria-hidden="true" class="invisible scrollbar vertical"
                        style="position: absolute; width: 10px; height: 23px; right: 0px; top: 0px;">
                        <div class="slider"
                            style="position: absolute; top: 0px; left: 0px; width: 10px; transform: translate3d(0px, 0px, 0px); contain: strict; height: 23px;">
                        </div>
                    </div>
                </div>
                <div class="controls">
                    <div title="Match Case (Alt+C)" class="monaco-custom-toggle codicon codicon-case-sensitive" tabindex="0"
                        role="checkbox" aria-checked="false" aria-label="Match Case (Alt+C)" aria-disabled="false"
                        style="color: inherit;"></div>
                    <div title="Match Whole Word (Alt+W)" class="monaco-custom-toggle codicon codicon-whole-word"
                        tabindex="0" role="checkbox" aria-checked="false" aria-label="Match Whole Word (Alt+W)"
                        aria-disabled="false" style="color: inherit;"></div>
                    <div title="Use Regular Expression (Alt+R)" class="monaco-custom-toggle codicon codicon-regex"
                        tabindex="0" role="checkbox" aria-checked="false" aria-label="Use Regular Expression (Alt+R)"
                        aria-disabled="false" style="color: inherit;"></div>
                </div>
            </div>
            <div class="find-actions">
                <div class="matchesCount" title="" style="min-width: 69px;">1 of 1</div>
                <div title="Previous Match (Shift+Enter)" tabindex="0" class="button codicon codicon-find-previous-match"
                    role="button" aria-label="Previous Match (Shift+Enter)" aria-disabled="false"></div>
                <div title="Next Match (Enter)" tabindex="0" class="button codicon codicon-find-next-match" role="button"
                    aria-label="Next Match (Enter)" aria-disabled="false"></div>
                <div title="Find in Cell" class="monaco-custom-toggle codicon codicon-find-selection"
                    tabindex="0" role="checkbox" aria-checked="false" aria-label="Find in Cell"
                    aria-disabled="true" style="color: inherit;"></div>
            </div>
        </div>
        <div title="Close (Escape)" tabindex="0" class="button codicon codicon-widget-close" role="button"
            aria-label="Close (Escape)" aria-disabled="false"></div>
        <div class="replace-part">
            <div class="monaco-findInput" style="width: 235px;">
                <div class="monaco-scrollable-element " role="presentation" style="position: relative; overflow: hidden;">
                    <div class="monaco-inputbox idle" data-keybinding-context="18"
                        style="overflow: hidden; background-color: var(--vscode-input-background); color: var(--vscode-input-foreground); border: 1px solid var(--vscode-input-border, transparent);">
                        <div class="ibwrapper"><textarea class="input empty" autocorrect="off" autocapitalize="off"
                                spellcheck="false" wrap="off" aria-label="Replace" placeholder="Replace" title="Replace"
                                style="background-color: inherit; color: var(--vscode-input-foreground); height: 23px; width: calc(100% - 22px);"></textarea>
                            <div class="mirror" style="white-space: pre; overflow-wrap: initial; padding-right: 22px;">
                                &nbsp;</div>
                        </div>
                    </div>
                    <div role="presentation" aria-hidden="true" class="invisible scrollbar horizontal"
                        style="position: absolute; width: 0px; height: 10px; left: 0px; bottom: 0px;">
                        <div class="slider"
                            style="position: absolute; top: 0px; left: 0px; height: 10px; transform: translate3d(0px, 0px, 0px); contain: strict; width: 0px;">
                        </div>
                    </div>
                    <div role="presentation" aria-hidden="true" class="invisible scrollbar vertical"
                        style="position: absolute; width: 10px; height: 23px; right: 0px; top: 0px;">
                        <div class="slider"
                            style="position: absolute; top: 0px; left: 0px; width: 10px; transform: translate3d(0px, 0px, 0px); contain: strict; height: 23px;">
                        </div>
                    </div>
                </div>
                <div class="controls">
                    <div title="Preserve Case (Alt+P)" class="monaco-custom-toggle codicon codicon-preserve-case"
                        tabindex="0" role="checkbox" aria-checked="false" aria-label="Preserve Case (Alt+P)"
                        aria-disabled="false" style="color: inherit;"></div>
                </div>
            </div>
            <div class="replace-actions">
                <div title="Replace (Enter)" tabindex="0" class="button codicon codicon-find-replace" role="button"
                    aria-label="Replace (Enter)" aria-disabled="false"></div>
                <div title="Replace All (Ctrl+Alt+Enter)" tabindex="0" class="button codicon codicon-find-replace-all"
                    role="button" aria-label="Replace All (Ctrl+Alt+Enter)" aria-disabled="false"></div>
            </div>
        </div>
        <div class="monaco-sash vertical" style="width: 2px; left: -1px;"></div>
        </div>`)
        this.DOM = this.notebook.container.lastChild
        this.DOM_btn_replaceToggled = this.DOM.querySelector('.button.toggle.left.codicon.codicon-find-expanded')
        this.DOM_btn_close = this.DOM.querySelector('.button.codicon.codicon-widget-close');
        this.DOM_btn_replace = this.DOM.querySelector('.button.codicon-find-replace');
        this.DOM_btn_replaceAll = this.DOM.querySelector('.button.codicon-find-replace-all');
        [this.inputFind,this.inputReplace] = this.DOM.querySelectorAll('.input')
        this.DOM_text_matchCount = this.DOM.querySelector('.matchesCount')
        this.DOM_check_findInCell = this.DOM.querySelector('.codicon-find-selection')
        this.DOM_check_caseSensitive = this.DOM.querySelector('.codicon-case-sensitive')
        this.DOM_check_wholeWord = this.DOM.querySelector('.codicon-whole-word')
        this.DOM_check_regex = this.DOM.querySelector('.codicon-regex')
        this.DOM_check_preserveCase = this.DOM.querySelector('.codicon-preserve-case')
    
        this.addEventListener()
    }

    addEventListener(){
        this.DOM_btn_replaceToggled.addEventListener('click',()=>{
            if(this.DOM.classList.contains('replaceToggled'))
                this.DOM.style.height = '33px';
            else this.DOM.style.height = '62px';
            this.DOM.classList.toggle('replaceToggled')
        })

        this.DOM_btn_close.addEventListener('click',()=>{
            this.DOM.classList.remove('visible')
            for(const c of this.notebook.cells) c.findController.setSearchString('')
        })

        this.inputFind.addEventListener('keydown',(e)=>{
            if(e.key==='Enter') {
                this._changeSelectedMatch(1)
                e.preventDefault();
                e.stopPropagation();
            } else this._prevValueInputFind = this.inputFind.value
        })
        this.inputFind.addEventListener('keyup',(e)=>{
            const value = this.inputFind.value
            if(e.key!=='Enter' && this._prevValueInputFind !== value) {
                for(const c of this.notebook.cells) c.findController._state.change({ searchString: this.inputFind.value }, false);
                this.updateMatchCount()
            }
        })
        this.inputFind.addEventListener('focus',()=>{
            this.inputFind.parentElement.parentElement.classList.add('synthetic-focus')
        })
        this.inputFind.addEventListener('blur',()=>{
            this.inputFind.parentElement.parentElement.classList.remove('synthetic-focus')
        })
        this.inputReplace.addEventListener('focus',()=>{
            this.inputReplace.parentElement.parentElement.classList.add('synthetic-focus')
        })
        this.inputReplace.addEventListener('blur',()=>{
            this.inputReplace.parentElement.parentElement.classList.remove('synthetic-focus')
        })

        this.DOM_btn_replace.addEventListener('click',()=>{
            this.replace()
        })
        this.DOM_btn_replaceAll.addEventListener('click',()=>{
            this.replaceAll()
        })

        this.inputReplace.addEventListener('keydown',(e)=>{
            if(e.key==='Enter') {
                this.replace()
                e.preventDefault();
                e.stopPropagation();
            } else this._prevValueInputReplace = this.inputReplace.value
        })
        this.inputReplace.addEventListener('keyup',(e)=>{
            const value = this.inputFind.value
            if(e.key!=='Enter' && this._prevValueInputReplace !== value) {
                for(const c of this.notebook.cells) c.findController._state.change({replaceString:this.inputReplace.value}, true);
                this.updateMatchCount()
            }
        })

        const styleCkeck="color: var(--vscode-inputOption-activeForeground); border-color: var(--vscode-inputOption-activeBorder); background-color: var(--vscode-inputOption-activeBackground);"
        const styleNotCheck="color:inerit;"
        this.DOM_check_findInCell.addEventListener('click',()=>{const checked=this.DOM_check_findInCell.classList.toggle('checked');this.DOM_check_findInCell.style=checked?styleCkeck:styleNotCheck;})
        this.DOM_check_caseSensitive.addEventListener('click',()=>{const checked=this.DOM_check_caseSensitive.classList.toggle('checked');this.DOM_check_caseSensitive.style=checked?styleCkeck:styleNotCheck;this.updateOption('matchCase')})
        this.DOM_check_wholeWord.addEventListener('click',()=>{const checked=this.DOM_check_wholeWord.classList.toggle('checked');this.DOM_check_wholeWord.style=checked?styleCkeck:styleNotCheck;this.updateOption('wholeWord')})
        this.DOM_check_regex.addEventListener('click',()=>{const checked=this.DOM_check_regex.classList.toggle('checked');this.DOM_check_regex.style=checked?styleCkeck:styleNotCheck;this.updateOption('isRegex')})
        this.DOM_check_preserveCase.addEventListener('click',()=>{const checked=this.DOM_check_preserveCase.classList.toggle('checked');this.DOM_check_preserveCase.style=checked?styleCkeck:styleNotCheck;this.updateOption('preserveCase')})

        this.findInCell = ()=>this.DOM_check_findInCell.classList.contains('checked')
        this.optionsIsCheck = {
            isRegex: ()=>this.DOM_check_regex.classList.contains('checked'),
            wholeWord: ()=>this.DOM_check_wholeWord.classList.contains('checked'),
            matchCase: ()=>this.DOM_check_caseSensitive.classList.contains('checked'),
            preserveCase:()=>this.DOM_check_preserveCase.classList.contains('checked')
        }

        
        this.DOM.querySelector('.codicon-find-previous-match').addEventListener('click',()=>this._changeSelectedMatch(-1))

        this.DOM.querySelector('.codicon-find-next-match').addEventListener('click',()=>this._changeSelectedMatch(1))

    }
    getSate(){
        const state = {}
        for(const k of Object.keys(this.optionsIsCheck))state[k]=this.optionsIsCheck[k]()
        state['searchString'] = this.DOM.classList.contains('visible')?this.inputFind.value:''
        state['replaceString'] = this.inputReplace.value
    }
    updateOption(option){
        const newState = {}
        newState[option]=this.optionsIsCheck[option]()/*
        if(option==='isRegex'){
            if(newState.isRegex){
                this.inputFind.value = strings.escapeRegExpCharacters(this.inputFind.value)
            } else {
                this.inputFind.value = this.inputFind.value.replaceAll(/\\(.)/g, '$1')
            }
            newState.searchString = this.inputFind.value
        }*/
        for(const cell of this.notebook.cells) cell.findController._state.change(newState, true);
        this.updateMatchCount()
    }
    _updateAllOptions(cell){
        const newState = {}
        for(const option of ['isRegex','wholeWord','matchCase','preserveCase'])
            newState[option]=this.optionsIsCheck[option]()
        cell.findController._state.change(newState, true);
    }
    _updateOption(cell,option){
        const newState = {}
        newState[option]=this.optionsIsCheck[option]()
        cell.findController._state.change(newState, true);
    }

    _indexOfRange(ranges,r){
        const keys = ['startLineNumber','startColumn','endLineNumber' ,'endColumn']
        for(let i=0;i<ranges.length;i++){
            let isFind = true
            for(const k of keys){
                if(ranges[i][k] !== r[k]){isFind = false;break;}
            }
            if(isFind)return i
        }
    }
    search(range){
        this.currentCellMatch = this.notebook.currentCell
        let selectionSearchString = getSelectionSearchString(this.currentCellMatch.codeEditor, 'single', false) || this.inputFind.value;
        if (selectionSearchString) {
            if (this.optionsIsCheck.isRegex()) {
                selectionSearchString = strings.escapeRegExpCharacters(selectionSearchString);
            }
            for(const c of this.notebook.cells) c.findController._state.change({ searchString: selectionSearchString }, false);
            this.inputFind.value = this.currentCellMatch.findController._state.searchString
            this._prevValueInputFind = selectionSearchString
            this.currentCellMatch.findController?._model?._decorations?.setCurrentFindMatch(range)
//            $0.obj.codeEditor.getModel().getAllDecorations().filter(d=>d.options.description === 'find-match'/* current-find-match */).map(d=>d.id)
        }
        this.updateMatchCount()
        this.DOM.classList.add('visible')
        this.inputFind.focus()
        this.inputFind.select()

    }
    updateMatchCount(){
        let text = "No results"
        const allMatch = this.notebook.cells.map(c=>this.getAllDecoration(c)).flat()
        const currentMatch = this.currentCellMatch?this.getCurrentDecoration(this.currentCellMatch):undefined
        if(allMatch.length>0){
            if(currentMatch) text = `${allMatch.indexOf(currentMatch)+1} of ${allMatch.length}`
            else text = `? of ${allMatch.length}`
            this.DOM.classList.remove('no-results')
        } else {
            this.DOM.classList.add('no-results')
        }
        this.DOM_text_matchCount.innerText = text
    }

    replace(){
        if(!this.currentCellMatch)this.currentCellMatch = this.notebook.currentCell
        if(this.findInCell()) this.currentCellMatch.findController.replace()
        else {
            const currentMatch = this.getCurrentDecoration(this.currentCellMatch)
            if(!currentMatch){
                this._changeSelectedMatch(1)
            } else {
                const decos = this.getAllDecoration(this.currentCellMatch)
                const isLastMatchOfCell = decos.indexOf(currentMatch)==decos.length-1
                const txtBefore = this.currentCellMatch.codeEditor.getValue()
                this.currentCellMatch.findController.replace()
                //TODO: the first click on replace after search do not replace, so this hack :
                if(txtBefore === this.currentCellMatch.codeEditor.getValue()){
                    this.currentCellMatch.findController.moveToPrevMatch()
                    this.currentCellMatch.findController.replace()
                }
                if(isLastMatchOfCell){
                    this.currentCellMatch.findController.moveToPrevMatch()
                }
                if(!this.getCurrentDecoration(this.currentCellMatch)||isLastMatchOfCell){
                    this._changeSelectedMatch(1)
                }
            }
        }
        this.updateMatchCount()
    }

    replaceAll(){
        if(!this.currentCellMatch)this.currentCellMatch = this.notebook.currentCell
        if(this.findInCell()) this.currentCellMatch.findController.replaceAll()
        else for(const c of this.notebook.cells) c.findController.replaceAll()
        this.updateMatchCount()
    }

    _changeSelectedMatch(direction){
        if(!this.currentCellMatch)this.currentCellMatch= this.notebook.currentCell
        const allDeco = this.notebook.cells.map(c=>this.getAllDecoration(c))
        if(allDeco.flat().length>0){
            
            if(!this.findInCell()){
                let cellIndex = this.notebook.cells.indexOf(this.currentCellMatch)
                const currentMatch = this.getCurrentDecoration(this.currentCellMatch)
                const i = allDeco[cellIndex].indexOf(currentMatch)+direction
                if(i<0 && direction<0){
                    this.unDecorateFindMatch(this.currentCellMatch)
                    do{
                        cellIndex-=1
                        if(cellIndex<0) cellIndex = this.notebook.cells.length-1
                    }while(allDeco[cellIndex].length<=0)
                    this.currentCellMatch = this.notebook.cells[cellIndex]
                    this.currentCellMatch.DOM_source.scrollIntoView({ behavior: "smooth", inline: "nearest", block: "nearest" })
                    this.currentCellMatch.codeEditor.setPosition({lineNumber:Infinity, column: Infinity})
                } else if(i>=allDeco[cellIndex].length && direction>0){
                    this.unDecorateFindMatch(this.currentCellMatch)
                    do{
                        cellIndex+=1
                        if(cellIndex>=this.notebook.cells.length) cellIndex = 0
                    }while(allDeco[cellIndex].length<=0)
                    this.currentCellMatch = this.notebook.cells[cellIndex]
                    this.currentCellMatch.DOM_source.scrollIntoView({ behavior: "smooth", inline: "nearest", block: "nearest" })
                    this.currentCellMatch.codeEditor.setPosition({lineNumber:0, column: 0})
                }
            }
            if(direction<0)this.currentCellMatch.findController.moveToPrevMatch()
            else if(direction>0)this.currentCellMatch.findController.moveToNextMatch()
        }

        this.updateMatchCount()
    }
    getAllDecoration(cell){
        return cell.findController._model?._decorations?._decorations || []
    }
    getCurrentDecoration(cell){
        return cell.findController._model?._decorations?._highlightedDecorationId
    }


    unDecorateFindMatch(cell){
        if(cell.findController?._model?._decorations){
            const idDeco = cell.findController._model._decorations._highlightedDecorationId
            if(idDeco) cell.codeEditor.changeDecorations((changeAccessor) => changeAccessor.changeDecorationOptions(idDeco, FindDecorations._FIND_MATCH_DECORATION))
            cell.findController._model._decorations._highlightedDecorationId = null
        }
    }

    changeCellFocus(){
        if(this.currentCellMatch)this.unDecorateFindMatch(this.currentCellMatch)
        this.currentCellMatch=this.notebook.currentCell
        this.changeFocus = true
    }
}

class Cell {
    constructor(notebook, cell_type, json) {
        this.notebook = notebook;
        this.id = 'cellID_' + (idCell++)
        cell_type = cell_type ? cell_type : json.cell_type;
        if (json) {
            this.data = json
            this.data.cell_type = cell_type
            this.data.source = [this.data.source.join('')]
        } else {
            this.data = { "cell_type": cell_type, "source": [""], "metadata": {} }
            if (cell_type === 'code') {
                this.data.execution_count = null
                this.data.outputs = []
            }
        }
        this.DOM = document.createElement('div')
        this.DOM.className = 'cell'
        this.DOM.addEventListener('click', () => { this.notebook.currentCell = this })

        let btnGroup = document.createElement('ul')
        btnGroup.className = "buttonSlot"
        //btn run
        let li = document.createElement('li')
        const btnP = document.createElement('a')
        btnP.className = 'icon-play'
        btnP.addEventListener('click', () => {
            if (btnP.classList.contains('icon-play')) {
                this.notebook.runCell(this)
            } else if(cell_type === 'code') {
                fetchW('api/notebook/Interupt/' + this.notebook.idNotebook)
            } else {
                this.divContentOutputs.style.display = 'none'
                this.divContentSource.style.display = null
                this.changeBtnPlay()
            }
        })
        this.changeBtnPlay = () => {
            btnP.classList.toggle('icon-play')
            btnP.classList.toggle('icon-stop')
        }
        li.appendChild(btnP)
        btnGroup.appendChild(li)




        //btn up
        li = document.createElement('li')
        li.className = 'up'
        let btn = document.createElement('a')
        btn.className = 'icon-up'
        btn.addEventListener('click', () => {
            this.notebook.moveUp(this)
        })
        li.appendChild(btn)
        btnGroup.appendChild(li)


        //btn down
        li = document.createElement('li')
        li.className = 'down'
        btn = document.createElement('a')
        btn.className = 'icon-down'
        btn.addEventListener('click', () => {
            this.notebook.moveDown(this)
        })
        li.appendChild(btn)
        btnGroup.appendChild(li)


        //btn delete
        li = document.createElement('li')
        btn = document.createElement('a')
        btn.className = 'icon-trash'
        btn.addEventListener('click', () => {
            this.notebook.removeCell(this)
        })
        li.appendChild(btn)
        btnGroup.appendChild(li)

        //btn clean output

        if (this.data.cell_type === 'code') {
            li = document.createElement('li')
            li.className = 'clean'
            btn = document.createElement('a')
            btn.className = 'icon-clean'
            btn.addEventListener('click', () => {
                this.clearOutput()
                if (this.notebook.setIsEdited) this.notebook.setIsEdited(true)
            })
            li.appendChild(btn)
            btnGroup.appendChild(li)
        }

        this.DOM.appendChild(btnGroup)

        let leftPanelSource = document.createElement('div')
        leftPanelSource.className = 'leftPanel'
        this.DOM_execution_count = document.createElement('h6')
        leftPanelSource.appendChild(this.DOM_execution_count)
        this._updateExecutionCount()

        this.divContentSource = document.createElement('div')
        this.divContentSource.className = 'sourceCell'
        this.DOM_source = document.createElement('div')
        this.DOM_source.className = 'rightPanel'
        this.DOM_source.id = this.id
        //this.DOM_source.style = "height:25px; width:100%"
        this.divContentSource.appendChild(leftPanelSource)
        this.divContentSource.appendChild(this.DOM_source)
        this.DOM.appendChild(this.divContentSource)

        this.divContentOutputs = document.createElement('div')
        this.divContentOutputs.className = 'outputsCell'
        let leftPanelOutputs = document.createElement('div')
        leftPanelOutputs.className = 'leftPanel'

        this.DOM_output = document.createElement('div')
        this.DOM_output.className = 'rightPanel'
        this.divContentOutputs.appendChild(leftPanelOutputs)
        this.divContentOutputs.appendChild(this.DOM_output)
        this.DOM.appendChild(this.divContentOutputs)

        if (this.data.cell_type === 'markdown') {
            this.divContentSource.style.paddingBottom = '25px'
            if (this.data.source.join('') !== '') {
                this.divContentOutputs.style.display = 'none'
            }
            this.divContentOutputs.addEventListener('dblclick', () => {
                this.divContentOutputs.style.display = 'none'
                this.divContentSource.style.display = null
                this.changeBtnPlay()
            })
        }

        //bottom add new cell
        let bottomButton = document.createElement('div')
        bottomButton.className = 'bottomGroup'
        btn = document.createElement('a')
        btn.innerText = '+ Code'
        btn.className = 'buttunAdd'
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            e.stopPropagation()
            this.notebook.newCell(this, 'code')
        })
        bottomButton.appendChild(btn)
        btn = document.createElement('a')
        btn.innerText = '+ Markdown'
        btn.className = 'buttunAdd'
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            e.stopPropagation()
            this.notebook.newCell(this, 'markdown')
        })
        bottomButton.appendChild(btn)
        this.DOM.appendChild(bottomButton)



        this.notebook.cellsContainer.appendChild(this.DOM)
        this.DOM.addEventListener('keydown', (evt) => {
            if (evt.key === 'Enter' && evt.shiftKey) {
                this.notebook.runCurrentCell();
                this.notebook.nextCell(); //TODO: ajouter focus (voir ajouter ça dans les options de l'editeur)
                evt.preventDefault();
                evt.stopPropagation();
            } else if (evt.key === 'Enter' && evt.ctrlKey) {
                this.notebook.runCurrentCell();
                evt.preventDefault();
                evt.stopPropagation();
            }
        }, true)

        this.codeEditor = CodeEditorProvider.createEditor(this.id, { lineNumbers: 'on', minimap: false, language: (cell_type==='code'?'python':'markdown'), padding: { top: 0, bottom: 0 },scrollVertical:"hidden", callbackGlobalSearch:(r)=>this.notebook.widgetGlobalFind.search(r) });
        CodeEditorProvider.setEditorCompletionProvider(this.id, [this.notebook.idNotebook])
        let textContent = this.data.source.join('')
        this.codeEditor.setValue(textContent)
        this.DOM.obj = this
        this.findController = this.codeEditor.getContribution("editor.contrib.findController")
        this.findController._createFindWidget = ()=>{}
        this.findController.start({forceRevealReplace: true,seedSearchStringFromSelection: true,seedSearchStringFromGlobalFindClipboard: false},this.notebook.widgetGlobalFind.getSate())
        //this.findController._state.isRevealed = true
        //this.findController.closeFindWidget()
        //this.findController.dispose()

        this.codeEditor.onDidChangeModelContent(() => {
            let txtContent = this.codeEditor.getValue()
            this.data.source = [txtContent]
            if (this.notebook.setIsEdited) this.notebook.setIsEdited(true)
        })

        let prevHeight = (this.codeEditor.getModel()?.getLineCount()) * 18 + 40//((textContent.match(/\n/g) || []).length + 1) * 18 + 38

        this.codeEditor.onDidChangeModelDecorations(() => {
            let height = (this.codeEditor.getModel()?.getLineCount()) * 18 + 40
            if (height !== prevHeight) {
                this.DOM_source.style.height = height + 'px'
                prevHeight = height
                this.codeEditor.layout()
            }
        });
        this.codeEditor.layout()

        this.DOM_execution_time = document.createElement('p')
        this.DOM_execution_time.className = "executionTime"
        this.DOM_execution_time.innerText = ""
        this.DOM_source.appendChild(this.DOM_execution_time)


        this.DOM_source.style.height = prevHeight + 'px'

        this._renderOutputs()
    }

    runCell() {
        this.data.source = [this.codeEditor.getValue()]
        if (this.data.cell_type === "code") {
            if (this.data.source.join('').trim() === '') {
                this.execution_time = 0;
                this._updateExecutionTime('finish')
                this.data.execution_count = this.notebook.execution_count++;
                this.data.outputs = []
                this._renderOutputs()
                this.notebook.runCellFinish(false);
                return false
            }
            //document.getElementById('btnRunCell').disabled = true;


            let lastOuputStream;
            let lastOuputStreamDOM;
            this.changeBtnPlay()
            this.clearOutput()
            this.execution_time = 0
            this.timer = setInterval(() => {
                this.execution_time += 0.1;
                this._updateExecutionTime('loading')
            }, 100);
            if(!this.notebook.isRunning){
                this.notebook.tokenBuild = Parameters.getCurrentItemBuild().token;
                this.notebook.checksum = Parameters.getCurrentItemBuild().checksum;
                this.notebook.isRunning = true;
            }
            streamJson('api/notebook/evaluate/' + this.notebook.idNotebook + '/' + this.notebook.tokenBuild, {
                method: 'POST', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, body: JSON.stringify({ command: this.data.source.join('') })
            },{
                callBackouput:(jsonValue)=>{
                    for (let v of jsonValue) {
                        if (v.hasOwnProperty("stream")) {
                            if(!lastOuputStream){
                                lastOuputStream = {name:'stdout',output_type:'stream',text:[v.stream]}
                                this.data.outputs.push(lastOuputStream)
                                lastOuputStreamDOM = this._renderOutputStream(lastOuputStream);
                            } else if(!lastOuputStream.text[lastOuputStream.text.length-1].endsWith('\n')){
                                const i = lastOuputStream.text.length - 1
                                lastOuputStream.text[i]=(lastOuputStream.text[i] + v.stream)?.replace(/^.*\r/gm, '')
                                this._renderOutputStream(lastOuputStream,lastOuputStreamDOM);
                            } else if(this.data.outputs[this.data.outputs.length-1] === lastOuputStream){
                                lastOuputStream.text.push(v.stream)
                                this._renderOutputStream(lastOuputStream,lastOuputStreamDOM);
                            } else {
                                lastOuputStream = {name:'stdout',output_type:'stream',text:[v.stream]}
                                this.data.outputs.push(lastOuputStream)
                                lastOuputStreamDOM = this._renderOutputStream(lastOuputStream);
                            }
                        } else if (v.hasOwnProperty("output_type")) {
                            this.data.outputs.push(v)
                            this._renderOutput(v)
                        } else if (v.hasOwnProperty("completion")){
                            this.notebook.addCompletionItems(v['completion'],this.id)
                        }
                        else {
                            console.log(v)
                        }
                    }
                },
                callBackDone:()=>{
                    clearInterval(this.timer);
                    let thereAreError = this.data.outputs.some(c => (c.output_type === 'error'))
                    this._updateExecutionTime(thereAreError ? 'error' : 'finish')
                    this.data.execution_count = this.notebook.execution_count++;
                    this._updateExecutionCount()
                    this.notebook.runCellFinish(thereAreError);
                    this.changeBtnPlay()
                }
            })
        } else if (this.data.cell_type === "markdown") {
            this.notebook.runCellFinish(false);
            this._renderMarkdown()
        }
    }

    hardResetTimer() {
        clearInterval(this.timer);
        this._updateExecutionTime('error')
        this.DOM_execution_count.innerText = '[ ]'
    }

    focus() {
        this.codeEditor.focus()
        if (!isElementInViewport(this.DOM_source))
            this.DOM_source.scrollIntoView({ behavior: "smooth", inline: "nearest", block: "nearest" })
    }

    clearOutput() {
        this.data.outputs = []
        this.DOM_output.innerHTML = ''
    }

    _updateExecutionTime(status,withoutTime=false) {
        let timeText = ''
        let minutes = 0
        let seconds = 0
        withoutTime|=this.execution_time===undefined
        if(!withoutTime){
            minutes = ~~(this.execution_time/60)
            seconds = (this.execution_time % 60)
            timeText = '&nbsp;' + (minutes>0?minutes+'m ':'')+seconds.toFixed(1) + 's'
        }
        if (this.prevStatus !== status) {
            if (status === 'finish') {
                this.DOM_execution_time.innerHTML = `<span style="color:#7bd88f;font-size:13px" class="icon-ok"></span><span>${timeText}</span>`
            } else if (status === 'error') {
                this.DOM_execution_time.innerHTML = `<span style="color:#fc618d;font-size:13px" class="icon-cancel"></span><span>${timeText}</span>`
            } else if (status === 'loading') {
                this.DOM_execution_time.innerHTML = `<span style="font-size:13px" class="icon-load"></span><span>${timeText}</span>`
            } else if (status === 'waiting') {
                this.DOM_execution_time.innerHTML = `<span style="font-size:13px" class="icon-waiting"></span><span>${timeText}</span>`
            } else {
                this.DOM_execution_time.innerHTML = `<span style="font-size:13px" class="icon-cancel"></span><span>${timeText}</span>`
            }
        } else if(!withoutTime){
            this.DOM_execution_time.children[1].innerText = ' ' + (minutes>0?minutes+'m ':'')+seconds.toFixed(1) + 's'
        } else {
            this.DOM_execution_time.children[1].innerText = ''
        }
        this.prevStatus = status;
    }


    _updateExecutionCount(running) {
        if (this.data.cell_type === 'code') this.DOM_execution_count.innerText = running ? '[*]' : (this.data.execution_count ? `[${this.data.execution_count}]` : '[ ]')
    }




    _renderOutput(c){
        let k;
        if(c.output_type==='error'){
            this._renderOutputError(c)
        } else if(c.output_type==='stream'){
            this._renderOutputStream(c)
        }else if(c.data.hasOwnProperty('text/html')){
            this._renderOutputHtml(c)
        } else if(c.data.hasOwnProperty('application/vnd.plotly.v1+json')){
            this._renderOutputPlotly(c)
        } else if(k = Object.keys(c.data).find(v=>v.includes('image'))){
            this._renderImage(c,k)
        } else if(c.data.hasOwnProperty('text/plain')){
            this._renderOutputText(c)
        }
    }
    _renderOutputPlotly(c){
        for(const d of c.data['application/vnd.plotly.v1+json']){
            const gd = document.createElement('div')
            this.DOM_output.appendChild(gd)
            Plotly.newPlot(gd, d);
            (new MutationObserver((m,o) => {
                for(const n of m[0].removedNodes) {
                    if(n===gd){
                        console.log("plotly purge",gd)
                        Plotly.purge(n);
                        o.disconnect()
                        break;
                    }
                }
            })).observe(gd.parentElement, { childList: true });
        }
    }
    _renderOutputStream(c,dom=undefined){
        if(!dom) {
            dom = document.createElement('p')
            this.DOM_output.appendChild(dom)
        }
        dom.textContent = c.text.join('')
        return dom
    }

    _renderOutputText(c){
        const dom = document.createElement('p')
        dom.innerHTML = c.data['text/plain'].join('')
        this.DOM_output.appendChild(dom)
    }

    _renderOutputHtml(c){
        const dom = document.createElement('div')
        dom.innerHTML = c.data['text/html'].join('')
        this.DOM_output.appendChild(dom)
    }

    _renderImage(c,k){
        let src = 'data:' + k + ';base64,' + c.data[k]
        const dom = document.createElement('img')
        dom.src = src
        this.DOM_output.appendChild(dom)
    }

    _renderMarkdown(){
        this.divContentOutputs.style.display = null
        this.DOM_output.innerHTML = ''
        const dom = renderMarkdown({value:this.data.source.join(''),supportHtml:true}).element
        dom.className = "markdown"
        dom.querySelectorAll('[data-href]').forEach(a=>{a.href=a.dataset.href;a.target='_blank'; delete a.dataset.href})
        this.DOM_output.appendChild(dom)
        //this.DOM_output.innerHTML = source ? this._convertMarkdownToHTML(source) : '<center><em>Empty markdown cell, double click to edit</em></center>'
        this.divContentSource.style.display = 'none'

        this.changeBtnPlay()
    }

    _renderOutputError(c){
        let htmlTxt = ansiToHtmlConverter.toHtml(c.traceback.join('\n').replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll('\n', '<br/>').replaceAll(' ', '&nbsp;'))
        const dom = document.createElement('p')
        dom.innerHTML = htmlTxt
        this.DOM_output.appendChild(dom)
    }
    _renderOutputs() {
        //this.DOM_output.innerHTML = ''
        if (this.data.cell_type === "code") {
            this.data.outputs.forEach(c => {
                this._renderOutput(c)
            })
        } else if (this.data.cell_type === "markdown") {
            this._renderMarkdown()
        }
    }
}

export class Notebook {
    constructor(container, fileNameGetter, json, saveCallBack, setIsEdited) {
        try{
            json = (typeof json === 'string' && json) ? JSON.parse(json) : json
            json = (typeof json === 'object') ? json:JSON.parse(JSON.stringify(defaultNoteBook))
        } catch (e) { json = JSON.parse(JSON.stringify(defaultNoteBook)) }
        this.execution_count = 1;
        this.variablesType = {}
        this.isRunning = false;
        this.idNotebook = create_UUID()
        CodeEditorProvider.addCompletionProvider(this.idNotebook, () => [])
        this.container = container;
        this.container.style = "height: 100%; width: 100%; overflow-y: hidden;"
        this.fileNameGetter = fileNameGetter;
        this.saveCallBack = saveCallBack;
        this.setIsEdited = setIsEdited

        this.cellsContainer = document.createElement('div')
        this.cellsContainer.className = "cellsContainer"
        this.cellsContainer.style.paddingBottom= "25%";
        this._initHeader()
        this.cellsContainer.tabIndex=0
        this.cellsContainer.addEventListener('keydown', (evt) => {
            if (((navigator?.userAgentData?.platform || navigator?.platform || 'unknown').match("Mac") ? evt.metaKey : evt.ctrlKey) && evt.key == 's') {
                this.saveCallBack(this.getJSON())
                evt.preventDefault();
                evt.stopPropagation();
            }
        }, true)
        this.container.appendChild(this.cellsContainer);
        this.widgetGlobalFind = new WidgetGlobalFind(this);
        /*
        this.container.tabIndex = 0
        this.container.addEventListener('keydown',(evt)=>{
            if ((evt.key === 'f' || evt.key === 'F') && evt.ctrlKey) {
                searchBar.classList.add('visible')
                evt.preventDefault();
                evt.stopPropagation();
            }
        }, true)*/

        this.createEventLeavePage()
        this.data = json
        if (json && json.cells.length > 0) {
            this.cells = this.data.cells.map(cell => {
                return new Cell(this, undefined, cell)
            })
        } else {
            this.cells = [new Cell(this)]
        }
        delete this.data.cells
        if (this.cells[0]) this.currentCell = this.cells[0]
        this.stackRun = []

    }



    newCell(cell, cell_type='code') {
        if (cell instanceof Cell) {
            let i = this.cells.indexOf(cell) + 1
            let c = new Cell(this, cell_type)
            this.cells.splice(i, 0, c)
            cell.DOM.after(c.DOM)
            setTimeout(() => { this.currentCell = c; this.currentCell.focus() }, 100)
        } else if (cell === 'first') {
            this.cells.unsift(new Cell(this, cell_type))
            this.currentCell = this.cells[0]
            this.currentCell.focus()
        } else {
            this.cells.push(new Cell(this, cell_type))
            this.currentCell = this.cells[this.cells.length - 1]
            this.currentCell.focus()
        }
        if (this.setIsEdited) this.setIsEdited(true)
    }

    runCell(cell) {
        if (this.stackRun.includes(cell)) return false
        cell._updateExecutionCount(true)
        cell._updateExecutionTime('waiting')
        this.stackRun.push(cell)
        if (this.stackRun.length === 1) {
            this.btnRestart.classList.remove('disable')
            this.stackRun[0].runCell()
        }
        if (this.setIsEdited) this.setIsEdited(true)
    }

    moveUp(cell) {
        let currentPosition = this.cells.indexOf(cell)
        cell.DOM.previousSibling.before(cell.DOM)
        this.cells.splice(currentPosition - 1, 0, this.cells.splice(currentPosition, 1)[0])
        if (this.setIsEdited) this.setIsEdited(true)
    }

    moveDown(cell) {
        let currentPosition = this.cells.indexOf(cell)
        cell.DOM.nextSibling.after(cell.DOM)
        this.cells.splice(currentPosition + 1, 0, this.cells.splice(currentPosition, 1)[0])
        if (this.setIsEdited) this.setIsEdited(true)
    }

    removeCell(cell) {
        let i = this.cells.indexOf(cell)
        this.cells.splice(i, 1)
        if (i < this.cells.length) this.currentCell = this.cells[i]
        else if (i - 1 < this.cells.length) this.currentCell = this.cells[i - 1]
        CodeEditorProvider.removeEditor(cell.id)
        this.cellsContainer.removeChild(cell.DOM)
        if (this.setIsEdited) this.setIsEdited(true)
    }

    remove(){
        for (let c of this.cells) {
            CodeEditorProvider.removeEditor(c.id)
        }
        CodeEditorProvider.removeCompletionProvider(this.idNotebook)
        this.container.parentElement.removeChild(this.container)
        if(this.isRunning) this.kill()
    }

    runCurrentCell() {
        this.runCell(this.currentCell)
        if (this.setIsEdited) this.setIsEdited(true)
    }

    runCellFinish(withErr) {
        this.stackRun.shift()
        if (withErr){
             this.stackRun.forEach(cell=>{
                cell._updateExecutionTime('stop',true)
                cell.data.execution_count = undefined
                cell._updateExecutionCount(false)
             })
             this.stackRun = []
        }
        if (this.stackRun.length > 0) this.stackRun[0].runCell()
    }
    /*
        resetEnv() {
            this.stackRun = []
            fetchW('api/notebook/'+this.idNotebook+'/resetenv', { method: 'GET', headers: { 'Content-Type': 'application/json' } }).then((response) => response.json()).then((data) => {
                if (data.result) {
                    this.execution_count = 1;
                }
            })
        }
    */
    nextCell() {
        let i = this.cells.indexOf(this.currentCell)
        if (i < this.cells.length - 1) {
            this.currentCell = this.cells[i + 1]
        } else {
            this.newCell()
        }
        this.currentCell.focus()
    }

    get currentCell() {
        return this._currentCell
    }

    set currentCell(cell) {
        this.refreshStatus()
        if(cell!==this.widgetGlobalFind.currentCellMatch)this.widgetGlobalFind.changeCellFocus()
        if (this.currentCell === cell) return false
        if (this.currentCell){
             this.currentCell.DOM.classList.remove('selected');
        }
        cell.DOM.classList.add('selected');
        this._currentCell = cell;
    }

    getJSON() {
        this.data.cells = JSON.parse(JSON.stringify(this.cells.map(c => c.data)))
        this.data.metadata = defaultNoteBook.metadata
        this.data.nbformat = defaultNoteBook.nbformat
        this.data.nbformat_minor = defaultNoteBook.nbformat_minor
        return this.data
    }

    createEventLeavePage() {
        window.addEventListener('beforeunload', (e) => {
            if (this.isRunning) {
                let confirmationMessage = "Are you sure to exit page without saving the simulation ?";
                e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
                return confirmationMessage;              // Gecko, WebKit, Chrome <34
            }
        });
        window.addEventListener('unload', () => {
            if (this.isRunning) {
                let info = { event: "beforeunload event" };
                navigator.sendBeacon("/api/notebook/kill/" + this.idNotebook, JSON.stringify(info))
            }
        });
    }

    kill(){
        let info = { event: "restart kernel event" };
        return fetchW("api/notebook/kill/" + this.idNotebook, {
            method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(info)
        }).then((response) => response.json());
    }
    addCompletionItems(completionItems,idCell) {
            this.variablesType = Object.assign(this.variablesType, completionItems.newType)
            const variables = completionItems.variables
            CodeEditorProvider.addCompletionProvider(this.idNotebook, () => {
                return [{variables,variablesType:this.variablesType,notebook:true}]
            })
            CodeEditorProvider.updateCurrentCompletion(CodeEditorProvider.monaco[idCell].providers)
    }
    refreshStatus(){
        if(this.isRunning && this.checksum !== Parameters.getCurrentItemBuild().checksum){
            this.statusGUI.innerText = "model has been update"
            this.statusGUI.className = 'icon-attention'
            this.statusGUI.style.color = "#fd9353"
        } else if(this.isRunning) {
            this.statusGUI.innerText = "Running"
            this.statusGUI.className = undefined
            this.statusGUI.style.color = "#2ecc71"
        } else {
            this.statusGUI.innerText = ""
            this.statusGUI.className = undefined
        }

    }
    _initHeader() {
        let header = document.createElement('div')
        header.className = "header-notebook"
        let btn
        if (this.saveCallBack){
            btn = document.createElement('a')
            btn.className = "icon-save"
            btn.innerText = "Save"
            btn.addEventListener('click', () => {
                this.saveCallBack(this.getJSON())
            })
            header.appendChild(btn)
        }
        btn = document.createElement('a')
        btn.className = "icon-play"
        btn.innerText = "run all"
        btn.addEventListener('click', () => {
            this.cells.forEach(c => this.runCell(c))
        })
        header.appendChild(btn)
        this.btnRestart = document.createElement('a')
        this.btnRestart.classList.add("icon-reload")
        this.btnRestart.classList.add('disable')
        this.btnRestart.innerText = "restart kernel"
        this.btnRestart.addEventListener('click', () => {
            if (!this.btnRestart.classList.contains('disable')){
                this.kill().then((data) => {
                    this.btnRestart.classList.add('disable')
                    if (this.stackRun.length > 0) this.stackRun[0].hardResetTimer()
                    this.isRunning = false;
                    this.execution_count = 1;
                    this.stackRun = []
                    this.refreshStatus()
                })
            }
        })
        header.appendChild(this.btnRestart)
        btn = document.createElement('a')
        btn.className = "icon-clean"
        btn.innerText = "clear outputs"
        btn.addEventListener('click', () => {
            this.cells.forEach(c => c.clearOutput())
        })
        header.appendChild(btn)

        const paramSelector = document.createElement('select')
        paramSelector.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>'
        paramSelector.addEventListener('focus', function () {
            this.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>' + Parameters.getCurrentItemBuild().items.map((p, i) => ('<option value="' + i + '">' + p.titleText + '</option>')).join('')
        })
        paramSelector.addEventListener('change', () => {
            if (paramSelector.value) {
                let paramItem = Parameters.getCurrentItemBuild().items[parseInt(paramSelector.value)]
                let cell = new Cell(this, undefined, { cell_type: 'code', source: [('params = ' + JSON.stringify({ input: Parameters.getParamFromItemParam(paramItem) }, null, 2))], metadata: {}, execution_count: null, outputs: [] })
                if (this.currentCell) {
                    let i = this.cells.indexOf(this.currentCell)
                    this.cells.splice(i, 0, cell)
                    this.currentCell.DOM.before(cell.DOM)
                } else {
                    this.cells.unsift(cell)
                    this.cells[0].DOM.before(cell.DOM)
                }
                if (this.setIsEdited) this.setIsEdited(true)
                paramSelector.value = ''
            }
        })
        header.appendChild(paramSelector)
        this.statusGUI = document.createElement('p')
        this.statusGUI.style = "margin-left:auto"
        header.appendChild(this.statusGUI)
        btn = document.createElement('a')
        btn.className = "icon-download"
        btn.style = "background-color:var(--theme);color:var(--dark-bg-0)"
        btn.innerText = "download"
        btn.addEventListener('click', () => {
            downloadFile(URL.createObjectURL(new Blob([JSON.stringify(this.getJSON(), null, 2)], {
                type: "text/plain"
            })), this.fileNameGetter())
        })
        header.appendChild(btn)

        btn = document.createElement('a')
        btn.className = "logo-theme-switch"
        this.cellsContainer.classList.add('follow-theme')
        btn.style = 'height: 20px;min-width: 20px;width: 20px;border: 0px;background: var(--theme);'
        btn.addEventListener('click',()=>{
            this.cellsContainer.classList.toggle('follow-theme')
            if(this.cellsContainer.classList.contains('follow-theme')) btn.style.background = 'var(--theme)'
            else  btn.style.background = 'var(--text)'
        })


        header.appendChild(btn)
        this.container.appendChild(header)
        this.container.nb = this
    }
}