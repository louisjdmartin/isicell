/**
 * ModalWindows class
 * 
 */
const background = document.createElement('div')
background.style.display = 'none'
background.classList.add('context-menu')
document.body.appendChild(background)
export default class ContextMenu {

    constructor(entries) {
        this.target = undefined
        this.cb_cancel = undefined
        this.div = document.createElement('div')
        const ul = document.createElement('ul')
        for (const [name, [logo, clickAction]] of Object.entries(entries)) {
            const li = document.createElement('li')
            li.classList.add(logo)
            //li.innerHTML = `<i class="${logo}">${name}`
            li.textContent = name
            li.addEventListener('click',(e)=>{e.stopPropagation();clickAction(self.target); this.hide();},false)
            ul.appendChild(li)
        }
        this.div.appendChild(ul)

        var self = this;
        this.eventOnContextMenu = function (e) {
            e.preventDefault();
            self.target = this
            self.show(e.pageX,e.pageY)
        }
    }


    show(x,y){
        background.appendChild(this.div)
        background.onclick = () => { this.hide(this.cb_cancel) }
        background.oncontextmenu = (e) => { e.preventDefault();this.hide(this.cb_cancel) }


        background.style.display = null
        this.div.style.left = x+'px'
        this.div.style.top = y+'px'
    }

    hide(cb) {
        background.style.display = 'none'
        background.innerHTML = ""
        if(cb)cb()
    }
}