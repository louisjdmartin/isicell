import '../css/customList.css'
const INDENT_IN_PX = 10
export function loadCustomListElement(){

    JSON.unflatten = function (data) {
        "use strict";
        if (typeof data !== 'object' || Array.isArray(data))
            return data;
        var result = {}, cur, prop, idx, last, temp;
        for (var p in data) {
            cur = result, prop = "", last = 0;
            do {
                idx = p.indexOf(".", last);
                temp = p.substring(last, idx !== -1 ? idx : undefined);
                cur = cur[prop] || (cur[prop] = (!isNaN(parseInt(temp)) ? [] : {}));
                prop = temp;
                last = idx + 1;
            } while (idx >= 0);
            cur[prop] = data[p];
        }
        return result[""];
    }
    JSON.flatten = function (data,level=Infinity) {
        var result = {};
        function recurse(cur, prop,l) {
            if (typeof cur !== 'object' || Array.isArray(cur) || l===0) {
                result[prop] = cur;
            } else {
                var isEmpty = true;
                for (var p in cur) {
                    isEmpty = false;
                    recurse(cur[p], prop ? prop + "." + p : p,l-1);
                }
                if (isEmpty) result[prop] = {};
                
            }
        }
        recurse(data, "",level);
        return result;
    }
/*
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }
    if (window.HTMLCollection && !HTMLCollection.prototype.forEach) {
        HTMLCollection.prototype.forEach = Array.prototype.forEach;
    }
*/



//    const tagChar = { 'MODEL': 'M', 'VIEWER': '\ud83d\udc41', 'CORE': '\u2699' }
    const tagChar = { 'MODEL': '\uf1de', 'VIEWER': '\ue800', 'CORE': '\ue803' }
    const tagRank = { '': -4 }
    tagRank[tagChar['MODEL']] = 3
    tagRank[tagChar['VIEWER']] = -2;
    tagRank[tagChar['CORE']] = -1;
    let ID = -1;
    function getNewID() {
        ID += 1;
        return ID;
    }

    customElements.define('custom-list', class extends HTMLElement {

        clear() {
            for (var i = this.childNodes.length; i--;) this.firstChild.remove()
        }
        addSection(title, options = {}) {
            const section = document.createElement('custom-section')
            this.appendChild(section)
            Object.keys(options).forEach(attr => {
                section[attr] = options[attr];
            });
            section.titleText = title
            return section
        }
    });



    customElements.define('custom-section', class extends HTMLElement {
        constructor() {
            super();
            const tmp = this._tmp = document.createElement('template');
            const header = this._header = document.createElement('div');
            tmp.appendChild(header);

            const title = this._title = document.createElement('span');
            header.appendChild(title);

            const list = this._list = document.createElement('ul');
            tmp.appendChild(list);
        }

        connectedCallback() {
            if (!this.__connected) {
                for (var i = this._tmp.childNodes.length; i--;)  this.appendChild(this._tmp.firstChild)
                delete this._tmp;
                this.__connected = true
                this.refreshIndent()
            }
        }

        applyOptions(sectionOptions) {
            Object.keys(sectionOptions).forEach(attr => { this[attr] = sectionOptions[attr]; });
            return this;
        }

        refreshIndent() {
            this._header.style.paddingLeft = (this.levelIndent * INDENT_IN_PX) + 'px'
        }

        set parent(p) {
            this._parent = p
        }
        get parent() {
            return this._parent
        }

        get levelIndent() {
            if (!this.parent) return 1
            return this.parent.levelIndent + 1
        }

        get titleText() {
            return this._title.textContent;
        }
        set titleText(value) {
            this._title.textContent = value;
        }

        get items(){
            return [...this._list.children]
        }

        clear() {
            for (var i = this._list.childNodes.length; i--;) this._list.firstChild.delete()
            this.updateSize()
        }

        delete() {
            let p = this.parent
            this.remove();
            delete this;
            if(p)p.updateSize()
        }

        get toggleable() {
            return this._toggleIcon !== undefined;
        }
        set toggleable(value) {
            if (value && !this._toggleIcon) {
                const toggleIcon = this._toggleIcon = document.createElement('span');
                toggleIcon.textContent = '▼'
                toggleIcon.className = 'collapse_icon'
                this._toggled = true;
                this._list.style.maxHeight = '0px';
                this._header.classList.add('collapsable')
                this._header.insertBefore(toggleIcon, this._title)
                this._header.addEventListener('click', (e) => {
                    if (e.target.localName !== 'a' && (!this.editable || e.target !== this._title)) this.toggle()
                })
            } else if (!value && this._toggleIcon) {
                this._toggleIcon.remove()
                this._header.classList.remove('collapsable')
                delete this._toggleIcon
            }
        }
        toggle() {
            if (!this.toggleable) return false;
            this._toggled = !this._toggled;
            if (this._toggled) this.updateSize();
            else this._list.style.maxHeight = '0px';

            //else this._list.style.maxHeight = this._list.scrollHeight+'px';
            this._toggleIcon.classList.toggle('active')
            return true;
        }

        get addable() {
            return this._btnAdd !== undefined;
        }
        set addable(itemBuilder) {
            if (itemBuilder && !this._btnAdd) {
                if (!this._btnSlot) { this._btnSlot = document.createElement('span'); this._btnSlot.classList.add('btn-slot'); this._header.appendChild(this._btnSlot); }
                const btnAdd = this._btnAdd = document.createElement('a');
                btnAdd.textContent = '+';
                this.itemBuilder = itemBuilder
                btnAdd.addEventListener('click', () => {
                    var item = this.itemBuilder()
                    this.addItem(item);
                    if (!item.editable && !item.valueText) item.editable = 'removeAfter'
                    if (item.displayEditMode && !item.valueText) item.displayEditMode()
                    if (this.toggleable && !this._toggled) this.toggle()
                })
                this._btnSlot.appendChild(btnAdd);
            } else if (!itemBuilder && this._btnAdd) {
                this._btnAdd.remove()
                delete this._btnAdd
            } else if (itemBuilder) {
                this._btnAdd.remove()
                delete this._btnAdd
                this.addable = itemBuilder
            }
        }

        get itemBuilder(){
            return this._itemBuilder
        }

        set itemBuilder(ib){
            this._itemBuilder = ib;
        }


        addItem(item) {
            item.parent = this;
            this._list.appendChild(item)
            this.updateSize()
            this.sort()
        }

        updateSize() {
            if (this.toggleable)
                if (this._toggled) this._list.style.maxHeight = null//this._list.scrollHeight + 'px';
            //if (this.parent) this.parent.updateSize()
        }


        get deletable() {
            return this._btnDel !== undefined;
        }

        set checkBeforeDelete(cb){
            this._checkBeforeDelete = cb
        }

        set deletable(callBack) {
            if (callBack && !this._btnDel) {
                if (!this._btnSlot) { this._btnSlot = document.createElement('span'); this._btnSlot.classList.add('btn-slot'); this._header.appendChild(this._btnSlot); }
                const btnDel = this._btnDel = document.createElement('a');
                btnDel.textContent = '×';
                btnDel.classList.add('close')
                btnDel.addEventListener('click',async () => {
                    if((typeof this._checkBeforeDelete === 'function')?await this._checkBeforeDelete():true){
                        this.remove();
                        delete this;
                        if(typeof callBack === 'function')callBack()
                    }
                })
                this._btnSlot.appendChild(btnDel);
            } else if (!callBack && this._btnDel) {
                this._btnDel.remove()
                delete this._btnDel
            }
        }

        get editable() {
            return this._input !== undefined;
        }
        set editable(callBackEdit) {
            if (callBackEdit && !this._input) {
                const input = this._input = document.createElement('input');
                input.style.display = "none"
                this._title.insertAdjacentElement('afterend', input);
                input.type = 'text'
                const editingValidate = () => {
                    if (this.titleText === '' && input.value === '') { this.remove(); return false; }
                    if (typeof callBackEdit === 'function') callBackEdit(this.titleText, input.value)
                    if (input.value !== '') this.titleText = input.value
                    this.hideEditMode()
                }
                input.addEventListener('keyup', (e) => { if (e.key === 'Enter') this.hideEditMode() })
                input.addEventListener('focusout', editingValidate)
                this._header.addEventListener('dblclick', () => { this.displayEditMode() })//this._content.addEventListener('dblclick',this.displayEditMode)

            } else if (callBackEdit) {
                this._input.remove()
                delete this._input
                this.editable = callBackEdit
            } else {
                this._input.remove()
                delete this._input
            }
        }
        displayEditMode() {
            if (!this.editable) return undefined
            this._input.value = this.titleText
            this._input.style.display = null
            this._title.style.display = 'none'
            this._input.focus()
        }
        hideEditMode() {
            if (this.editable) this._input.style.display = 'none'
            this._title.style.display = null
        }

        sort() {
            if(!this._disabledSorting){
                const items = this.items
                this._list.innerHTML = ''
                const sortedList = this._list.cloneNode(false); 
                if (!this.comparator)
                    items.sort(function (a, b) {
                        const rankA = a.rank ? a.rank : -4
                        const rankB = b.rank ? b.rank : -4
                        if (rankA !== rankB) return rankA > rankB ? -1 : 1;
                        else return a.valueText < b.valueText ? -1 : 1;
                    });
                else items.sort(this.comparator)

                for (let i = 0; i < items.length; i++) sortedList.appendChild(items[i]);
                this._list.parentNode.replaceChild(sortedList, this._list);
                this._list = sortedList
            }
        }

        displayIO() {
            this._list.childNodes.forEach((i) => i.displayIO())
        }
        hideIO() {
            this._list.childNodes.forEach((i) => i.hideIO())
        }

    });


    class SimpleItem extends HTMLElement {
        constructor() {
            super();
            const tmp = this._tmp = document.createElement('template')
            const content = this._content = document.createElement('div');
            const value = this._value = document.createElement('span');
            content.appendChild(value);
            tmp.appendChild(content)
        }

        connectedCallback() {
            if (!this.__connected) {
                for (var i = this._tmp.childNodes.length; i--;)  this.appendChild(this._tmp.firstChild)
                //for(let att of this._tmp.attributes) console.log(att.name,att.value)
                delete this._tmp;
                this.__connected = true
                this.refreshIndent()
            }
        }

        applyOptions(ItemOptions) {
            //.filter(i=> ['typeProvider','prefix2','type','title','addable','editable','deletable'].indexOf(i)!==-1)
            Object.keys(ItemOptions).forEach(attr => { this[attr] = ItemOptions[attr]; });
            return this;
        }

        refreshIndent() {
            this.style.paddingLeft = (this.levelIndent * INDENT_IN_PX) + 'px'
        }

        set parent(p) {
            this._parent = p
        }
        get parent() {
            return this._parent
        }

        get levelIndent() {
            if (!this.parent) return 1
            return this.parent.levelIndent + 1
        }
        delete() {
            let p = this.parent
            this.remove();
            delete this;
            if(p)p.updateSize()
        }

        get deletable() {
            return this._btnDel !== undefined;
        }

        set checkBeforeDelete(cb){
            this._checkBeforeDelete = cb
        }

        set deletable(callBack) {
            if (callBack && !this._btnDel) {
                const btnDel = this._btnDel = document.createElement('a');
                btnDel.textContent = '×';
                btnDel.addEventListener('click',async (e) => {
                    if((typeof this._checkBeforeDelete === 'function')?await this._checkBeforeDelete():true){
                        if (typeof callBack === 'function') callBack(this)
                        this.delete()
                        e.preventDefault()
                    }
                })
                this._content.appendChild(btnDel);
            } else if (!callBack && this._btnDel) {
                this._btnDel.remove()
                delete this._btnDel
            }
        }

        get intergrityCheck(){
            return this._intergrityCheck
        }
        set intergrityCheck(func) {
            this._intergrityCheck = func
        }


        get valueText() {
            return this._value.textContent;
        }
        set valueText(value) {
            if (this.intergrityCheck) value = this.intergrityCheck(this, value)
            this._value.textContent = value;
            if (this.parent) this.parent.sort()
        }
        set valueTextNoIntegrityCheck(value) {
            this._value.textContent = value;
            if (this.parent) this.parent.sort()
        }

        get type() {
            if (!this._type) return undefined
            return this._type.textContent
        }
        set type(value) {
            if ((value||typeof value ==='string') && this._type) {
                if(value.trim()!=='')this._type.textContent = value
                else this._type.innerHTML = "&nbsp;"
            } else if (value!==undefined && !this._type) {
                this.classList.add('typed')
                const type = this._type = document.createElement('span');
                if(value.trim()!=='')type.textContent = value
                else type.innerHTML = "&nbsp;"
                this._content.insertBefore(type, this._value);
                if (this.editable && !this._typeSelector) {
                    const typeSelector = this._typeSelector = document.createElement('select');
                    this._input.insertAdjacentElement('beforebegin', typeSelector);
                }
            } else if (value===undefined && this._type) {
                this._type.remove();
                this.classList.remove('typed')
                delete this._type;
                if (this._typeSelector) {
                    this._typeSelector.remove();
                    delete this._typeSelector;
                }
            }
        }

        get editable() {
            return this._containerEdit !== undefined;
        }
        set editable(callBackEdit) {
            if (callBackEdit && !this._containerEdit) {
                const containerEdit = this._containerEdit = document.createElement('div');
                containerEdit.style.display = "none"
                this.classList.add('editable')
                if (this.tmp) this.tmp.appendChild(containerEdit); else this.appendChild(containerEdit)
                if (this._type) {
                    const typeSelector = this._typeSelector = document.createElement('select');
                    if (this._typeProvider) typeSelector.addEventListener('click', this._typeProvider)
                    containerEdit.appendChild(typeSelector)
                }
                const input = this._input = document.createElement('input');
                input.type = 'text'
                containerEdit.appendChild(input)
                const editingValidate = () => {
                    if (this.valueText === '' && input.value === '') { this.remove(); return false; }
                    if (input.value !== ''){
                        this.valueText = input.value
                        if (this.callBackEdit) { this.callBackEdit(this); delete this.callBackEdit}
                        if (typeof callBackEdit === 'function')
                            callBackEdit(this)
                        else if (callBackEdit == 'removeAfter') {
                            if (this._containerEdit){
                                this._containerEdit.remove()
                                this.classList.remove('editable')
                                delete this._input
                                delete this._typeSelector
                                delete this._containerEdit
                            }
                        }
                    } else {
                        this._containerEdit.remove()
                        this.classList.remove('editable')
                        delete this._input
                        delete this._typeSelector
                        delete this._containerEdit
                    }
                    if (this._typeSelector) this.type = this._typeSelector.value
                    this.hideEditMode()
                }
                input.addEventListener('keyup', (e) => { if (e.key === 'Enter') this.hideEditMode() })
                this._containerEdit.addEventListener('focusout', (e) => { if (e.relatedTarget !== this._typeSelector && e.relatedTarget !== this._input ) editingValidate() })
                this._content.addEventListener('dblclick', () => { this.displayEditMode() })//this._content.addEventListener('dblclick',this.displayEditMode)
            } else if (callBack) {
                this._containerEdit.remove()
                delete this._input
                delete this._typeSelector
                delete this._containerEdit
                this.editable = callBack
            } else {
                this._containerEdit.remove()
                this.classList.remove('editable')
                delete this._input
                delete this._typeSelector
                delete this._containerEdit
            }
        }

        displayEditMode() {
            if (!this.editable) return undefined
            if (this._typeProvider && this._typeSelector) this._typeSelector.innerHTML = this._typeProvider()
            if (this._typeSelector) this._typeSelector.value = this.type
            this._input.value = this.valueText
            this._containerEdit.style.display = null
            this._content.style.display = 'none'
            this._input.focus()
        }
        hideEditMode() {
            if (this.editable) this._containerEdit.style.display = 'none'
            this._content.style.display = null
        }

        set typeProvider(provider) {
            this._typeProvider = () => {
                if (this._typeSelector)
                    return provider().map((t) => t.replaceAll('<', '&lt;').replaceAll('>', '&gt;')).reduce((p, t) => p + `<option value="${t}">${t}</option>`, '')
            }
        }

        get taggable() {
            return this._tagSlot !== undefined;
        }

        set taggable(bool) {
            if (bool && !this.taggable) {
                const tagSlot = this._tagSlot = document.createElement('span')
                tagSlot.className = "cl-marker"
                this._value.insertAdjacentElement('afterend', tagSlot);
            }
            else if (!bool && this.taggable) {
                this._tagSlot.remove()
                delete this._tagSlot
            }
        }

        hasTag(tag) {
            if (!this.taggable) return undefined
            return this._tagSlot.innerText.indexOf(tagChar[tag]) !== -1
        }

        addTag(tag) {
            if (!this.taggable) return undefined
            if (this.hasTag(tag)) return undefined
            if (this.ioable && tag === 'MODEL') this._modelCheckBox.checked = true
            if (this.ioable && tag === 'VIEWER') this._viewerCheckBox.checked = true
            this._tagSlot.innerText = [tagChar[tag], ...this._tagSlot.innerText.split(' ')].sort((a, b) => tagRank[a] > tagRank[b] ? -1 : 1).join(' ').trim()
            this.rank = this._tagSlot.innerText.split(' ').reduce((ac, val) => ac + tagRank[val], 0)
            this.rank = this.rank === tagRank[tagChar['CORE']]?-100:this.rank
        }

        removeTag(tag) {
            if (!this.taggable) return undefined
            if (!this.hasTag(tag)) return undefined
            if (this.ioable && tag === 'MODEL') this._modelCheckBox.checked = false
            if (this.ioable && tag === 'VIEWER') this._viewerCheckBox.checked = false

            this._tagSlot.innerText = this._tagSlot.innerText.replace(tagChar[tag], '').trim()
            this.rank = this._tagSlot.innerText.split(' ').reduce((ac, val) => ac + tagRank[val], 0)
            this.rank = this.rank === tagRank[tagChar['CORE']] ? -100 : this.rank
        }

        get ioable() {
            return this._ioSlot !== undefined;
        }

        set ioable(bool) {
            if (bool && !this.ioable) {
                const ioSlot = this._ioSlot = document.createElement('span')
                ioSlot.style.display = 'none'
                ioSlot.className = "cl-marker"
                this._value.insertAdjacentElement('afterend', ioSlot);
                ioSlot.appendChild(document.createTextNode(tagChar.MODEL+' '))
                const modelCheckBox = this._modelCheckBox = document.createElement('input')
                modelCheckBox.type = 'checkbox'
                modelCheckBox.checked = this.hasTag('MODEL')
                ioSlot.appendChild(modelCheckBox)
                if(!this.noviewTag){
                    ioSlot.appendChild(document.createTextNode(" "+tagChar.VIEWER+' '))
                    const viewerCheckBox = this._viewerCheckBox = document.createElement('input')
                    viewerCheckBox.type = 'checkbox'
                    viewerCheckBox.checked = this.hasTag('VIEWER')
                    ioSlot.appendChild(viewerCheckBox)
                }
            }
            else if (!bool && this.ioable) {
                this._ioSlot.remove()
                delete this._ioSlot
            }
        }

        displayIO() {
            if (!this.ioable) return undefined
            if (this.taggable) this._tagSlot.style.display = 'none'
            if (this.deletable) this._btnDel.style.display = 'none'
            this._ioSlot.style.display = null
        }

        hideIO() {
            if (!this.ioable) return undefined
            if (this.taggable) this._tagSlot.style.display = null
            if (this.deletable) this._btnDel.style.display = null
            this._ioSlot.style.display = 'none'
            if (this._modelCheckBox.checked) this.addTag("MODEL")
            else this.removeTag("MODEL")
            if (this._viewerCheckBox?.checked) this.addTag("VIEWER")
            else this.removeTag("VIEWER")
        }


        //editable
        //se démerrdé pour passer un callback qui donne la liste des type pour le click sur la liste pour qu'elle se refresh

        //taggable

        //ioable
    }
    customElements.define('custom-item', SimpleItem);//,{extends:'li'});


    customElements.define('select-gradient', class extends HTMLElement {
        constructor() {
            // Always call super first in constructor
            super();

            // Create a shadow root
            this.container = document.createElement('div');
            this.container.classList.add('gradientColor-select')

            //this.container.dataset.value = Object.keys(gradientPaletteCSS)[0]
            /*for each element, create a new DIV that will act as the selected item:*/
            this.currentSelected = document.createElement("div");
            this.currentSelected.classList.add('current')
            this.container.appendChild(this.currentSelected);
            //this.currentSelected.style.backgroundImage = 'linear-gradient(90deg,' + gradientPaletteCSS[container.dataset.value].join(',') + ')';
            /*for each element, create a new DIV that will contain the option list:*/
            this.listOptions = document.createElement("div");
            this.container.appendChild(this.listOptions);
            this.listOptions.style.display = 'none';
            this.currentSelected.addEventListener("click", (e) => {
                e.stopPropagation();
                this.listOptions.style.display = this.listOptions.style.display ? null : 'none';
                this.currentSelected.classList.toggle("select-arrow-active");
            });
            document.addEventListener("click", () => this.listOptions.style.display = 'none');
            this.reverseInput = document.createElement("span");
            this.reverseInput.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 21"><g fill="none" fill-rule="evenodd" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" transform="translate(3 3)"><path d="m6.5 6.5-4 4 4 4" /><path d="m14.5 10.5h-12" /><path d="m8.5.5 4 4-4 4" /><path d="m12.5 4.5h-12" /></g></svg>'
            this.reverseInput.classList.add('reverse')
            this.container.appendChild(this.reverseInput)
            this.reverseInput.addEventListener('click', () => this.isReverse = !this.isReverse)


            // Create some CSS to apply to the shadow dom
            const style = document.createElement('style');

            style.textContent = `
            
        div.gradientColor-select {
            position: relative;
            display: flex;
            flex-direction: row;
        }
        div.current {
            width:100%;
        }

        span.reverse{
            height:25px;
            width:25px;
            background: var(--bg-input);
            color: var(--theme-color);
            border-radius: 0 5px 5px 0px;
            cursor: pointer;
        }
        span.reverse:hover{
            background: var(--bg-input-hover);
            color: var(--text-2);
        }

        /*style the arrow inside the select element:*/
        div.current:after {
            position: absolute;
            content: "";
            top: 10px;
            right: 35px;
            width: 0;
            height: 0;
            border: 5px solid transparent;
            border-color: #fff transparent transparent transparent;
        }

        /*point the arrow upwards when the select box is open (active):*/
        div.current.select-arrow-active:after {
            border-color: transparent transparent #fff transparent;
            top: 5px;
        }

        /*style the items (options), including the selected item:*/
        div.gradientColor-select>div>div,
        div.gradientColor-select>div {
            cursor: pointer;
            user-select: none;
            height: 25px;
        }

        div.gradientColor-select>div>div{
            margin-top: 1px;
        }
        /*style items (options):*/
        div.gradientColor-select>div:nth-child(2) {
            position: absolute;
            top: 110%;
            left: 10px;
            right: 35px;
            z-index: 99;
        }
    `;
            const shadow = this.attachShadow({ mode: 'open' })
            // Attach the created elements to the shadow dom
            shadow.appendChild(style);
            shadow.appendChild(this.container);

        }

        setGradientOptions(gradientPaletteCSS) {
            this.gradientPaletteCSS = gradientPaletteCSS
            for (const palette in gradientPaletteCSS) {
                const option = document.createElement("div");
                this.listOptions.appendChild(option);
                option.style.backgroundImage = 'linear-gradient(90deg,' + gradientPaletteCSS[palette].join(',') + ')';
                option.dataset.value = palette
                option.addEventListener("click", (e) => {
                    this.value = palette
                    this.listOptions.style.display = 'none';
                    this.currentSelected.classList.remove("select-arrow-active");
                    this.container.click();
                });
            }
        }

        get isReverse() {
            return this.reverse
        }

        set isReverse(r) {
            this.reverse = r
            for (const opt of this.listOptions.children) opt.style.backgroundImage = this.generateGradientCSS(opt.dataset.value)
            if (this.value) this.value = this.value
        }

        get value() {
            return this.selected
        }

        set value(p) {
            this.currentSelected.style.backgroundImage = this.generateGradientCSS(p)
            this.selected = p
            if (this.onChangePalette) this.onChangePalette(p)
        }

        set onchange(cb) {
            this.onChangePalette = cb
        }

        generateGradientCSS(palette) {
            return 'linear-gradient(' + (this.isReverse ? '-' : '') + '90deg,' + this.gradientPaletteCSS[palette].join(',') + ')';
        }

    });

}