import projectManager from './ProjectManager';
import { FileExplorer, FilesViewer, ResourceMonitor, RunInBackground, RunningNotebooks, modalDelete } from './serverFilesManager';



function dropJSON(targetEl, callback) {
    // disable default drag & drop functionality
    function preventDefaults(e) {
        e.preventDefault()
        e.stopPropagation()
    }
    targetEl.addEventListener('dragenter', (e) => { targetEl.classList.add('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragover', (e) => { targetEl.classList.add('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragend', (e) => { targetEl.classList.remove('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragleave', (e) => { targetEl.classList.remove('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragover', (e) => { preventDefaults(e); }, false);

    targetEl.addEventListener('drop', function (event) {
        targetEl.classList.remove('hoverDrag')
        event.dataTransfer.dropEffect = 'copy';
        for (let f of event.dataTransfer.files) {
            if(f.type.includes('ipynb')){
                var reader = new FileReader();
                reader.onloadend = (e) => {
                    var data = JSON.parse(e.target.result)
                    callback(f.name,data);
                };
                reader.readAsText(f);
            }
        }
        event.preventDefault();
    }, false);
}


let iditem = 0

/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class NotebookManager {

    // Singleton initialisation
    constructor() { }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer, defaultData) {

        this.notebookDOM = document.createElement('div')
        this.notebookDOM.id = 'notebookGUI'
        this.notebookDOM.style = "height:100%; width:100%;"

        document.getElementById('contentView').appendChild(this.notebookDOM)

        this.fileViewer = new FilesViewer(this.notebookDOM)
        

        this.container = document.createElement('div')
        this.container.id = 'notebookActivity'
        this.container.style.width = '100%'
        this.container.style.height = '100%'
        mainContainer.appendChild(this.container)
        this.fileItemBuilder = (name,codedata) => {
            let file = document.createElement('custom-item')
            file.editable = true
            if(name) file.valueText = name
            file.checkBeforeDelete = ()=>modalDelete.showConfirmation(false,file.valueText)
            file.deletable = () => {
                if (this.fileViewer.isOpen(file)) this.fileViewer.close(file)
            }
            file.codedata = codedata ? codedata : ''
            file.intergrityCheck = (_,txt) => { return txt.endsWith(".ipynb") ? txt : (txt + '.ipynb') }
            file._value.addEventListener('click', (e) => {
                if(file.valueText!=='')
                    if (this.fileViewer.isOpen(file))
                        this.fileViewer.select(file)
                    else this.fileViewer.open(file, JSON.parse(JSON.stringify(file.codedata)), { save: (json)=>{file.codedata=json;projectManager.save(true);}})
            })
            file._content.className = 'icon-file-notebook color5'
            const id = iditem
            iditem++
            file.getFullPath = ()=>"###"+id+'###'
            return file
        }
        this.customList = document.createElement('custom-list')
        this.container.appendChild(this.customList)

        this.monitor = new ResourceMonitor()
        this.customList.appendChild(this.monitor.section)

        this.notebookSection = this.customList.addSection("notebook files", {
            addable: () => this.fileItemBuilder(),
            toggleable: true,
            deletable: false,
        })
        this.notebookSection.classList.add('title')


        this.fileExplorer = new FileExplorer(this.fileViewer)
        this.customList.appendChild(this.fileExplorer.root)

        this.runInBackground = new RunInBackground(this.fileViewer)
        this.fileViewer.setRunInBackgroundInstance(this.runInBackground)
        this.customList.appendChild(this.runInBackground.mainSection)

        this.runningNotebooks = new RunningNotebooks()
        this.customList.appendChild(this.runningNotebooks.mainSection)


        dropJSON(this.notebookSection, (file,codedata) => {
            this.notebookSection.addItem(this.fileItemBuilder(file, codedata))
        });
    }


    getValues() {
        let data = {};
        this.notebookSection.items.forEach(file => {
                data[file.valueText] = file.codedata
        })
        return data
    }
    onClickActivity() {
        this.fileExplorer.refresh()
        this.runInBackground.refresh()
        this.monitor.startAutoRefresh(10000)
    }
    onLeaveActivity() {
        this.monitor.stopAutoRefresh()
    }

    importNotebooks(data) {
        this.clear()
        let sec = this.notebookSection
        for (const [file, codedata] of Object.entries(data)) {
            sec.addItem(this.fileItemBuilder(file,codedata))
        }
    }

    clear() {
        //document.getElementById('notebookGUI').innerHTML = ""
        let elements = document.getElementById('notebookGUI').getElementsByClassName('notebook');
        while (elements.length > 0) elements[0].parentNode.removeChild(elements[0]);
        this.notebookSection.clear()
    }

    addUpdater(fun) {
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update() {

    }

}

const notebookManager = new NotebookManager();
export default notebookManager;