import exportJsonProject from "../inputs_outputs/exportJsonProject";
import loadDiagram from "../inputs_outputs/loadDiagram";
import ActivityManager from "./ActivityManager";
import Parameters from "./ParametersManager";
import { fetchW, getLoader } from "../utils/misc";
import projectManager from "./ProjectManager";
import getFinalJson from "../inputs_outputs/getFinalJson";
import Swal from "sweetalert2";
import '../css/demoProject.css'
import stateMachine from "../diagrams/stateMachine";
function generateCard(demo){
    const domCard = document.createElement('div')
    domCard.classList.add('svgCard')
    domCard.classList.add(demo.D3?'D3':'D2')
    domCard.innerHTML =`${demo.svg}<p class="header-svgCard">${demo.name}</p><p>${demo.description.replace('\\n','<br>')}</p></div>`
    domCard.querySelector('svg').addEventListener('click',()=>{
        fetchW('/api/getDemo/'+demo.name, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            projectManager.loadProject(data.xml)
        })
    })
    return domCard
}

function generateAddDemoCard(){
    const domCard = document.createElement('div')
    domCard.classList.add('svgCard')
    domCard.innerHTML = `<svg fill="var(--text-2)" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-5 -5 55.402 55.402"><g><path d="M 41.267,20.961767 H 24.427233 V 4.134 C 24.427233,1.851 24.749353,0.02786099 22.707,0 20.736656,-0.02687866 21.042578,1.8510098 21.035862,4.135 V 20.971767 H 4.141 c -2.283,0 -4.139,-0.289243 -4.138,1.730233 0.00424757,1.75204 1.9659404,1.57307 4.127,1.606947 H 21.035862 V 41.27 c 0.04201,2.916712 -0.33442,4.177802 1.667138,4.133 2.089619,0 1.717573,-1.85101 1.724233,-4.133 V 24.307947 H 41.267 c 2.283,0 4.158515,0.285139 4.133,-1.600947 -0.02778,-2.053493 -1.85201,-1.738718 -4.133,-1.745233 z"/></g></svg>
                         <p>Save current Project as new demo</p>`
    domCard.querySelector('svg').addEventListener('click', async e=>{
        let { value: [name,description,D3] } = await Swal.fire({
            title: 'Name of the demo file',
            html:
                '<input id="generateDemoName" class="swal2-input">' +
                '<textarea id="generateDemoDescription" class="swal2-textarea" style="min-height:150px;min-width:300px;max-width:300px"></textarea>' +
                '<label for="generateDemoIs3D" style="display: flex;justify-content: center;"><input type="checkbox" value="1" id="generateDemoIs3D"><span class="swal2-label">3D</span></label>',
            focusConfirm: false,
            preConfirm: () => {
                return [
                    document.getElementById('generateDemoName').value,
                    document.getElementById('generateDemoDescription').value,
                    document.getElementById('generateDemoIs3D').checked
                ]
            },
            showCancelButton: true,
        })
        if (name) {
            const loader = getLoader()
            loader.setText('Demo generation ...');
            loader.show();
            name = name.replaceAll(' ', '_')
            let xml = await exportJsonProject(false)
            xml.build.build[xml.build.currentBuild].failedCompile = false
            xml.build.build[xml.build.currentBuild].failedCompileExplore = true
            xml.build.build[xml.build.currentBuild].changeDetected =false
            xml.build.build[xml.build.currentBuild].changeDetectedXML =false
            xml = JSON.parse(JSON.stringify(xml).replaceAll(Parameters.getCurrentItemBuild().token, name).replaceAll("changeDetected&#34;:true", "changeDetected&#34;:false"))
            const finalJson = getFinalJson()
            ActivityManager.toggleActivity('cells_behavior')
            const svg = await stateMachine.modeler.saveSVG()
            fetchW("/api/generateDemo", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ finalJson, xml, svg:svg.svg , name, description,D3: D3?true:false  })
            }).then(_=>loader.hide())
        }
    })
    return domCard
}

class DemosManager {
    constructor(){

        this.viewContainer = document.createElement('div')
        this.viewContainer.id = 'DemosManagerGUI'
        this.viewContainer.classList.add("svgCardContainer")
        document.getElementById('contentView').appendChild(this.viewContainer)

    }

    init(defaultData){
        if(defaultData.User.username === 'admin')
        this.viewContainer.appendChild(generateAddDemoCard())

        defaultData['Demos'].forEach(demo => {
            this.viewContainer.appendChild(generateCard(demo))
        });
        let demoName = (new URLSearchParams(document.location.search)).get('demo')
        let listAvailableDemoName = defaultData['Demos'].map(a=>a.name)
        //console.log(defaultData['Demos'])
        if (listAvailableDemoName.includes(demoName))
            fetchW('/api/getDemo/' + demoName, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then(res => res.json()).then((data => {
                loadDiagram(data.xml, false)
                this.domDemosSelector.style.display = 'none'
                this.domLoggin.classList.add('hide')
            }))
    }
}


const demoManager = new DemosManager()

export default demoManager
