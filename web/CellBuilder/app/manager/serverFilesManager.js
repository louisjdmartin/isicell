import { file } from 'jszip'
import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import { quickToast } from '../guiComponent/notification'
import Parameters from './ParametersManager'
import { Notebook } from '../guiComponent/notebook'
import ModalWindows from '../guiComponent/modalWindow'
import ContextMenu from '../guiComponent/contextMenu'
import { fetchW, utf8_to_b64 } from '../utils/misc'
import { io } from "socket.io-client";
import { Terminal } from "xterm";
import { FitAddon } from 'xterm-addon-fit';
import { WebglAddon } from 'xterm-addon-webgl';
import "xterm/css/xterm.css"; // DO NOT forget importing xterm.css
import PanelManager from '../guiComponent/panelManager'
let termnialCount = 0;
function userFolder(action, data, cb) {
    
    fetchW('/api/userFolder/' + action, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, signal: AbortSignal.timeout(5000),
        body: JSON.stringify(data)
    }).then(res => res.json()).then(data =>{ if(!data.err)cb(data)}).catch(err=>console.error(err))
}
 
function createModalDeleteFile() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode("Are you sure you want to delete the folder: \"\"?")
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const yes = document.createElement('a')
    const no = document.createElement('a')
    yes.innerText = "Yes"
    no.innerText = 'No'
    yes.style.backgroundColor = '#e74c3c'
    no.style.backgroundColor = '#2ecc71'

    footer.appendChild(no); footer.appendChild(yes);
    const modal = new ModalWindows('Confirmation', div)
    modal.callbackFocusOut = modal.callbackClose = ()=>{modal?.resolve(false);modal.resolve = undefined}
    modal.contentAndHeader.style.width = '450px'

    modal.showConfirmation = (isFolder,name) =>{
        msgNode.nodeValue = `Are you sure you want to delete the ${isFolder?'folder':'file'}: "${name}"?`
        modal.show()
        yes.focus()
        return new Promise((resolve, reject) => {
            modal.resolve = resolve
        })
    }

    yes.addEventListener('click', () => {
        modal?.resolve(true)
        modal.resolve = undefined
        modal.hide()
    })
    no.addEventListener('click', () => {
        modal?.resolve(false)
        modal.resolve = undefined
        modal.hide()
    })
    return modal
}


function createModalCloseWithoutSave() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode("Your changes will be lost if you don'tsave them.")
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const dontsave = document.createElement('a')
    const cancel = document.createElement('a')
    const save = document.createElement('a')
    dontsave.innerText = "Don't Save"
    cancel.innerText = "Cancel"
    save.innerText = 'Save'
    dontsave.style.backgroundColor = '#e74c3c'
    cancel.style.backgroundColor = '#10499e'
    save.style.backgroundColor = '#2ecc71'

    footer.appendChild(dontsave); footer.appendChild(cancel); footer.appendChild(save);
    var modal = new ModalWindows('Do you want to save the changes you made ?', div)
    modal.contentAndHeader.style.width = '450px'
    dontsave.addEventListener('click', () => {
        modal.hide()
        modal.cbClose()
    })
    cancel.addEventListener('click', () => {
        modal.hide()
    })
    save.addEventListener('click', () => {
        modal.hide()
        modal.cbSave()
    })
    return modal
}

const modalDelete = createModalDeleteFile();

export {modalDelete};

const modalClose = createModalCloseWithoutSave();

function splitPath(path){
    let i = path.lastIndexOf('/') + 1
    return [path.slice(0, i), path.slice(i)]
}

let headerPythonFile = "###### header to import current project #####\nimport sys\nsys.path.append(sys.argv[1])\nfrom build.swig.isiCellPy import *\n#############################################"

function dropFiles(targetEl, callbackSuccess,callbackError) {
    // disable default drag & drop functionality
    function preventDefaults(e) {
        e.preventDefault()
        e.stopPropagation()
    }
    targetEl.addEventListener('dragenter', (e) => { targetEl.classList.add('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragover', (e) => { targetEl.classList.add('hoverDrag'); preventDefaults(e); }, false);
    targetEl.addEventListener('dragend', (e) => { targetEl.classList.remove('hoverDrag'); preventDefaults(e); },false);
    targetEl.addEventListener('dragleave', (e) => { targetEl.classList.remove('hoverDrag'); preventDefaults(e); },false);
    targetEl.addEventListener('dragover', (e) => { preventDefaults(e); },false);

    targetEl.addEventListener('drop', function (event) {
        targetEl.classList.remove('hoverDrag')
        event.dataTransfer.dropEffect = 'copy';
        let formData = new FormData()
        formData.append("path", targetEl.getFullPath());
        for (let f of event.dataTransfer.files) {
            formData.append('files', f)
        }
        fetchW('/api/uploadFiles', {method: 'POST',body: formData}).then(callbackSuccess).catch(callbackError)
        preventDefaults(event);
    },false);
}

function colorizeFile(name,dom){
    if (name === "Terminal") dom.className = 'icon-terminal'
    else if (name.endsWith('.py')) dom.className = 'icon-file-code color4'
    else if (name.endsWith('.json')) dom.className = 'icon-file-json color3'
    else if (name.endsWith('.ipynb')) dom.className = 'icon-file-notebook color5'
    else if (name.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)) dom.className = 'icon-file-image color6'
    else if (name.endsWith('.csv')) dom.className = 'icon-file-table color2'
    else dom.className = 'icon-file'
}
export class FileExplorer {

    constructor(filesViewer) {
        this.root = document.createElement('custom-section')
        this.root.toggleable = true
        this.copyItem = undefined
        this.cutItem = undefined
        this.contextMenu = new ContextMenu({
            "rename": ["icon-rename",(item) => {
                this.contextMenu.cb_cancel();
                item.displayEditMode()
            }],
            "copy":["icon-copy", (item) => {
                this.contextMenu.cb_cancel()
                if (this.cutItem) this.cutItem.classList.remove('dashed')
                if (this.copyItem) this.copyItem.classList.remove('dashed')
                this.copyItem = item
                this.cutItem = undefined;
                this.copyItem.classList.add('dashed')
                this.contextMenu.pastBtn.style.display = null
                // this.contextMenu.cb_cancel()
                // this.contextMenu.cb_valid = (i) => (i._content|i._header).style.border = null
            }],
            "cut":["icon-cut", (item) => {
                this.contextMenu.cb_cancel()
                if (this.cutItem) this.cutItem.classList.remove('dashed')
                if (this.copyItem) this.copyItem.classList.remove('dashed')
                this.cutItem = item
                this.copyItem = undefined;
                this.cutItem.classList.add('dashed')
                this.contextMenu.pastBtn.style.display = null

                // this.contextMenu.cb_valid()
                // this.contextMenu.cb_valid = () => (item._content | item._header).style.border = null

            }],
            "past": ["icon-paste", (item) => {
                this.contextMenu.cb_cancel();
                if (item.tagName !== 'CUSTOM-SECTION') item = item.parent
                const fromItem = this.copyItem || this.cutItem
                let dest;
                let copyPath = this.copyItem?.getFullPath()
                let cutPath = this.cutItem?.getFullPath()
                console.log(copyPath,cutPath)
                let fileName = (copyPath?copyPath.slice(copyPath.lastIndexOf('/')+1):cutPath.slice(cutPath.lastIndexOf('/')+1))
                const listNames = item.items.filter((i) => i.tagName === fromItem.tagName).map((i) => i.valueText || i.titleText)
                console.log(fileName,listNames)
                while (listNames.includes(fileName)) {
                    if(fileName.includes('.')){
                        const lastDotPos = fileName.lastIndexOf('.');
                        fileName = fileName.slice(0, lastDotPos)+' copy'+fileName.slice(lastDotPos)
                    } else {
                        fileName += ' copy'
                    }
                }
                dest = item.getFullPath()+'/'+fileName
                console.log(dest)
                if(this.copyItem){
                    userFolder('copy', { path: copyPath, newPath:dest }, (err) => {
                        if (err.err) {
                            quickToast.fire({ icon: 'error', title: 'copy error' })
                        } else if (this.copyItem !== dest) {
                            const [path,nameF] = splitPath(dest)
                            if (this.copyItem.tagName === 'CUSTOM-SECTION'){
                                this.refresh()
                            } else {
                                let newFile = this._buildFileItem(path,nameF)
                                item.addItem(newFile)
                            }
                        } else {
                            this.refresh()
                        }
                        if (this.cutItem) this.cutItem.classList.remove('dashed')
                        if (this.copyItem) this.copyItem.classList.remove('dashed')
                        this.cutItem = undefined
                        this.copyItem = undefined
                        this.contextMenu.pastBtn.style.display = 'none'
                    })
                } else if (this.cutItem) {
                    if(cutPath!==dest)
                    userFolder('move', { path: cutPath, newPath:dest }, (err) => {
                        if (err.err) {
                            quickToast.fire({ icon: 'error', title: 'move error' })
                        } else {
                            this.filesViewer.rename(item, cutPath);
                            item.addItem(this.cutItem);
                            this.cutItem.refreshIndent();
                        }
                        if (this.cutItem) this.cutItem.classList.remove('dashed')
                        if (this.copyItem) this.copyItem.classList.remove('dashed')
                        this.cutItem = undefined
                        this.copyItem = undefined
                        this.contextMenu.pastBtn.style.display = 'none'
                    })
                    else {
                        if (this.cutItem) this.cutItem.classList.remove('dashed')
                        if (this.copyItem) this.copyItem.classList.remove('dashed')
                        this.cutItem = undefined
                        this.copyItem = undefined
                        this.contextMenu.pastBtn.style.display = 'none'
                    }
                } else {
                    if (this.cutItem) this.cutItem.classList.remove('dashed')
                    if (this.copyItem) this.copyItem.classList.remove('dashed')
                    this.cutItem = undefined
                    this.copyItem = undefined
                    this.contextMenu.pastBtn.style.display = 'none'
                }
            }],
            "download": ["icon-download", (item) => {
                this.contextMenu.cb_cancel()
                let link = document.createElement("a");
                link.download = (item.valueText || item.titleText);
                link.href = "/api/userFolder/download/" + encodeURIComponent(utf8_to_b64(item.getPath())) + "/" + encodeURIComponent(link.download);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }],
            "delete": ["icon-trash",(item)=>{
                item._btnDel?.click()
            }]
        });
        [...this.contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'delete')[0].style.color = 'var(--empty-activity)'
        this.contextMenu.pastBtn = [...this.contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'past')[0]
        this.contextMenu.pastBtn.style.display = 'none'
        this.contextMenu.cb_cancel = () => { this.contextMenu.target.classList.remove('dashed-theme'); }
        this.root.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            let div = e.target.closest('custom-section, custom-item')
            if (div && div !== this.root) {
                this.contextMenu.target = div
                this.contextMenu.target.classList.add('dashed-theme');
                this.contextMenu.show(e.pageX, e.pageY)
            }
        })
        this.filesViewer = filesViewer
        this._buildFolderItem('.', [])
    }

    refresh(){
        userFolder('refresh', {}, (data) => {
            //TODO: add save of opened folder
            this.root.clear();
            this._buildFolderItem('.', data.files.files)
        })
    }

    _buildFileItem(path, name){
        let file = document.createElement('custom-item')
        if (name) {
            file.valueText = name
            file.prevValueText = name
        }
        file.getPath = () => (file.parent ? file.parent.getFullPath() : path)+'/'
        file.getFullPath = () => file.getPath() + file.valueText
        file.editable = () => {
            if (file.prevValueText) {
                if (file.prevValueText !== file.valueText) {
                    let newPath = file.getFullPath()
                    let oldPath = file.getPath() + file.prevValueText
                    userFolder('rename', { path:oldPath, newPath }, (err) => {
                        if (err.err) {
                            file.valueText = file.prevValueText
                            quickToast.fire({ icon: 'error', title: 'rename error' })
                        } else {
                            file.prevValueText = file.valueText
                            this.filesViewer.rename(file,oldPath)
                        }
                    })
                }
            } else {
                userFolder('write', { path: file.getFullPath(), data: file.valueText.endsWith('.py')?headerPythonFile:'' }, (err) => {
                    if (err.err){
                         file.remove()
                        quickToast.fire({ icon: 'error', title: 'new file error' })
                    } else {
                        file.prevValueText = file.valueText
                    }
                })
            }
            colorizeFile(file.valueText,file._content);
        }
        file.checkBeforeDelete = ()=>{return modalDelete.showConfirmation(false,file.valueText)}
        file.deletable = ()=>{
            userFolder('delete', { path: file.getFullPath() }, (err) => {
                if (err.err) {
                    quickToast.fire({ icon: 'error', title: 'delete file error' })
                    this.refresh()
                } else {
                    if (this.filesViewer.isOpen(file))this.filesViewer.close(file)
                    file.remove()
                }
            })
        } 
        file._value.addEventListener('click', (e) => {
            let pathFile = file.getFullPath()

            if (!this.filesViewer.isOpen(file))
                userFolder('read', { path: pathFile }, (data) => {
                    if (!data.err) {
                        const isIMG = file.valueText.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)
                        this.filesViewer.open(file,data.data,{save:isIMG?false:true,type:isIMG?'img':'txt',runInBackground:pathFile.endsWith('.py'),addParameters:pathFile.endsWith('.py'),reload:!pathFile.endsWith('.py')})
                    } else {
                        quickToast.fire({ icon: 'error', title: 'read file error' })
                    }
                })
            else {
                this.filesViewer.select(file)
            }
        })
        this.filesViewer.updateDomItem(file)
        colorizeFile(file.valueText, file._content);
        return file
    }

    _buildFolderItem(pathWithName, content) {
        let dir = (pathWithName === '.') ? this.root : document.createElement('custom-section')
        dir.comparator = (a, b) => {
            if (a.titleText && b.titleText) return a.titleText < b.titleText ? -1 : 1
            if (a.titleText || b.titleText) return a.titleText ? -1 : 1
            return a.valueText < b.valueText ? -1 : 1
        }
        const [path, name] = splitPath(pathWithName)
        dir.titleText = (pathWithName === '.')?'server files':name
        if (pathWithName !== '.'){
            dir.getPath = () => (dir.parent ? dir.parent.getFullPath()+'/' : path)
            dir.getFullPath = () => dir.getPath() + dir.titleText
        } else {
            dir.getPath = () => '.'
            dir.getFullPath = () => '.'
        }

        if (pathWithName === '.' && !this.root.alreadyInit) {
            dir.deletable = true
            dir.classList.add('title')
            let btnRefresh = document.createElement('a')
            btnRefresh.classList.add('icon-reload')
            btnRefresh.addEventListener('click', () => {
                this.refresh()
            })
            dir._btnSlot.insertBefore(btnRefresh, dir._btnSlot.firstChild);
            dir.deletable = false
        }

        if (pathWithName !== '.'){
            dir.toggleable = true
            dir.checkBeforeDelete = ()=>{return modalDelete.showConfirmation(true,dir.titleText)}
            dir.deletable = () => {
                userFolder('delete', { path: dir.getFullPath() }, (err) => {
                    if (err.err) {
                        quickToast.fire({ icon: 'error', title: 'delete folder error' })
                        this.refresh()
                    } else {
                        this.filesViewer.closeAllPathStartWith(dir.getFullPath())
                    }
                })
            }
            dir.toggle();
            dir.editable = (prevVal, newVal) => {
                if (prevVal !== newVal)
                if (prevVal) {
                    let oldPath = dir.getFullPath()
                    let newPath = dir.getPath() + newVal 
                    userFolder('rename', { path: oldPath, newPath }, (err) => {
                        if (err.err) {
                            dir.titleText = prevVal
                            quickToast.fire({ icon: 'error', title: 'rename folder error' })
                        } else {
                            renameAllPathStartWith(oldPath, newPath)
                        }
                    })
                } else {
                    userFolder('newDir', { path: dir.getPath() + newVal }, (err) => {
                        if (err.err) {
                            quickToast.fire({ icon: 'error', title: 'new dir error' })
                            dir.remove()
                        }
                    })
                }
            }
        }
        if (pathWithName !== '.' || !this.root.alreadyInit) {
            let btnAddDir = document.createElement('a')
            btnAddDir.classList.add('icon-folder')
            btnAddDir.addEventListener('click', () => {
                let newDir = this._buildFolderItem(dir.getFullPath()+'/' , [])
                dir.addItem(newDir)
                newDir.displayEditMode()
            })
            dir._btnSlot.insertBefore(btnAddDir, dir._btnSlot.firstChild);

            let btnAddFile = document.createElement('a')
            btnAddFile.classList.add('icon-file')
            btnAddFile.addEventListener('click', () => {
                let newFile = this._buildFileItem(dir.getFullPath())
                dir.addItem(newFile)
                newFile.displayEditMode()
            })
            dir._btnSlot.insertBefore(btnAddFile, dir._btnSlot.firstChild);
            dropFiles(dir, () => { this.refresh(); console.log('SUCESS') }, (e) => { quickToast.fire({ icon: 'error', title: 'upload files' }) })
        }


        if (pathWithName === '.') dir.alreadyInit = true
        for (let f of content) {
            if (typeof f === 'object') {
                dir.addItem(this._buildFolderItem('./'+f.path,f.files))
            } else {
                dir.addItem(this._buildFileItem(dir.getFullPath(),f))
            }
        }
        return dir
    }
}

export class FilesViewer {
    constructor(container) {
        this.openFiles = {}
        PanelManager.createSplitterContainer('notebookGUI', 'osef111', true)
        this.panelManager = new PanelManager('osef111',true);
        this.splitCount = 0
        this.panelList = {}
        this.container = container
        this.newPanel(true)
        window.fv = this
    }

    newPanel(isMainPanel=false){
        const id = isMainPanel?'FVmainPanel':"FVpanel"+(this.splitCount++)
        this.panelList[id] = []
        const panel = this.panelManager.createPanel(id);

        const headerWrapper = document.createElement('div')
        headerWrapper.className = "header-files-viewer"

        headerWrapper.addEventListener("wheel", (evt) => {
            evt.preventDefault();
            if(Math.abs(evt.deltaY) > Math.abs(evt.deltaX)) headerWrapper.scrollLeft += evt.deltaY;
            else headerWrapper.scrollLeft += evt.deltaX;
        });
        panel.contextMenu = new ContextMenu({
            "split": ["icon-columns",(item) => {
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===item)[0]
                const info = this.openFiles[itemPath]
                const listPanel = Object.keys(this.panelList)
                const idxPanel = listPanel.indexOf(info.panel.id)
                const panelId = listPanel[idxPanel]
                
                const currentListPanelItem = this.panelList[panelId]
                if (info.domTab.classList.contains('selected')){
                    let i =currentListPanelItem.indexOf(itemPath)
                    if(i > 0)i--;else i++;
                    if (currentListPanelItem.length > 1) this.select(this.openFiles[currentListPanelItem[i]].domListItem)
                }
                this.panelList[panelId].splice(this.panelList[panelId].indexOf(itemPath),1)

                if(idxPanel === listPanel.length-1){
                    this.newPanel()
                } else {
                    this.currentPanel = document.getElementById(listPanel[idxPanel+1])
                }
                this.currentPanel.header.appendChild(info.domTab)
                this.currentPanel.contentTab.appendChild(info.domView)
                this.panelList[this.currentPanel.id].push(itemPath)
                info.panel = this.currentPanel

                this.select(info.domListItem)
                this.cleanEmptyPanel()
            }],
        })
        panel.newTerminal = new ContextMenu({
            "new Terminal": ["icon-terminal",(item) => {
                const fakePath = ">>>"+(termnialCount++)+".term"
                this.currentPanel = item
                this.open({valueText:'Terminal',getFullPath:()=>fakePath})
            }],
        })
        headerWrapper.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            const tab = e.target.closest('.tab')
            if(e.target===headerWrapper){
                panel.newTerminal.target = panel
                panel.newTerminal.show(e.pageX,e.pageY)
            }else if (tab) {
                panel.contextMenu.target = tab
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===tab)[0]
                panel.contextMenu.show(e.pageX, e.pageY)
            }
        })
        
        panel.header = document.createElement('div')
        panel.header.style ="width: max-content; height:40px;isplay: inline-block;"
        headerWrapper.appendChild(panel.header)



		panel.header.addEventListener('dragstart', (e) => {
            if(isMainPanel && this.panelList[id].length<=1){
                e.preventDefault();
                return;
            }
			let tabsContainer0 = panel.header;
			let dragStartIndex = Array.from(tabsContainer0.children).indexOf(e.target);
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/plain', null);
			e.target.classList.add('dragging');
			const draggingTab = e.target
		    const allTab = document.getElementById('notebookGUI').querySelectorAll('.header-files-viewer')
			const ondragover = (e) => {
			    e.preventDefault();
                const overwraper =e.target.classList.contains('header-files-viewer')
                if(overwraper){
                    e.target.firstChild.appendChild(draggingTab)
                    dragStartIndex = Array.from(e.target.firstChild.children).indexOf(draggingTab);
                    tabsContainer0 = e.target.firstChild
                    return;
                }
			    const tabsContainer = e.target.closest('.header-files-viewer > div');
			    const dragOverTab = e.target.closest('.tab');
			    if(tabsContainer0 !== tabsContainer) dragStartIndex = 0
			    if (!draggingTab || !dragOverTab || draggingTab === dragOverTab) return;

			    const dropIndex = Array.from(tabsContainer.children).indexOf(dragOverTab);
			    
			    if (dragStartIndex < dropIndex) tabsContainer.insertBefore(draggingTab, dragOverTab.nextSibling);
			    else tabsContainer.insertBefore(draggingTab, dragOverTab);
			    
			    dragStartIndex = Array.from(tabsContainer.children).indexOf(draggingTab);
			    tabsContainer0 = tabsContainer
			}
			const ondragend = () => {
			    if (draggingTab) draggingTab.classList.remove('dragging');
				allTab.forEach(t=>{
					t.ondragover = null
					t.ondragend = null
				})
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===draggingTab)[0]
                if(panel.header !== tabsContainer0){
                    const targetPanel = tabsContainer0.closest('.panel-split-h')
                    const info = this.openFiles[itemPath]
                    const isSelected =info.domTab.classList.contains('selected')
                    if (isSelected){
                        let i =this.panelList[id].indexOf(itemPath)
                        if(i > 0)i--;else i++;
                        if (this.panelList[id].length > 1) this.select(this.openFiles[this.panelList[id][i]].domListItem)
                    }
                    this.panelList[id].splice(this.panelList[id].indexOf(itemPath),1)
                    this.panelList[targetPanel.id].splice(dragStartIndex,0,itemPath)
                    info.panel = targetPanel
                    targetPanel.contentTab.appendChild(info.domView)
                    this.currentPanel = targetPanel
                    if(isSelected)this.select(info.domListItem)
                    this.cleanEmptyPanel()
                } else {
                    this.panelList[id].splice(this.panelList[id].indexOf(itemPath),1)
                    this.panelList[id].splice(dragStartIndex,0,itemPath)
                }
			}
			allTab.forEach(t=>{
				t.ondragover = ondragover
				t.ondragend = ondragend
			})
		})


        panel.contentTab = document.createElement('div')
        panel.contentTab.classList.add('container-file-viewer')
        panel.appendChild(headerWrapper)
        panel.appendChild(panel.contentTab)
        this.currentPanel = panel
        this.updateSizePanel()
    }

    updateSizePanel(){
        const listPanel = Object.keys(this.panelList)
        const pc = 100/listPanel.length
        this.panelManager.setWidthPanel(listPanel.map(_=>pc),false)
    }

    
    open(itemFile,dataContent,options={}){
        const itemPath = itemFile.getFullPath()
        if (!this.openFiles.hasOwnProperty(itemPath)){
            const domTab = document.createElement('div')
            domTab.className = 'tab'
            domTab.draggable = true
            const domFileName = document.createElement('span')
            domFileName.innerText = itemFile.valueText
            colorizeFile(itemFile.valueText, domFileName)
            domTab.appendChild(domFileName)
            const closeBtn = document.createElement('a')
            closeBtn.innerText = '×'
            domTab.appendChild(closeBtn)
            let domView = document.createElement('div')
            this.currentPanel.contentTab.appendChild(domView)
            const info = { domTab, domView, domListItem: itemFile }

            let remove;
            let save;
            let reload;
            if(itemFile.valueText.endsWith('.ipynb')){
                let callBackSave;
                let setEdited;
                if(options.save){
                    if (typeof options.save === 'function') callBackSave = (json,cb) => { options.save(json); info.domTab.classList.remove('edited');if(cb)cb(); }
                    else callBackSave = (data,cb)=>{
                            userFolder('write', { path:info.domListItem.getFullPath(), data: JSON.stringify(data) }, (err) => {
                                if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                                else info.domTab.classList.remove('edited')
                                if(cb)cb(err.err)
                            })
                        }
                    setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                }
                const nb = new Notebook(domView,()=>info.domListItem.valueText,dataContent,callBackSave,setEdited);
                if(options.save) save = (cb)=>callBackSave(nb.getJSON(),cb)
                remove = ()=>nb.remove()
            } else if (itemFile.valueText.endsWith('.csv')) {
                const setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                const ce = new CsvEditor(domView, () => info.domListItem.getFullPath(), dataContent, setEdited, options)
                remove = () => ce.remove()
                save = ce.save
                reload = ()=>ce.reload()
            } else if (itemPath.endsWith('.term')) {
                const ce = new TerminalUI(domView)
                remove = () => ce.remove()
            } else {
                const setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                const fe = new FileEditor(domView, () => info.domListItem.getFullPath(), dataContent, setEdited,this.runInBackground,options)
                remove = ()=>fe.remove()
                save = fe.save
                reload = ()=>fe.reload()
            }
            info.remove = remove
            info.save = save
            info.reload = reload
            //if (itemFile.valueText.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)) domView = document.createElement('div')
            //else 
            closeBtn.addEventListener('click', (e) => {
                if(info.domTab.classList.contains('edited')){
                    modalClose.cbClose = ()=>{
                        this.close(info.domListItem)
                    }
                    modalClose.cbSave = ()=>{
                        info.save((err)=>{
                            console.log(err)
                            if (!err) this.close(info.domListItem)
                        })
                    }
                    modalClose.show()
                } else this.close(info.domListItem)
                e.preventDefault(); e.stopPropagation()},false)
            info.panel = this.currentPanel
            domTab.addEventListener('click', (e) => { this.select(info.domListItem); e.preventDefault(); e.stopPropagation()},false)
            this.currentPanel.header.appendChild(domTab)
            this.panelList[this.currentPanel.id].push(itemPath)
            this.openFiles[itemPath] = info
        }
        this.select(itemFile)
    }

    setRunInBackgroundInstance(runInBackground){
        this.runInBackground = runInBackground;
    }

    select(itemFile){
        const itemInfo = this.openFiles[itemFile.getFullPath()]
        Object.values(this.openFiles).filter(i=>i.panel===itemInfo.panel).forEach((fileInfo)=>{
            fileInfo.domTab.classList.remove('selected')
            fileInfo.domView.style.display = 'none'
            if(fileInfo.domListItem?._value)fileInfo.domListItem._value.style.color = null
        })
        if(itemFile.domListItem?._value)
        itemFile._value.style.color = 'var(--text-hover)'
        if (!itemFile.parent?._toggled)itemFile.parent?.toggle()
        const k = itemFile.getFullPath()
        this.openFiles[k].domTab.classList.add('selected')
        this.openFiles[k].domView.style.display = null
        const el = this.openFiles[k].domTab
        const wrapper = el.parentNode.parentNode
        const elLeft = el.offsetLeft + el.offsetWidth;
        const elParentLeft = wrapper.offsetLeft + wrapper.offsetWidth;
        if (elLeft >= elParentLeft + wrapper.scrollLeft) {
            wrapper.scrollLeft = elLeft - elParentLeft;
        } else if (elLeft <= wrapper.offsetLeft + wrapper.scrollLeft) {
            wrapper.scrollLeft = el.offsetLeft - wrapper.offsetLeft;
        }
        this.currentPanel = itemInfo.panel
    }

    close(itemFile) {
        if(this.isOpen(itemFile)){
            const k = itemFile.getFullPath()
            const l = Object.keys(this.openFiles)
            const isCurrent = this.openFiles[k].domTab.classList.contains('selected')
            
            if (isCurrent && itemFile._value) itemFile._value.style.color = null
            this.openFiles[k].remove()
            this.openFiles[k].domTab.remove()
            const panelId = this.openFiles[k].panel.id

            if (isCurrent){
                const currentListPanelItem = this.panelList[panelId]
                let i =currentListPanelItem.indexOf(k)
                if(i > 0)i--;else i++;
                if (currentListPanelItem.length > 1) this.select(this.openFiles[currentListPanelItem[i]].domListItem)
            }
            this.panelList[panelId].splice(this.panelList[panelId].indexOf(k),1)

            delete this.openFiles[k]
            this.cleanEmptyPanel()
        }
    }

    cleanEmptyPanel(){
        let isDelete = false;
        Object.keys(this.panelList).filter(k=>k!=="FVmainPanel"&&this.panelList[k].length===0).forEach(p=>{
            delete this.panelList[p]
            this.panelManager.removePanel(p)
            isDelete = true;
        })
        const listPanel = Object.keys(this.panelList)
        if(!listPanel.includes(this.currentPanel.id)) this.currentPanel = document.getElementById(listPanel[listPanel.length-1])
        if(isDelete) this.updateSizePanel()
    }

    isOpen(itemFile){
        return this.openFiles.hasOwnProperty(itemFile.getFullPath())
    }

    closeAllPathStartWith(path) {
        Object.keys(this.openFiles).forEach((p)=>{
            if(!p.startsWith(path)){
                this.currentPanel.contentTab.removeChild(this.openFiles[k].domView)
                this.openFiles[k].domTab.remove()
                delete this.openFiles[k]
            }
        })
    }

    renameAllPathStartWith(oldPath, newPath) {
        Object.keys(this.openFiles).forEach((p) => {
            if (p.startsWith(oldPath)) delete Object.assign(this.openFiles, { [newPath + p.slice(oldPath.length)]: this.openFiles[p] })[p];
        })
    }

    updateDomItem(item){
        const k = item.getFullPath()
        if(this.openFiles.hasOwnProperty(k)) this.openFiles[k].domListItem = item;
    }


    rename(itemFile, prevPath) {
        if (this.openFiles.hasOwnProperty(prevPath)){
            const domFileName = this.openFiles[prevPath].domTab.querySelector('span')
            domFileName.innerText = itemFile.valueText
            colorizeFile(itemFile.valueText, domFileName)
            delete Object.assign(this.openFiles, { [itemFile.getFullPath()]: this.openFiles[prevPath] })[prevPath]
        }
    }

}

class TerminalUI {
    constructor(container) {
        this.terminal = new Terminal({theme:{
            foreground: '#F7F1FF',
            background: '#222222',
            selectionBackground: '#5DA5D533',
            selectionInactiveBackground: '#666466',
            black: '#404446',
            brightBlack: '#565456',
            red: '#ec517d',
            brightRed: '#fc618d',
            green: '#6bb87f',
            brightGreen: '#7bd88f',
            yellow: '#eFb856',
            brightYellow: '#FFD866',
            blue: '#4aa4d6',
            brightBlue: '#5ab4e6',
            magenta: '#847ac3',
            brightMagenta: '#948ae3',
            cyan: '#62e0eF',
            brightCyan: '#72F0FF',
            white: '#F8F8F8',
            brightWhite: '#F7F1FF'
          },fontFamily: "Fira Code"});

        this.container = container
        container.style.width = "100%"
        container.style.height = "100%"//"calc(100% - 40px)"
        container.style.backgroundColor = "#222222"
        container.style.padding = "5px 0 40px 5px"
        const term = document.createElement('div')
        term.style.width = "100%"
        term.style.height = "100%"
        container.appendChild(term)
        const fitAddon = new FitAddon();
        this.terminal.loadAddon(fitAddon);
        this.terminal.loadAddon(new WebglAddon());
        this.terminal.open(term);
        this.socket = io();
        this.terminal.onResize(size => {
            this.socket.emit("resize", size);
        })
        fitAddon.fit()

        this.terminal.onData((data) => this.socket.emit("input", data));
        this.socket.on("output", (data) => this.terminal.write(data));
        this.socket.on("disconnect", () => {
            term.innerHTML = ""
        });

        let lastInterval;
        window.addEventListener('resize',(e)=>{
            if(lastInterval) clearTimeout(lastInterval)
            lastInterval = setTimeout(()=>fitAddon.fit(),500)
        })
        setTimeout(()=>this.terminal.focus(),500)
    }
  
  
  
    remove() {
      this.socket.disconnect();
      this.container.remove();
      delete this
    }
  }
class CsvEditor {
    constructor(container, pathGetter, content, setIsEdited, options) {
        this.fileEditorDOM = container
        this.fileEditorDOM.style = "height:100%; width:100%;"
        this.pathGetter = pathGetter
        this.contentOnServe = content
        this.setIsEdited = setIsEdited
        this.csvTable = document.createElement('csv-table')
        let header = document.createElement('div')
        header.className = "header-notebook table-info-container"
        let btn;
        if (options.save) {
            btn = document.createElement('a')
            btn.innerText = "Save"
            this.save = (cb) => {
                let path = this.pathGetter()
                let content = this.csvTable.getCSV()
                userFolder('write', { path, data: content }, (err) => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                    else { this.contentOnServe = content; this.setIsEdited(false) }
                    if (cb) cb(err.err)
                })
            }
            btn.addEventListener('click', () => this.save())
            header.appendChild(btn)
        }
        btn = document.createElement('a')
        btn.innerText = "Reload"
        btn.addEventListener('click', () => this.reload())
        header.appendChild(btn)
        this.btnReload = btn
/*
        let spacer = document.createElement('div')
        spacer.style.marginLeft = 'auto';
        header.appendChild(spacer)*/
        this.fileEditorDOM.appendChild(header)

        const wrapper = document.createElement('div')
        wrapper.className = 'cellsContainer'
        wrapper.appendChild(this.csvTable)
        this.fileEditorDOM.appendChild(wrapper)
        this.csvTable.setData(content);
        this.csvTable.cbEdit = this.setIsEdited;
        [...this.csvTable.getPageManager().children].forEach(d=>header.appendChild(d))

    }
    remove() {
        this.fileEditorDOM.parentElement.removeChild(this.fileEditorDOM)
    }
    reload() {
        userFolder('read', { path: this.pathGetter() }, (data) => {
            if (!data.err) {
                this.contentOnServe = data.data
                this.csvTable.setData(data.data)
                this.setIsEdited(false)
            } else {
                quickToast.fire({ icon: 'error', title: 'read file error' })
            }
        })
    }
}

let idEditor = 0
class FileEditor {
    constructor(container, pathGetter, content, setIsEdited, runInBackground, options) {
        this.fileEditorDOM = container
        this.fileEditorDOM.style = "height:100%; width:100%; overflow-y:hidden"
        this.pathGetter = pathGetter
        this.contentOnServe = content
        this.setIsEdited = setIsEdited
        this.options = options
        let header = document.createElement('div')
        header.className = "header-notebook"
        let btn;
        if (options.save){
            btn = document.createElement('a')
            btn.innerText = "Save"
            btn.classList.add('icon-save')
            this.save = (cb) => {
                let path = this.pathGetter()
                let content = this.editor.getValue()
                userFolder('write', { path, data: content }, (err) => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                    else { this.contentOnServe = content; this.setIsEdited(false) }
                    if(cb)cb(err.err)
                })
            }
            btn.addEventListener('click', ()=>this.save())
            header.appendChild(btn)

        }

        if(options.reload){
            btn = document.createElement('a')
            btn.innerText = "Reload"
            btn.classList.add('icon-reload')
            btn.addEventListener('click', () => this.reload())
            header.appendChild(btn)
            this.btnReload = btn
        }


        if(options.addParameters){
            const paramSelector = document.createElement('select')
            paramSelector.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>'
            paramSelector.addEventListener('focus', function () {
                this.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>' + Parameters.getCurrentItemBuild().items.map((p, i) => ('<option value="' + i + '">' + p.titleText + '</option>')).join('')
            })
            paramSelector.addEventListener('change', () => {
                if (paramSelector.value) {
                    let paramItem = Parameters.getCurrentItemBuild().items[parseInt(paramSelector.value)]
                    const l = this.editor.getPosition().lineNumber+1;
                    const indent = this.editor.getModel().getValueInRange({startLineNumber: l,endLineNumber: l+1,startColumn: 1,endColumn: 1}).match(/^(\s*)/)[0].replace('\n','')
                    const range = {startLineNumber: l,endLineNumber: l,startColumn: 1,endColumn: 1}
                    this.editor.executeEdits("insert-parameters", [{identifier: { major: 1, minor: 1 }, range, text: ('params = ' + JSON.stringify({ input: Parameters.getParamFromItemParam(paramItem) }, null, 2)).replaceAll(/^/gm,indent)+'\n', forceMoveMarkers: true}]);
                }
            })
            header.appendChild(paramSelector)
        }

        let spacer = document.createElement('div')
        spacer.style.marginLeft = 'auto';
        header.appendChild(spacer)
        if(options.runInBackground){
            btn = document.createElement('a')
            btn.style = "background-color:var(--theme);color:var(--dark-bg-0)"
            btn.innerText = "Run in background"
            btn.classList.add('icon-play')
            btn.addEventListener('click', () => {
                let path = this.pathGetter()
                fetchW('/api/nohup/' + Parameters.getCurrentItemBuild().token, {
                    method: 'POST', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                    body: JSON.stringify({ path })
                }).then(res => res.json()).then(data => {
                    if (data.err) quickToast.fire({ icon: 'error', title: 'run error' })
                    else runInBackground.refresh(data.token)
                    //refresh backgournd run manager
                })
            })
            this.btnRunBackground = btn
            header.appendChild(btn)
        }

        if (options.tokenKillnohup) {
            btn = document.createElement('a')
            btn.style = "background-color:#d13b4b;color:var(--dark-bg-0)"
            btn.classList.add('icon-stop')
            btn.innerText = "Stop execution"
            btn.addEventListener('click', () => {
                btn.style.display = 'none'
                fetchW('/api/killnohup/' + options.tokenKillnohup, { // + itemRunInBackground.token
                    method: 'GET'
                }).then(res => res.json()).then(err => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'kill error' })
                    header.removeChild(btn)
                    runInBackground.refresh()
                })
            })
            this.btnStopBackground = btn
            header.appendChild(btn)
        }
        this.fileEditorDOM.appendChild(header)

        if(options.type==='img'){
            const imgDiv = document.createElement('div')
            imgDiv.className = 'cellsContainer'
            imgDiv.style = "overflow-x: hidden;"
            this.imgView = document.createElement('img')
            this.imgView.style = "transform: translate(-50%, 0px);margin-left: 50%;max-width: 100%;"
            imgDiv.appendChild(this.imgView)
            this.fileEditorDOM.appendChild(imgDiv)
            this.imgView.src = content
        } else {
            let fileEditor = document.createElement('div')
            this.idEditor = 'serverFileEditor'+idEditor
            fileEditor.id = this.idEditor
            fileEditor.className = 'cellsContainer'
            this.fileEditorDOM.appendChild(fileEditor)
            this.editor = CodeEditorProvider.createEditor('serverFileEditor'+idEditor, {
                lineNumbers: 'on', minimap: true, language: 'python',readOnly: options.readOnly
            });
            this.editor.setValue(content)
            this.editor.layout()
            fileEditor.addEventListener('focusout', () => {
                this.setIsEdited(this.editor.getValue('serverFileEditor'+idEditor)!==this.contentOnServe)
            })

            if (options.save)
            fileEditor.addEventListener('keydown', (evt) => {
                this.setIsEdited(this.editor.getValue('serverFileEditor'+idEditor)!==this.contentOnServe)
                if (((navigator?.userAgentData?.platform || navigator?.platform || 'unknown').match("Mac") ? evt.metaKey : evt.ctrlKey) && evt.key == 's') {
                    this.save();
                    evt.preventDefault();
                    evt.stopPropagation();
                }
            }, true)
            idEditor++
        }
    }

    remove(){
        if (this.idEditor)CodeEditorProvider.removeEditor(this.idEditor)
        this.fileEditorDOM.parentElement.removeChild(this.fileEditorDOM)
    }

    reload() {
        if(this.options.reload){
            userFolder('read', { path: this.pathGetter() }, (data) => {
                if (!data.err) {
                    this.contentOnServe = data.data
                    if(this.imgView) this.imgView.src = data.data
                    else if(this.editor) this.editor.setValue(data.data)
                    this.setIsEdited(false)
                } else {
                    quickToast.fire({ icon: 'error', title: 'read file error' })
                }
            })
        }
    }
}

export class ResourceMonitor {
    constructor() {
        this.section = document.createElement('custom-section')
        this.section.classList.add('title')
        this.section.style = "position: sticky; top: 0; background-color: var(--bg-0); z-index: 1; border-bottom: 2px solid var(--bg-4);"
        this.section.titleText = "resource monitor"

        this.cpu = this._buildCursor("#12b8f1")
        this.mem = this._buildCursor("#12f1b8")

    }


    _buildCursor(color) {
        let container = document.createElement('div')
        container.style = "border-radius: 4px;background-color: var(--bg-input);margin: 7px 7px;color: #000;text-shadow: 0 0 2px #fff;font-weight: bolder;"
        let bar = document.createElement('div')
        bar.style = "border-radius: 4px;background-color:" + color + ";width:0%;padding: 2px 10px;text-align: center;white-space: nowrap;"
        bar.innerText = "0%"
        container.appendChild(bar)
        this.section.addItem(container)
        return bar
    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh() {
        if (document.hasFocus()){
            fetchW('/api/monitor', { method: 'GET' }).then(res => res.json()).then(data => {
                if(data.err) return;
                this.cpu.style.width = data.cpu + '%'
                this.cpu.innerText = data.cpu?.toFixed(2) + '% CPU'
                this.mem.style.width = (data.activeMem * 100 / data.totalMem) + '%'
                this.mem.innerText = (data.activeMem)?.toFixed(2) + ' / ' + data.totalMem?.toFixed(2) + ' Go'
            })
        }
    }

    startAutoRefresh(ms) {
        if (!this.interval) {
            this.refresh()
            this.interval = setInterval(() => {
                if (this.section.offsetParent)
                    this.refresh()
            }, ms)
        }
    }
} 



export class RunInBackground {
    constructor(filesViewer) {
        this.filesViewer = filesViewer;
        this.mainSection = document.createElement('custom-section')
        this.mainSection.toggleable = true
        this.mainSection.classList.add('title')
        this.mainSection.titleText = "run in background"
        this.mainSection.deletable = true
        this.mainSection.classList.add('title')
        let btnRefresh = document.createElement('a')
        btnRefresh.classList.add('icon-reload')
        btnRefresh.addEventListener('click', () => this.refresh())
        this.mainSection._btnSlot.appendChild(btnRefresh);
        this.mainSection.deletable = false
        this.sections = {}

    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh(autoOpenToken) {
        fetchW('/api/statesnohup', { method: 'GET' }).then(res => res.json()).then(data => {
            for(const section of Object.values(this.sections)) section.clear()
            if(data.err) return;
            for(const [secName,infos] of Object.entries(data.data)){
                for(let info of infos){
                    if (!this.sections.hasOwnProperty(secName)) {
                        this.sections[secName] = document.createElement('custom-section')
                        this.sections[secName].titleText = secName
                        this.sections[secName].toggleable = true
                        this.mainSection.addItem(this.sections[secName])
                    }
                    let item = document.createElement('custom-item')
                    item.isRunningBackground = true
                    if (secName !== 'running') {
                        item.deletable = () => {
                            fetchW('/api/removenohup/'+info.token, { method: 'GET' }).then(res => res.json()).then(err => {
                                if(err.err)this.refresh()
                                else this.filesViewer.close(item)
                            })
                        }
                    }
                    info.status = secName
                    item.info = info
                    item.getFullPath = () => './.backgroundRun/' + info.token + '.log'
                    item.valueText = info.fileRun + ' ' + (new Date(info.date)).toLocaleString("fr-FR")
                    this.sections[secName].addItem(item)
                    const open = () => {
                        if(this.filesViewer.isOpen(item)) this.filesViewer.openFiles[item.getFullPath()].reload()
                        else userFolder('read', { path: item.getFullPath() }, (data) => {
                                if (data.err) quickToast.fire({ icon: 'error', title: 'read file error' })
                                else this.filesViewer.open(item, data.data, {tokenKillnohup:(info.status==='running')?info.token:false,readOnly:true,reload:true})
                            })
                    }
                    item._value.addEventListener('click', open)
                    if(info.token === autoOpenToken){
                         this.filesViewer.open(item, '', {tokenKillnohup:(info.status==='running')?info.token:false,readOnly:true,reload:true})
                         setTimeout(open,3000)
                    }
                }
            }
        })
    }
}


export class RunningNotebooks {
    constructor() {
        this.mainSection = document.createElement('custom-section')
        this.mainSection.toggleable = true
        this.mainSection.classList.add('title')
        this.mainSection.titleText = "running notebooks"
        this.mainSection.deletable = true
        this.mainSection.classList.add('title')
        let btnRefresh = document.createElement('a')
        btnRefresh.classList.add('icon-reload')
        btnRefresh.addEventListener('click', () => this.refresh())
        this.mainSection._btnSlot.appendChild(btnRefresh);
        this.mainSection.deletable = false

    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh() {
        fetchW('/api/notebook/getMyNotebooksRunning', { method: 'GET' }).then(res => res.json()).then(data => {
            if(data.err) return;
            this.mainSection.clear()
            for(const notebook of data.data){
                    let item = document.createElement('custom-item')
                    item.deletable = () => {
                        let info = { event: "kill from runningNotebook panel" };
                        fetchW("api/notebook/kill/" + notebook.id, {
                            method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(info)
                        }).then((response) => response.json()).then(()=>this.refresh())
                    }
                    item.valueText = '' + (new Date(notebook.lastUse)).toLocaleString("fr-FR")
                    item._btnDel.insertAdjacentHTML('beforebegin', `<span style="border-radius:3px;background-color:#12b8f1;padding: 0 3px 0 3px;color: black;margin: 0 3px 0 auto;">${notebook.cpu}%</span><span style="border-radius:3px;background-color:#12f1b8;padding: 0 3px 0 3px;color: black;margin: 0 3px 0 3px;">${notebook.mem}%</span>`)
                    item._btnDel.style.marginLeft = 0
                    this.mainSection.addItem(item)
                }
        })
    }
}