
class ActivityManager {

    constructor(){ 
        this.activityList = {}
        this.callBackContent = {}
        this._currentActivity = undefined;
    }


    addActivity(idToggle,idActivity,idContent,callBackOn,callBackOff,callMouseLeave,isDefault=false){
        this.activityList[idToggle] = {
            idActivity: idActivity,
            idContent: idContent,
            callBackOn:callBackOn,
            callBackOff:callBackOff
        }
        const activityDom = document.getElementById(idActivity)
        const contentDom = document.getElementById(idContent)
        const btnDom = document.getElementById(idToggle)
        if(isDefault){
            this._currentActivity = idToggle
            if(activityDom) activityDom.style.display = null
            if(contentDom) contentDom.style.display = null
            if(btnDom) btnDom.parentElement.classList.add('active')
        } else {
            if (this.activityList[this._currentActivity].idActivity !== idActivity && activityDom) activityDom.style.display = 'none'
            if (this.activityList[this._currentActivity].idContent !== idContent && contentDom) contentDom.style.display = 'none'
            if(btnDom) btnDom.parentElement.classList.remove('active')
        }
        if(btnDom) btnDom.onclick = () =>{this.toggleActivity(idToggle)}
        if(callMouseLeave && activityDom)
            activityDom.addEventListener('mouseleave', (event) => {
                callMouseLeave();
            });
    }

    toggleActivity(idToggle){
        let currentToggle = this._currentActivity
        let currentActivity = this.activityList[currentToggle].idActivity
        let nextActivity = this.activityList[idToggle].idActivity
        let currentContent = this.activityList[currentToggle].idContent
        let nextContent = this.activityList[idToggle].idContent
        if(currentToggle !== idToggle){
            if(document.getElementById(currentToggle))document.getElementById(currentToggle).parentElement.classList.remove('active')
            if(document.getElementById(idToggle))document.getElementById(idToggle).parentElement.classList.add('active')

            //if(currentActivity!==nextActivity){
                if(document.getElementById(currentActivity))document.getElementById(currentActivity).style.display = 'none'
                if(document.getElementById(nextActivity))document.getElementById(nextActivity).style.display = null
            //}
            //if (currentContent !== nextContent) {
                if(document.getElementById(currentContent))document.getElementById(currentContent).style.display = 'none'
                if(document.getElementById(nextContent))document.getElementById(nextContent).style.display = null
            //}

            window.dispatchEvent(new Event('resize'));
            if (currentContent !== nextContent) {
                if (this.callBackContent.hasOwnProperty(currentContent)) {
                    if (this.callBackContent[currentContent].callBackOff)
                        this.callBackContent[currentContent].callBackOff() 
                }
                if (this.callBackContent.hasOwnProperty(nextContent)) {
                    if (this.callBackContent[nextContent].callBackOn)
                        this.callBackContent[nextContent].callBackOn() 
                }
            }
            if(this.activityList[currentToggle].callBackOff) this.activityList[currentToggle].callBackOff()
            if(this.activityList[idToggle].callBackOn) this.activityList[idToggle].callBackOn()
            this._currentActivity = idToggle
        }
    }

    get currentActivity(){
        return this._currentActivity
    }

    addEventContentChange(idContent,callBackOn, callBackOff) {
        this.callBackContent[idContent] = {callBackOn,callBackOff}
    }
}

const activityManager = new ActivityManager()
export default activityManager;