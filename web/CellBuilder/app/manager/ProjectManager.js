import JSZip from "jszip";
import ModalWindows from "../guiComponent/modalWindow";
import exportJsonProject from "../inputs_outputs/exportJsonProject";
import loadDiagram from "../inputs_outputs/loadDiagram";
import ActivityManager from "./ActivityManager";
import Parameters from "./ParametersManager";
import { downloadFile } from "../utils/misc";
import { panelManagerContent } from "../setups/PanelManagerSetup";
import { longToast, quickToast } from "../guiComponent/notification";
import stateMachine from "../diagrams/stateMachine";
import behaviorDiagram from "../diagrams/behaviorDiagram";



class isicellDB {
    // connect to IndexedDB database
    constructor() {
      return new Promise((resolve, reject) => {
        // connection object
        this.db = null;
        // no support
        if (!('indexedDB' in window)) reject('not supported');
        // open database
        const dbOpen = indexedDB.open('isicellDB',2);
        let toMigration = undefined;
        // database upgrade event
        dbOpen.onupgradeneeded = async e => {
            const db = dbOpen.result
            console.log(db, e.oldVersion, e.newVersion);
            if(!db.objectStoreNames.contains('preview')) db.createObjectStore('preview');
            if(!db.objectStoreNames.contains('project')) db.createObjectStore('project');
            if(db.objectStoreNames.contains('myStore')) {
                let lastXML = await(new Promise((resolve, reject) => {const t = e.target.transaction.objectStore('myStore').get("diagramXML");t.onsuccess = ()=>resolve(t.result)}))
                let lastAutoSave = await(new Promise((resolve, reject) => {const t = e.target.transaction.objectStore('myStore').get("autoSave");t.onsuccess = ()=>resolve(t.result)}))
                toMigration = {lastXML,lastAutoSave}
                db.deleteObjectStore('myStore')
            }
        };

        dbOpen.onsuccess = async () => {
            this.db = dbOpen.result;
            if(toMigration){
                if(toMigration.lastAutoSave && toMigration.lastAutoSave!==toMigration.lastXML){
                    await this.setProject('autoSave',JSON.parse(toMigration.lastAutoSave))
                }
                await this.setProject('oldVersion',JSON.parse(toMigration.lastXML))
                await this.setPreview('oldVersion',{svg:'<svg></svg>', name:'oldVersion', date: Date.now() })
            }
            resolve( this );
        };

        dbOpen.onerror = e => {
            reject(`IndexedDB error: ${ e.target.errorCode }`);
        };
      });
    }

    getAllPreview(){
        return new Promise((resolve, reject) => {
            const store = this.db.transaction('preview', 'readonly').objectStore('preview')
            // read record
            const request = store.getAll();
            request.onsuccess = () => {
                this.allProjectName = request.result.map(p=>p.name)
                resolve(request.result); // success
            };
            request.onerror = () => {
                reject(request.error); // failure
            };
        });
    }

    // store item
    setPreview(name, value) {
        return new Promise((resolve, reject) => {
        // new transaction
        const
            transaction = this.db.transaction('preview', 'readwrite'),
            store = transaction.objectStore('preview');
        // write record
        store.put(value, name);
        transaction.oncomplete = () => {
            resolve(true); // success
        };
        transaction.onerror = () => {
            reject(transaction.error); // failure
        };
        });
    }

    getPreview(name){
        return new Promise((resolve, reject) => {
            const store = this.db.transaction('preview', 'readonly').objectStore('preview')
            // read record
            const request = store.get(name);
            request.onsuccess = () => {
                resolve(request.result); // success
            };
            request.onerror = () => {
                reject(request.error); // failure
            };
        });
    }

    hasAutoSave(){
        return new Promise((resolve, reject) => {
            const store = this.db.transaction('project', 'readonly').objectStore('project')
            // read record
            const request = store.getKey('autoSave');
            request.onsuccess = () => {
                resolve(request.result!==undefined); // success
            };
            request.onerror = () => {
                resolve(false); // success
                //reject(request.error); // failure
            };
        });

    }

    getProject(name){
        return new Promise((resolve, reject) => {
            const store = this.db.transaction('project', 'readonly').objectStore('project')
            // read record
            const request = store.get(name);
            request.onsuccess = () => {
                resolve(request.result); // success
            };
            request.onerror = () => {
                reject(request.error); // failure
            };
        });
    }

    // store item
    setProject(name, value) {
        return new Promise((resolve, reject) => {
        // new transaction
        const
            transaction = this.db.transaction('project', 'readwrite'),
            store = transaction.objectStore('project');
        // write record
        store.put(value, name);
        transaction.oncomplete = () => {
            resolve(true); // success
        };
        transaction.onerror = () => {
            reject(transaction.error); // failure
        };
        });
    }

    removeProject(name){
        return new Promise((resolve, reject) => {
        // new transaction
        const
            transaction = this.db.transaction('project', 'readwrite'),
            store = transaction.objectStore('project');
        // write record
        store.delete(name);
        transaction.oncomplete = () => {
            resolve(true); // success
        };
        transaction.onerror = () => {
            reject(transaction.error); // failure
        };
        });

    }
    removePreview(name){
        return new Promise((resolve, reject) => {
        // new transaction
        const
            transaction = this.db.transaction('preview', 'readwrite'),
            store = transaction.objectStore('preview');
        // write record
        store.delete(name);
        transaction.oncomplete = () => {
            resolve(true); // success
        };
        transaction.onerror = () => {
            reject(transaction.error); // failure
        };
        });

    }

    async renameProjectAndPreview(name,newName){
        console.log("rename ",name,"to",newName)
        const project = await this.getProject(name)
        const preview = await this.getPreview(name)
        preview.name = newName
        if(project && preview){
            await this.removePreview(name)
            await this.removeProject(name)
            await this.setPreview(newName, preview)
            await this.setProject(newName, project)
            return true
        } 
        return false
    }
}

class ProjectManager {
    constructor(){
        this.dateLastSave = Date.now();

        this.viewContainer = document.createElement('div')
        this.viewContainer.id = 'projectManagerGUI'
        this.viewContainer.classList.add("svgCardContainer")
        document.getElementById('contentView').appendChild(this.viewContainer)



        let divModal = document.createElement('div')
        divModal.innerHTML = 'Changes to the project were not saved. If you change project, you will lose these modifications.<footer><a style="background-color:#2ecc71">Cancel</a><a style="background-color:#e74c3c">Open project</a></footer>'
        this.modalChangeProject = new ModalWindows('Changes unsaved', divModal)
        divModal.addEventListener('click', async (e) => {
        if (e.target.tagName === 'A') {
            if(e.target.textContent === 'Open project') {
                try{
                    let data;
                    if(this.modalChangeProject.data){
                        data = this.modalChangeProject.data
                        delete this.modalChangeProject.data
                    } else if(this.modalChangeProject.projectName !== undefined){
                        data = await this.db.getProject(this.modalChangeProject.projectName)
                        if(this.modalChangeProject.domCard){
                            this.domActiveCard = this.modalChangeProject.domCard
                            delete this.modalChangeProject.domCard
                        }
                    }
                    if(data){
                        await loadDiagram(data)
                        panelManagerContent.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1);
                        ActivityManager.toggleActivity('cells_behavior');
                        await this.db.removeProject('autoSave')
                        this.currentProjectName = this.modalChangeProject.projectName
                        this.modalChangeProject.hide();

                        quickToast.fire({
                            icon: 'success',
                            title: 'Diagram loaded'
                        });
                    } else {
                        console.error("ah ?")
                    }
                } catch(error) {
                    console.error("error opening file",error);
                    longToast.fire({
                        icon: 'error',
                        title: 'File couldn\'t load'
                    })
                }
            } else {
                this.modalChangeProject.hide()
            }
        }
        })
        divModal = document.createElement('div')
        divModal.innerHTML = 'Are you sure you want to remove this project?<footer><a style="background-color:#2ecc71">No</a><a style="background-color:#e74c3c">Yes</a></footer>'
        this.modalDeleteProject = new ModalWindows('Deleting a project', divModal)
        divModal.addEventListener('click', async (e) => {
            if (e.target.tagName === 'A') {
                if(e.target.textContent === 'Yes') {
                    this.modalDeleteProject.projectDom.parentElement.removeChild(this.modalDeleteProject.projectDom);
                    if(this.currentProjectName === this.modalDeleteProject.projectName){
                        this._domActiveCard = undefined
                        this.currentProjectName = undefined
                    }
                    this.db.removePreview(this.modalDeleteProject.projectName)
                    this.db.removeProject(this.modalDeleteProject.projectName)

                    this.allProjectName.splice(this.allProjectName.indexOf(this.modalDeleteProject.projectName),1)
                    this.modalDeleteProject.hide();
                } else {
                    this.modalDeleteProject.hide()
                }
            }
            })

        return new Promise(async (resolve, reject) => {
            this._currentProjectName = localStorage.getItem('currentProjectName');
            this.db = await new isicellDB();
            let previews = await this.db.getAllPreview();
            previews.sort((a,b)=>(a.date>b.date)?-1:1)
            this.allProjectName = previews.map(p=>p.name)
            this.viewContainer.appendChild(this.generateAsCard())
            previews.forEach(preview => {
                this.viewContainer.appendChild(this.generateCard(preview))
            });
            resolve(this)
        });
    }

    async generatePreviewFromCurrentProject(){
        let currentActivity = ActivityManager.currentActivity
        ActivityManager.toggleActivity('cells_behavior')
        const svg = await stateMachine.modeler.saveSVG()
        ActivityManager.toggleActivity(currentActivity)
        return {svg:svg.svg , name:this.currentProjectName, date: Date.now() }
    }

    generateAsCard(){
        const domCard = document.createElement('div')
        domCard.date = Infinity
        domCard.classList.add('svgCard')
        domCard.innerHTML = `<svg fill="var(--text-2)" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-5 -5 55.402 55.402"><g><path d="M 41.267,20.961767 H 24.427233 V 4.134 C 24.427233,1.851 24.749353,0.02786099 22.707,0 20.736656,-0.02687866 21.042578,1.8510098 21.035862,4.135 V 20.971767 H 4.141 c -2.283,0 -4.139,-0.289243 -4.138,1.730233 0.00424757,1.75204 1.9659404,1.57307 4.127,1.606947 H 21.035862 V 41.27 c 0.04201,2.916712 -0.33442,4.177802 1.667138,4.133 2.089619,0 1.717573,-1.85101 1.724233,-4.133 V 24.307947 H 41.267 c 2.283,0 4.158515,0.285139 4.133,-1.600947 -0.02778,-2.053493 -1.85201,-1.738718 -4.133,-1.745233 z"/></g></svg>
                             <p>Save current Project as new project</p>`
        domCard.querySelector('svg').addEventListener('click',e=>{
            if(this.allProjectName.includes(this.currentProjectName)){
                let newName = this.currentProjectName + " copy"
                while(this.allProjectName.includes(newName)) {
                    newName += " copy"
                }
                this.currentProjectName = newName
                this.save(false)
            } else {
                this.save(false)
            }
        })
        return domCard

    }

    generateCard(preview){
        const domCard = document.createElement('div')
        domCard.date = preview.date
        domCard.classList.add('svgCard')
        if(preview.name===this.currentProjectName) this.domActiveCard = domCard
        domCard.innerHTML = `${preview.svg}<div class="header-svgCard2"><span>${preview.name}</span><input style="display:none;" value="${preview.name}" /><a class="icon-rename" style="background-color:#2ecc71;"></a><a class="icon-download" style="background-color:#10499e;"></a><a class="icon-trash" style="background-color:#e74c3c;"></a></div><p>${(new Date(preview.date)).toLocaleString()}</p></div>`
        let nameDom = domCard.querySelector('span')
        let name = nameDom.textContent
        let renameDom = domCard.querySelector('input')
        renameDom.addEventListener("focusout",async (e) => {
            if(renameDom.style.display !== 'none'){
                renameDom.value = name
                nameDom.style.display = null
                renameDom.style.display = 'none'
            }
        });
        renameDom.addEventListener('keyup',async(e)=>{
            if(this.allProjectName.includes(renameDom.value)){
                renameDom.style.color = "#e74c3c"
                return
            }else{
                renameDom.style.color = null
            }
            if(e.key==='Enter') {
                const newName = renameDom.value
                nameDom.style.display = null
                renameDom.style.display = 'none'
                if(await this.db.renameProjectAndPreview(name,newName)){
                    if(this.currentProjectName===name)this.currentProjectName = newName
                    this.allProjectName[this.allProjectName.indexOf(name)] = newName
                    name = newName
                    console.log("new name",name)
                    nameDom.textContent = newName
                    console.log(nameDom,nameDom.textContent)
                }
        }})
        domCard.querySelector('svg').addEventListener('click', async (e) => {
            if(await this.db.hasAutoSave()){
                this.modalChangeProject.projectName = name
                this.modalChangeProject.domCard = domCard
                this.modalChangeProject.show()
            } else {
                await loadDiagram(await this.db.getProject(name))
                panelManagerContent.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1);
                ActivityManager.toggleActivity('cells_behavior')
                this.currentProjectName = name
                this.domActiveCard = domCard
            }
        })
        domCard.querySelector('.icon-rename').addEventListener('click',e=>{
            nameDom.style.display = 'none'
            renameDom.style.display = null
            renameDom.focus()
        })
        domCard.querySelector('.icon-trash').addEventListener('click',e=>{
            this.modalDeleteProject.projectName = name;
            this.modalDeleteProject.projectDom = domCard;
            this.modalDeleteProject.show()
        })
        domCard.querySelector('.icon-download').addEventListener('click',async e=>{   
            var zip = JSZip();
            console.log("init zip")
            zip.file("diag.xml", JSON.stringify(await this.db.getProject(name)));
            console.log("add file")
            zip.generateAsync({ type: "base64" }).then((base64) => {
                downloadFile("data:application/zip;base64," + base64, name+".isidiag")
                console.log("download")
            });
        })
        return domCard
    }

    sortCard(){
        [...this.viewContainer.children].sort((a,b)=>(a.date>b.date)?-1:1).forEach(card=>this.viewContainer.appendChild(card));
    }

    get currentProjectName(){
        return this._currentProjectName
    }

    set currentProjectName(value){
        if(value === undefined){
            value = "unnamed"
            let i = 2
            while(this.allProjectName.includes(value)){
                value = "unnamed " + i
                i+=1
            }
        }
        this._currentProjectName = value
        localStorage.setItem('currentProjectName',value);
    }

    get domActiveCard(){
        return this._domActiveCard
    }

    set domActiveCard(dom){
        if(this._domActiveCard) this._domActiveCard.classList.remove('active')
        this._domActiveCard = dom
        if(this._domActiveCard) this._domActiveCard.classList.add('active')
    }

    async loadProject(data){
        try {
            if(await this.db.hasAutoSave()){
                this.modalChangeProject.data = data
                this.modalChangeProject.projectName = undefined
                this.modalChangeProject.domCard = undefined
                this.modalChangeProject.show()
            } else {
                await loadDiagram(data)
                this.currentProjectName = undefined
                this.domActiveCard = undefined
                quickToast.fire({
                    icon: 'success',
                    title: 'Diagram loaded'
                });
                panelManagerContent.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1);
            }
        } catch {
            console.err("error loading project");
            longToast.fire({
                icon: 'error',
                title: 'File couldn\'t load'
            })
        }
    }

    async loadIsidiagFile(file){
        if (!file) {
            console.err("error opening file");
            return;
        }
        var reader = new FileReader();
        reader.onload = async e => {
            try {
                const data = await (await JSZip.loadAsync(e.target.result)).file("diag.xml").async("string")
                await this.loadProject(data)
            } catch {
                console.err("error opening file");
                longToast.fire({
                    icon: 'error',
                    title: 'File couldn\'t load'
                })
            }
        }
        reader.readAsBinaryString(file);

    }

    async save(auto=false){
        if(auto && Parameters.dateLastChange > this.dateLastSave){
            let xml = await exportJsonProject(false)
            await this.db.setProject('autoSave',xml)
            quickToast.fire({
                icon: 'success',
                title: 'auto save'
              })
        } else if(!auto){
            const preview = await this.generatePreviewFromCurrentProject()
            await this.db.setPreview(this.currentProjectName, preview)
            await this.db.setProject(this.currentProjectName, await exportJsonProject(false))
            if(!this.allProjectName.includes(this.currentProjectName)){
                this.viewContainer.append(this.generateCard(preview))
                this.allProjectName.push(this.currentProjectName)
                longToast.fire({
                    icon: 'success',
                    title: 'A new project has been create'
                })
            } else if(this.domActiveCard){
                this.domActiveCard.date = preview.date
                this.domActiveCard.querySelector('svg').outerHTML = preview.svg
                this.domActiveCard.querySelector('p').textContent = (new Date(preview.date)).toLocaleString()
                quickToast.fire({
                    icon: 'success',
                    title: 'your current work is saved in your browser'
                })
            }
            this.sortCard()
            await this.db.removeProject('autoSave')
            if(this.allProjectName.length>0){
                document.getElementById('projectManager_btn').parentElement.style.display = null
            }
        }
        this.dateLastSave = Date.now();
    }

    runAutoSave(second=15){
        if(this.autoSaveInterval === undefined)
            this.autoSaveInterval = setInterval(async () => {
                this.save(true)
            }, second * 1000)
    }

    stopAutoSave(){
        clearInterval(this.autoSaveInterval)
        this.autoSaveInterval = undefined
    }

    async loadAtStart(){
        if(await this.db.hasAutoSave()){
            await loadDiagram(await this.db.getProject('autoSave'))
            longToast.fire({
                icon: 'success',
                title: `loading auto save (${this.currentProjectName})`
            })
        } else if(this.allProjectName.includes(this.currentProjectName)){
            await loadDiagram(await this.db.getProject(this.currentProjectName))
            longToast.fire({
                icon: 'success',
                title: `loading project ${this.currentProjectName}`
            })
        } else if(this.allProjectName.length>0){
            ActivityManager.toggleActivity('projectManager_btn')
        }
        if(this.allProjectName.length>0){
            document.getElementById('projectManager_btn').parentElement.style.display = null
        }
        this.runAutoSave()
    }


}


const projectManager = await new ProjectManager()

export default projectManager
