import ContextMenu from '../guiComponent/contextMenu.js';
import ModalWindows from '../guiComponent/modalWindow.js';
import getFinalJson from '../inputs_outputs/getFinalJson.js';
import exportJsonProject from '../inputs_outputs/exportJsonProject.js';
import loadDiagram from '../inputs_outputs/loadDiagram.js';
import ActivityManager from './ActivityManager.js';
import BuilderManager from './BuilderManager.js';
import ExploreManager from './exploreManager.js';
import { downloadFile, fetchW, getLoader } from '../utils/misc.js';
import ViewerManager from '../viewer/ViewerManager.js';
import CodeEditorProvider from "../guiComponent/codeEditorProvider.js";
import md5 from 'js-md5'
import DiffManager from './diffManager.js';
import projectManager from './ProjectManager.js';
import '../css/parameters.css'
import { getDefaultColor } from '../utils/color.js';
import loggingManager from './LoggingManager.js';
/**
 * Instance Managing
 */
var instance = null;

var objectMap = (obj, fn) =>
    Object.fromEntries(
        Object.entries(obj).map(
            ([k, v], i) => [k, fn(v, k, i)]
        )
    )

var getNumDigits = (val) => {
    return (`${val}`.match(/\d/g) || []).length //a regex to compute the number of digits in a number. Note that decimals will get counted as digits, which is why to establish our limit and step we rounded
}
var getAutoMinMaxStep = (val) => {
    let step, limit;
    if (val > -1 && val < 1) {
        const numDigits = getNumDigits(val);
        step = 1 / Math.pow(10, numDigits + 1);
        limit = 10 * val;
        if (val === 0) {
            limit = 1;
            step = 0.001;
        }
    } else {
        const numDigits = getNumDigits(Math.round(val));
        limit = Math.pow(10, numDigits);
        step = Math.pow(10, numDigits - 2);
    }
    if (val < 0.0001 && val > 0) {
        step = 0.000001;
        limit = 0.05;
    }
    return { min: 0, max: limit, step: step, hide: false };
}

function unselectParam(){
    if (parametersInstance.currentItemParam) {
        parametersInstance.currentItemParam.classList.toggle('active')
        parametersInstance.currentItemParam = undefined
        parametersInstance.paramGUI.clear()
    }
}
function loadVersion(itemBuilder){
    loadDiagram(itemBuilder.xml, true).then(i => {
        ActivityManager.toggleActivity('parambtn')
        parametersInstance.currentItemBuild.classList.toggle('active')
        parametersInstance.currentItemBuild = itemBuilder
        parametersInstance.currentItemBuild.classList.toggle('active')
        unselectParam();
        parametersInstance.currentItemBuild.changeDetected = false
        parametersInstance.currentItemBuild.changeDetectedXML = false
        parametersInstance.refreshBtnVisibility()
    })
}


function createModalOnChange() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode('Changes have been detected, what do you want to do ?')
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const overwriteBtn = document.createElement('a')
    const newBtn = document.createElement('a')
    const doNothingBtn = document.createElement('a')
    overwriteBtn.innerText = 'overwrite'
    newBtn.innerText = 'new'
    doNothingBtn.innerText = 'do nothing'
    overwriteBtn.style.backgroundColor = '#e74c3c'
    newBtn.style.backgroundColor = '#2ecc71'
    doNothingBtn.style.backgroundColor = '#10499e'

    footer.appendChild(newBtn); footer.appendChild(overwriteBtn); footer.appendChild(doNothingBtn)
    var modal = new ModalWindows('Build', div)

    overwriteBtn.addEventListener('click', () => {
        modal.hide()
        const finalJson = getFinalJson()
        parametersInstance.currentItemBuild.failedCompile = true
        parametersInstance.currentItemBuild.failedCompileExplore = true
        parametersInstance.currentItemBuild.checksum = md5(JSON.stringify(finalJson))
        document.getElementById('notebook').parentElement.style.display = 'none'
        document.getElementById('explore').parentElement.style.display = 'none'
        //parametersInstance.currentItemBuild.token = token
        parametersInstance.currentItemBuild.date = Date.now()
        parametersInstance.currentItemBuild.changeDetected = false
        parametersInstance.currentItemBuild.changeDetectedXML = false
        parametersInstance.currentItemBuild.finalJson = finalJson
        exportJsonProject(true).then(xml => parametersInstance.currentItemBuild.xml = xml)
        unselectParam()
        parametersInstance.currentItemBuild.updateParamsAndConfigs()
        /*
        let param = parametersInstance.getParamFromDiagram()
        for (let itemParam of parametersInstance.currentItemBuild.items) {
            const flattenLastParam = JSON.flatten(itemParam.param)
            const flattenParam = JSON.flatten(param)
            Object.keys(flattenParam).forEach(k => flattenParam[k] = flattenLastParam.hasOwnProperty(k) ? flattenLastParam[k] : flattenParam[k])
            itemParam.param = JSON.unflatten(flattenParam)
        }*/
        parametersInstance.refreshBtnVisibility()
    })
    newBtn.addEventListener('click', () => {
        modal.hide()
        parametersInstance.currentItemBuild.changeDetected = false
        parametersInstance.currentItemBuild.changeDetectedXML = false

        const color = parametersInstance.currentItemBuild.color
        const pythonCode = parametersInstance.currentItemBuild.pythonCode

        parametersInstance.addNewBuild(parametersInstance.currentItemBuild.titleText+' new')
        parametersInstance.currentItemBuild.color = color
        parametersInstance.currentItemBuild.pythonCode = pythonCode
        unselectParam()
        parametersInstance.refreshBtnVisibility()
    })
    doNothingBtn.addEventListener('click', () => {
        modal.hide();
    })
    return modal
}

function createModalLoad() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode('your model has been modified since the last compilation, do you want to save these modifications before changing version ?')
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const yes = document.createElement('a')
    const no = document.createElement('a')
    yes.innerText = 'yes'
    no.innerText = 'no'
    yes.style.backgroundColor = '#2ecc71'
    no.style.backgroundColor = '#e74c3c'

    footer.appendChild(yes); footer.appendChild(no);
    var modal = new ModalWindows('Build', div)

    yes.addEventListener('click', () => {
        modal.hide()
        parametersInstance.currentItemBuild.failedCompile = true
        parametersInstance.currentItemBuild.failedCompileExplore = true
        //parametersInstance.currentItemBuild.token = token
        parametersInstance.currentItemBuild.date = Date.now()
        parametersInstance.currentItemBuild.finalJson = getFinalJson()
        exportJsonProject(true).then(xml => {
            parametersInstance.currentItemBuild.xml = xml
            loadVersion(modal.item)
        })
        
    })
    no.addEventListener('click', () => {
        modal.hide()
        loadVersion(modal.item)
        
    })
    return modal
}

function createModalJSONParamEdit() {
    const div = document.createElement('div')
    const contentEditor = document.createElement('div')
    contentEditor.id = 'editorJsonParamEdit'
    contentEditor.style.height = '75vh'
    div.appendChild(contentEditor)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const yes = document.createElement('a')
    const no = document.createElement('a')
    yes.innerText = 'yes'
    no.innerText = 'no'
    yes.style.backgroundColor = '#2ecc71'
    no.style.backgroundColor = '#e74c3c'


    footer.appendChild(yes); footer.appendChild(no);
    var modal = new ModalWindows('paramsEditor', div)
    CodeEditorProvider.createEditor('editorJsonParamEdit', { lineNumbers: 'on', minimap: false, language: 'json' }); 

    yes.addEventListener('click', () => {
        modal.hide()
        const newJson = JSON.parse(CodeEditorProvider.getValue('editorJsonParamEdit'))
        
        const flattenLastParam = JSON.flatten(parametersInstance.currentItemParam.param)
        const flattenLastConfig = JSON.flatten(parametersInstance.currentItemParam.config,2)
        const flattenParam = JSON.flatten(newJson)
        Object.keys(flattenLastParam).forEach(k => {
            flattenLastParam[k] = flattenParam.hasOwnProperty(k) ? flattenParam[k] : flattenLastParam[k]
            const p = k.split('.')
            if(p[0]==='Cell' && p[1]!=='Common'){
                const kConfig = `${p[0]}.${p[2]}`
                if(flattenParam.hasOwnProperty(k) && flattenLastConfig[kConfig].hidden.includes(p[1])) flattenLastConfig[kConfig].hidden.splice(flattenLastConfig[kConfig].hidden.indexOf(p[1],1)) 
                else if(!flattenParam.hasOwnProperty(k) && !flattenLastConfig[kConfig].hidden.includes(p[1])) flattenLastConfig[kConfig].hidden.push(p[1]) 
            }
        })
        parametersInstance.currentItemParam.param = JSON.unflatten(flattenLastParam)
        parametersInstance.currentItemParam.config = JSON.unflatten(flattenLastConfig)
        parametersInstance.updateParamWidget()
    })
    no.addEventListener('click', () => {
        modal.hide()
    })
    modal.updateParams = (v) => CodeEditorProvider.setValue('editorJsonParamEdit', JSON.stringify(v, null, 2))

    return modal
}

var contextMenu = new ContextMenu({ 
    'edit name': ["icon-rename",(item) => { 
        item.parentElement.displayEditMode()
    }],
    'diff with current': ["icon-diff",(item) => {
        const buildItem = item.parentElement
        DiffManager.diff(buildItem.parent.items.indexOf(buildItem))
        document.getElementById('diff').parentElement.style.display = null
        ActivityManager.toggleActivity('diff')
    }],
    'recompile':["icon-compile", (item) => {
        const buildItem = item.parentElement
        BuilderManager.build('sql',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, token) => {
            buildItem.failedCompile = err
            buildItem.failedCompileExplore = true
            buildItem.token = token
            buildItem.compiledBy = loggingManager.username
            buildItem.date = Date.now()
            projectManager.save(true);
            if (err) parametersInstance.btnShowErrSlot.style.display = null
            else parametersInstance.btnShowErrSlot.style.display = 'none'
        })
    }],
    'recompile python':["icon-compile", (item) => {
        const buildItem = item.parentElement
        BuilderManager.build('all',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, newToken) => {
            buildItem.failedCompileExplore = err
            buildItem.failedCompile = err
            buildItem.token = newToken
            buildItem.compiledBy = loggingManager.username
            if (!err) {
                document.getElementById('explore').parentElement.style.display = null
                //ActivityManager.toggleActivity('explore')
            }
            if (err) parametersInstance.btnShowErrSlot.style.display = null
            else parametersInstance.btnShowErrSlot.style.display = 'none'
            projectManager.save(true);
        })
    }],
    'debug (GDB)':["icon-compile", (item) => {
        const buildItem = item.parentElement
        let itemParam = parametersInstance.currentItemParam
        if(!itemParam || itemParam.parent !== buildItem) itemParam = buildItem.items[buildItem.items.length-1]
        
        BuilderManager.build('debug',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, newToken) => {
            buildItem.failedCompileExplore = true
            buildItem.failedCompile = true
            buildItem.token = newToken
            buildItem.compiledBy = loggingManager.username
            parametersInstance.btnShowErrSlot.style.display = null
            projectManager.save(true);
        },parametersInstance.getParamFromItemParam(itemParam))
    }],
    'download c++ project':["icon-download", (item) => {
        const buildItem = item.parentElement
        const loader = getLoader()
        loader.setText('Preparing your files.');
        loader.show();

        fetchW('/api/download/' + buildItem.token).then(response => response.blob())
            .then(blob => {
                const reader = new FileReader;
                reader.addEventListener('load', () => {
                    downloadFile(reader.result, buildItem.titleText + ".zip")
                });
                reader.readAsDataURL(blob);
            }).catch(error=>console.log(error));
        loader.hide();
    }],
    'load this build': ["icon-load-version",(item) => {
        parametersInstance.modalLoad.item = item.parentElement
        if (parametersInstance.currentItemBuild.changeDetected || parametersInstance.currentItemBuild.changeDetectedXML) parametersInstance.modalLoad.show()
        else loadVersion(item.parentElement)
    }],
    'delete': ["icon-trash",(item) => {
        item.parentElement.remove()
    }]});

    [...contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'delete')[0].style.color = 'var(--empty-activity)'

function createBuildItem() {
    const buildItem = document.createElement('custom-section')
    buildItem.editable = true
    buildItem.toggleable = true
    buildItem.updateFromDiagram = ()=> {
        buildItem.failedCompile = true
        buildItem.failedCompileExplore = true
        buildItem.changeDetected = false
        buildItem.changeDetectedXML = false
        buildItem.date = Date.now()
        buildItem.finalJson = getFinalJson()
        exportJsonProject(true).then(xml => buildItem.xml = xml)
        buildItem.updateParamsAndConfigs()
    }
    buildItem.updateParamsAndConfigs = () => {
        if(buildItem.items.length>0){
            const flattenParam = JSON.flatten(buildItem.generateJsonParam())
            const flattenConfig = JSON.flatten(buildItem.generateConfig(),2)
            for (let itemParam of buildItem.items) {
                buildItem.copyParamFromflattenConfig(flattenParam,flattenConfig,itemParam,itemParam)
            }
        }
    }
    buildItem.copyParamFromItem = (itemParamSource,itemParamTarget) =>{
        const flattenParam = JSON.flatten(buildItem.generateJsonParam())
        const flattenConfig = JSON.flatten(buildItem.generateConfig(),2)
        buildItem.copyParamFromflattenConfig(flattenParam,flattenConfig,itemParamSource,itemParamTarget)
    }
    buildItem.copyParamFromflattenConfig = (flattenParam,flattenConfig,itemParamSource,itemParamTarget) =>{
        //const getTypeOrigin = Object.fromEntries([...parametersInstance.currentItemParam.parent.finalJson.paramInputs.Cell.map(i => ['Cell.'+i.value, i.type]),...parametersInstance.currentItemParam.parent.finalJson.paramInputs.Scenario.map(i => ['Scenario.'+i.value, i.type])])
        //const getTypeTarget = Object.fromEntries([...buildItem.finalJson.paramInputs.Cell.map(i => ['Cell.'+i.value, i.type]),...buildItem.finalJson.paramInputs.Scenario.map(i => ['Scenario.'+i.value, i.type])])
        if(itemParamSource){
            const flattenLastParam = JSON.flatten(itemParamSource.param)
            const flattenLastConfig = JSON.flatten(itemParamSource.config,2)
            Object.keys(flattenParam).forEach(k => flattenParam[k] = flattenLastParam.hasOwnProperty(k) ? flattenLastParam[k] : flattenParam[k])
            Object.keys(flattenConfig).forEach(k => flattenConfig[k] = flattenLastConfig.hasOwnProperty(k) ? (flattenLastConfig[k].type===flattenConfig[k].type?flattenLastConfig[k] : flattenConfig[k]): flattenConfig[k])
        }
        itemParamTarget.param = JSON.unflatten(flattenParam)
        itemParamTarget.config = JSON.unflatten(flattenConfig)

    }
    buildItem.generateJsonParam = () =>{
        const defaultValue = (type) => {
            if (type === 'int' || type === 'double' || type === 'float') return 1
            if (type === 'bool') return false
            if (type === 'Protocol') return []
        }
        const cell = Object.fromEntries(buildItem.finalJson.paramInputs.Cell.map(i => [i.value, defaultValue(i.type)]))
        const param = {
            'Cell': Object.fromEntries([...buildItem.finalJson.cellTypes, ...['Common']].map(t => [t, cell])),
            'Scenario': Object.fromEntries(buildItem.finalJson.paramInputs.Scenario.map(i => [i.value, defaultValue(i.type)])),
        }
        param['Scenario']['dt'] = 360
        return param
    }
    buildItem.generateConfig = () =>{
        const defaultValue = (type) => {
            if (type === 'int' || type === 'double' || type === 'float') return {type,min:0,max:2,hidden:[]}
            if (type === 'bool') return {type,hidden:[]}
            if (type === 'Protocol') return {type,ymin:0,ymax:100,dt:0.1,maxTime:100,hidden:[]}
        }
        const config = {
            'Cell': Object.fromEntries(buildItem.finalJson.paramInputs.Cell.map(i => [i.value, defaultValue(i.type)])),
            'Scenario': Object.fromEntries(buildItem.finalJson.paramInputs.Scenario.map(i => [i.value, defaultValue(i.type)])),
        }
        config['Scenario']['dt'] = {type:'double',min:100,max:1000,hidden:[]}
        return config
    }
    buildItem.addable = () => {
        const paramItem = parametersInstance.createParamItem()
        buildItem.copyParamFromItem(parametersInstance.currentItemParam,paramItem)
        unselectParam()
        parametersInstance.currentItemParam = paramItem
        parametersInstance.currentItemParam.classList.toggle('active')
        parametersInstance.updateParamWidget(buildItem)
        return paramItem
    }
    buildItem._tmp.firstChild.oncontextmenu = contextMenu.eventOnContextMenu
    buildItem._btnSlot.insertAdjacentHTML('afterbegin', '<span class="icon-round-player compilation-status"></span><span class="icon-round-python compilation-status"></span>')
    const [buildStatus,buildPythonStatus] = buildItem._btnSlot.querySelectorAll('span')
    Object.defineProperty(buildItem, 'failedCompile',{
        get: function () { return this._failedCompile; },
        set: function (status) { buildStatus.style.color = status?'var(--empty-activity)':'var(--completed-activity)'; this._failedCompile = status; },
    })
    Object.defineProperty(buildItem, 'failedCompileExplore', {
        get: function () { return this._failedCompileExplore; },
        set: function (status) { buildPythonStatus.style.color = status?'var(--empty-activity)':'var(--completed-activity)'; this._failedCompileExplore = status; },
    })
    return buildItem;
}

function createButton(name,parent,color){
    const btn = document.createElement('a')
    btn.textContent = name
    btn.classList.add('common-button')
    parent.appendChild(btn)
    if(color)btn.style.backgroundColor = color
    return btn
}

/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class Parameters {

    constructor() { 

     }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer, defaultData) {
        this.dateLastChange = 0;
        const container = document.createElement('div')
        container.id = 'sideMenuParameters'
        container.style.height = '100%'
        container.style.overflowY = 'auto'
        const sideMenu = document.createElement('custom-list')
        sideMenu.style.height = 'unset'
        sideMenu.style.marginBottom = '35px'
        mainContainer.appendChild(container)
        container.appendChild(sideMenu)

        this.btnRunSlot = document.createElement('div')
        this.btnRunSlot.classList.add('common-horizontal-button-slot')
        const btnRun2D = createButton('2D', this.btnRunSlot)
        const btnRun3D = createButton('3D', this.btnRunSlot)
        const btnCompile = createButton('Compile', this.btnRunSlot, "#fd9353")
        const btnRunExplore = createButton('Explore', this.btnRunSlot)
        const btnCompilePython = createButton('Compile python', this.btnRunSlot, "#fd9353")
        container.appendChild(this.btnRunSlot)
        this.refreshBtnVisibility = (paramItem) => {
            paramItem = paramItem || this.currentItemParam
            const buildItem = paramItem?.parent || this.currentItemBuild
            if (buildItem.failedCompile){
                btnRun2D.style.display = 'none'
                btnRun3D.style.display = 'none'
                btnCompile.style.display = null
            } else {
                btnRun2D.style.display = null
                btnRun3D.style.display = null
                btnCompile.style.display = 'none'
            }
            if (buildItem.failedCompileExplore){
                btnRunExplore.style.display = 'none'
                btnCompilePython.style.display = null
            } else {
                btnRunExplore.style.display = null
                btnCompilePython.style.display = 'none'
            }
            if (!paramItem) {
                btnRun2D.style.display = 'none'
                btnRun3D.style.display = 'none'
                btnRunExplore.style.display = 'none'
            }
        }

        this.btnShowErrSlot = document.createElement('div')
        this.btnShowErrSlot.classList.add('common-horizontal-button-slot')
        this.btnShowErrSlot.style.display = 'none'
        const btnShowErr = createButton('Show last compilation Error', this.btnRunSlot, "var(--empty-activity)")
        btnShowErr.addEventListener('click',()=>BuilderManager.errModel.show());
        this.btnShowErrSlot.appendChild(btnShowErr);
        container.appendChild(this.btnShowErrSlot);

        //if (BuilderManager.isFailToBuild() && this.currentItemBuild.failedCompile) BuilderManager.errModel.show();
        this.btnViewSlot = document.createElement('div')
        this.btnViewSlot.classList.add('common-horizontal-button-slot')
        this.btnViewSlot.style.display = 'none'
        const btnView = createButton('VIEW', this.btnViewSlot)
        container.appendChild(this.btnViewSlot)

        btnView.addEventListener('click', () => {
            document.getElementById('viewer').parentElement.style.display = null
            ActivityManager.toggleActivity('viewer')
            ViewerManager.view(this.currentItemViewer)
        })
        btnRun3D.addEventListener('click',()=>{
            document.getElementById('viewer').parentElement.style.display = null
            ActivityManager.toggleActivity('viewer')
            ViewerManager.run(this.currentItemParam, false)
            //this.paramGUI.style.pointerEvents = 'none';
        })
        btnRun2D.addEventListener('click', () => {
            document.getElementById('viewer').parentElement.style.display = null
            ActivityManager.toggleActivity('viewer')
            ViewerManager.run(this.currentItemParam, true)
            //this.paramGUI.style.pointerEvents = 'none';
        })
        btnRunExplore.addEventListener('click', () => {
            const buildItem = this.currentItemParam.parent
            if (buildItem.failedCompileExplore) {
                BuilderManager.build('all',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, newToken) => {
                    if (err) ActivityManager.toggleActivity('globallist_btn')
                    buildItem.failedCompileExplore = err
                    buildItem.failedCompile = err
                    buildItem.token = newToken
                    buildItem.compiledBy = loggingManager.username
                    if (!err) {
                        document.getElementById('explore').parentElement.style.display = null
                        ActivityManager.toggleActivity('explore')
                        ExploreManager.run(this.currentItemParam)
                    }
                    if (err) this.btnShowErrSlot.style.display = null
                    else this.btnShowErrSlot.style.display = 'none'
                    projectManager.save(true);
                })
            } else {
                document.getElementById('explore').parentElement.style.display = null
                ActivityManager.toggleActivity('explore')
                ExploreManager.run(this.currentItemParam)

            }
        })
        btnCompile.addEventListener('click', () => {
            const buildItem = this.currentItemParam?.parent || this.currentItemBuild
            if (buildItem.failedCompile) {
                BuilderManager.build('sql',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, newToken) => {
                    buildItem.token = newToken
                    buildItem.failedCompile = err
                    buildItem.failedCompileExplore = true
                    buildItem.compiledBy = loggingManager.username
                    this.refreshBtnVisibility()
                    if (err) this.btnShowErrSlot.style.display = null
                    else this.btnShowErrSlot.style.display = 'none'
                    projectManager.save(true);
                })
            } else {
                this.refreshBtnVisibility()
            }
        })
        btnCompilePython.addEventListener('click', () => {
            const buildItem = this.currentItemParam?.parent || this.currentItemBuild
            if (buildItem.failedCompileExplore) {
                BuilderManager.build('all',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, newToken) => {
                    buildItem.failedCompileExplore = err
                    buildItem.failedCompile = err
                    buildItem.token = newToken
                    buildItem.compiledBy = loggingManager.username
                    if (!err) {
                        document.getElementById('notebook').parentElement.style.display = null
                        //ActivityManager.toggleActivity('notebook')
                    } else {
                        ActivityManager.toggleActivity('globallist_btn')
                        document.getElementById('notebook').parentElement.style.display = 'none'
                    }
                    this.refreshBtnVisibility()
                    if (err) this.btnShowErrSlot.style.display = null
                    else this.btnShowErrSlot.style.display = 'none'
                    projectManager.save(true);
                })
            } else {
                this.refreshBtnVisibility()
                document.getElementById('notebook').parentElement.style.display = null
                //ActivityManager.toggleActivity('notebook')
            }
        })

        this.buildSection = document.createElement('custom-section')
        this.buildSection.classList.add('title')
        this.buildSection.titleText = 'Builds'
        sideMenu.appendChild(this.buildSection)
        

        this.paramGUI = document.createElement('parameter-widget')
        this.paramGUI.id = 'parametersGUI'

        this.paramGUI.addEventListener('mouseleave', () => {
            if (this.currentItemParam){
                if (this.paramGUI.changeDetected) {
                    this.dateLastChange = Date.now();
                }
            }
        })

        document.getElementById('contentView').appendChild(this.paramGUI)


        this.currentItemBuild = undefined //{ token: 'sim_2022213_1944_893', failed: true }
        this.currentItemParam = undefined
        this.currentItemViewer = undefined
        this.changeTracked = true;
        this.modalOnChange = createModalOnChange()
        this.modalLoad = createModalLoad()
        this.modalImportExportJsonParam = createModalJSONParamEdit()


    }

    onClickActivity() {
        if (this.buildSection.items.length>0) {
            if (this.currentItemBuild.changeDetected ) {
                if (md5(JSON.stringify(getFinalJson())) !== this.currentItemBuild.checksum)
                    this.modalOnChange.show()
                else {
                    this.currentItemBuild.changeDetected = false
                    this.currentItemBuild.changeDetectedXML = true
                }
            }
        } else {
            this.addNewBuild('version 1')
        }
        this.refreshBtnVisibility()
    }


    addNewBuild(name){
        const buildItem = createBuildItem()
        buildItem.titleText = name
        buildItem.token = 'noToken'
        buildItem.updateFromDiagram()
        this.buildSection.addItem(buildItem)
        const paramItem = this.createParamItem()
        buildItem.copyParamFromItem(this.currentItemParam,paramItem)
        buildItem.addItem(paramItem)
        if (this.currentItemBuild) this.currentItemBuild.classList.toggle('active')
        this.currentItemBuild = buildItem
        this.currentItemBuild.classList.toggle('active')
        return buildItem
    }

    createViewerItem(token,is2D) {
        const viewerItem = document.createElement('custom-item')
        viewerItem.editable = true
        viewerItem.deletable = ()=>{
            navigator.sendBeacon("/api/kill/" + viewerItem.token, JSON.stringify({ event: "beforeunload event", deleteSimu: true }))
            this.btnRunSlot.style.display = null
            this.btnViewSlot.style.display = 'none'
        }
        viewerItem.token = token
        viewerItem.is2D = is2D
        viewerItem.valueText = token
        viewerItem.addEventListener('click', () => {
            if (this.currentItemParam) this.currentItemParam.classList.remove('active')
            if (this.currentItemViewer) this.currentItemViewer.classList.remove('active')
            this.btnRunSlot.style.display = 'none'
            this.btnViewSlot.style.display = null
            this.currentItemViewer = viewerItem
            this.currentItemViewer.classList.add('active')
        })
        return viewerItem;
    }

    createParamItem(param,config,name='param') {
        const paramItem = document.createElement('custom-section')
        paramItem.deletable = ()=>{
            paramItem.items.forEach(viewerItem=>{
                navigator.sendBeacon("/api/kill/" + viewerItem.token, JSON.stringify({ event: "beforeunload event", deleteSimu: true }))
            })
            this.btnRunSlot.style.display = null
            this.btnViewSlot.style.display = 'none'
            if (paramItem === this.currentItemParam){
                unselectParam()
                this.refreshBtnVisibility()
            }
        }
        paramItem.editable = true
        paramItem.toggleable = true
        paramItem.titleText = name
        if(param) paramItem.param = JSON.parse(JSON.stringify(param))
        if(config) paramItem.config = JSON.parse(JSON.stringify(config))
        paramItem._title.addEventListener('click', () => {
            if (this.currentItemParam) this.currentItemParam.classList.remove('active')
            if (this.currentItemViewer) this.currentItemViewer.classList.remove('active')
            if (!paramItem.parent.failedCompile){
                this.updateChecksumBuild(paramItem.parent).then(() => {
                    this.refreshBtnVisibility(paramItem)
                    this.btnRunSlot.style.display = null
                })
            } else {
                this.refreshBtnVisibility(paramItem)
                this.btnRunSlot.style.display = null
            }
            this.btnViewSlot.style.display = 'none'
            this.currentItemParam = paramItem
            this.currentItemParam.classList.add('active')
            this.updateParamWidget()
        })
        return paramItem
    }

    changeDetected(){
        if (this.currentItemBuild && this.changeTracked) {
            this.currentItemBuild.changeDetected = true
            this.currentItemBuild.changeDetectedXML = true
            this.dateLastChange = Date.now()
        }
    }

    updateParamWidget(buildItem){
        buildItem = buildItem || this.currentItemParam.parent
        const color = getDefaultColor(buildItem.finalJson.cellTypes)
        if (buildItem.color.type) Object.keys(color).forEach((i) => color[i] = buildItem.color.type[i] ? buildItem.color.type[i] : color[i])
        this.paramGUI.setup(this.currentItemParam.param,this.currentItemParam.config, color ,(j)=>{
            this.modalImportExportJsonParam.updateParams(j)
            this.modalImportExportJsonParam.show()
        })
    }

    getData() {
        if (!this.buildSection) return undefined
        if (!this.currentItemBuild) return undefined
        return {build: this.buildSection.items.map(b=>{
            return{name:b.titleText,
                token: b.token,
                checksum: b.checksum,
                finalJson:b.finalJson,
                xml: b.xml,
                date: b.date,
                failedCompile: b.failedCompile,
                failedCompileExplore: b.failedCompileExplore,
                changeDetected: b.changeDetected,
                changeDetectedXML: b.changeDetectedXML,
                color: b.color,
                plots: b.plots,
                pythonCode: b.pythonCode,
                compiledBy: b.compiledBy,
                params:b.items.map(p=>{
                    return {name:p.titleText,param:p.param,config:p.config}
                })}}),
            currentBuild: this.buildSection.items.indexOf(this.currentItemBuild)}
    }


    loadData(data) {
        this.clear();
        if (!data.build) return undefined

        data.build.forEach((b,i)=>{
           const buildItem = createBuildItem()
            buildItem.titleText = b.name
            buildItem.token = b.token
            buildItem.xml = b.xml
            buildItem.date = b.date
            buildItem.checksum = b.checksum
            buildItem.finalJson = b.finalJson
            buildItem.failedCompile = b.failedCompile
            buildItem.failedCompileExplore = b.failedCompileExplore !== undefined ? b.failedCompileExplore : true
            buildItem.changeDetected = b.changeDetected
            buildItem.changeDetectedXML = b.changeDetectedXML
            buildItem.color = b.color
            buildItem.plots = b.plots
            buildItem.pythonCode = b.pythonCode
            buildItem.compiledBy = b.compiledBy
            b.params.forEach(p => {
                buildItem.addItem(this.createParamItem(p.param, p.config, p.name))
            })
            this.buildSection.addItem(buildItem)
            if(i === data.currentBuild) {
                this.currentItemBuild = buildItem
            } else {
                if (!b.failedCompile) this.updateChecksumBuild(buildItem)
            }
        })
        if(!this.currentItemBuild) this.currentItemBuild = this.buildSection.items[this.buildSection.items.length-1]
        const b = this.currentItemBuild
        b.classList.toggle('active')
        if (b.failedCompileExplore) document.getElementById('notebook').parentElement.style.display = 'none'
        else if (!b.failedCompile) this.updateChecksumBuild(b).then(() => this.refreshBtnVisibility())
    }

    updateChecksumBuild(itemBuild) {
        return fetchW('/api/checksum/' + itemBuild.token, { method: 'GET' }).then(res => res.json()).then(data => {
            const isCurrent = itemBuild === this.currentItemBuild
            if (!data.err) {
                itemBuild.checksum = itemBuild.checksum ? itemBuild.checksum : md5(JSON.stringify(itemBuild.finalJson))
                if (itemBuild.checksum === data.checksum) {
                    if (!itemBuild.failedCompileExplore && isCurrent) document.getElementById('notebook').parentElement.style.display = null
                } else {
                    itemBuild.failedCompile = true;
                    itemBuild.failedCompileExplore = true;
                    if (isCurrent) document.getElementById('notebook').parentElement.style.display = 'none'
                }
            } else {
                itemBuild.failedCompile = true;
                itemBuild.failedCompileExplore = true;
                if (isCurrent) document.getElementById('notebook').parentElement.style.display = 'none'
            }
        })
    }

    clear(){
        this.buildSection.clear()
        this.currentItemBuild = undefined
        this.paramGUI.clear()
    }

    getCurrentItemBuild() {
        return this.currentItemBuild
    }
    getParamFromDiagram() {
        let defaultValue = (type) => {
            if (type === 'int' || type === 'double') return 1
            if (type === 'bool') return false
            if (type === 'Protocol') return []
        }
        let cell = Object.fromEntries(document.getElementById('Cell Attributes').items.filter(i => i.hasTag('MODEL')).map(i => [i.valueText, defaultValue(i.type)]))
        let param = {
            'Cell': Object.fromEntries([...document.getElementById('Cell Types').items.map(t => t.valueText), ...['Common']].map(t => [t, cell])),
            'Scenario': Object.fromEntries(document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('MODEL')).map(i => [i.valueText, defaultValue(i.type)])),
        }
        param['Scenario']['dt'] = 360
        return param
    }

    getParamFromItemParam(itemParam) {
        return this.paramGUI.getJsonParameter(itemParam.param,itemParam.config)
        /*
        const flattenConfig = JSON.flatten(paramItem.config)
        return JSON.unflatten(Object.fromEntries(Object.entries(JSON.flatten(itemParam.param)).filter(([k, v]) => flattenConfig[k]?!flattenConfig[k].hide:true)))*/
    }

}

const parametersInstance = new Parameters();
export default parametersInstance;