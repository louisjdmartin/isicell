import { isCppCodeValid } from "../utils/utilsVaraibleName";
import { quickToast, longToast } from '../guiComponent/notification'
import SearchManager from "./searchManager";
import stateMachine from "../diagrams/stateMachine";
import behaviorDiagram from "../diagrams/behaviorDiagram";


/**
 * Generates a Universally unique
 */
function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}


/**
 * Computes the behavior flow graph recursively and return the final code with eventual errors
 * @param {*} currentId The id of the flow element on which to do the recursion
 * @param {*} flows The complete flow graph
 * @param {*} lvl The current indentation level of the element
 * @param {*} parents The parents it went through in the flow graph before currentId
function recursive(currentId,flows,lvl,parents){
    var targetFlows = flows.filter((f)=>{return f.source.id===currentId})
    var code = ""
    var tab = '\t'

if ((parents !== undefined && parents.length > 0) && parents.includes(currentId))
    return { code, error: { idElement: currentId, text: "Unhandeled loop detected" } }

var elseCondition = targetFlows.filter((t) => { return (t.condition === undefined) })
if (elseCondition.length > 1)
    return { code, error: { idElement: elseCondition.map((e) => { return e.idFlow }), text: "too many undefined conditions" } }

// Orders the targets to have the else at the end if there is one
targetFlows.sort((a, b) => {
    if (a.condition !== undefined && b.condition !== undefined)
        return (a.target.di.bounds.x - b.target.di.bounds.x)
    else return (a.condition === undefined) ? 1 : -1;
})

if (lvl !== 0 || flows.filter((f) => { return f.target.id === currentId }).length !== 0) { // If this is not the eventStart
    // write the function call
    var call = flows.filter((f) => { return f.target.id === currentId })[0].target.get('call') + ';\n'
    code += tab.repeat(lvl) + call
}

// Go in all branches
if (targetFlows.length < 1) { // If it's an end node
    return { code, error: false }
} else { // If it's not an end node
    var ret = false
    if (targetFlows.length === 1 && targetFlows[0].condition === undefined) { // If there is only one target without condition
        ret = recursive(targetFlows[0].target.id, flows, lvl, [...parents, currentId])
        code += ret.code // write returned code
    } else { // If there's a bunch of targets
        // Loop through all the targets writting the conditions at the same time
        var error = false
        targetFlows.forEach((targetFlow, i) => {
            if (i === 0) { // first condition of multiple conditions has to be an if
                code += tab.repeat(lvl) + 'if(' + targetFlow.condition + '){\n'
            }
            else if (targetFlow.condition === undefined) { // if there is an else
                code += tab.repeat(lvl) + '} else {\n'
            }
            else { // All the inbetween elif
                code += tab.repeat(lvl) + '} else if(' + targetFlow.condition + '){\n'
            }
            ret = recursive(targetFlow.target.id, flows, lvl + 1, [...parents, currentId])
            if (ret.error) error = ret.error
            code += ret.code // write returned code
        });
        ret.error = error
        code += tab.repeat(lvl) + '}\n' // close condition
    }
    return { code, error: ret ? ret.error : { idElement: currentId, text: "unexpected" } }
}
}
 */
/**
 * Computes the behavior flow graph recursively and return the final code with eventual errors
 * @param {*} currentId The id of the flow element on which to do the recursion
 * @param {*} flows The complete flow graph
 * @param {*} lvl The current indentation level of the element
 * @param {*} parents The parents it went through in the flow graph before currentId
 **/
function recursive(currentId,flows,lvl,parents){
    var targetFlows = flows.filter((f)=>{return f.source.id===currentId})
    var sourceFlows = flows.filter((f) => { return f.target.id === currentId })
    var element = sourceFlows[0]?.target
    var code = ""
    var tab = '\t'

    /**
     * Error Handling
     */
    if((parents!==undefined && parents.length>0) && parents.includes(currentId)){
        var pbParentFlow = parents[Math.max(...sourceFlows.map(f => parents.indexOf(f.source.id)))]
        return { code, error: { idElement: sourceFlows.filter(f => f.source.id === pbParentFlow).map(f => f.idFlow),text:"Unhandeled loop detected"}}
    }
    var error = false
    if(element){ // If this is not the eventStart
        if(element.$type === 'bpmn:Task')
            code += tab.repeat(lvl) + element.get('call') + ';\n'

        if (element.$type === 'bpmn:Task' && targetFlows.length === 1) {
            var ret = recursive(targetFlows[0].target.id, flows, lvl, [...parents, currentId])
            code += ret.code // write returned code
            error = ret.error
        } else if (element.$type === 'bpmn:Gateway' && targetFlows.length === 2) {
            if (!element.get('name')) error = { idElement: [currentId], text: "condition activity without condition code" }
            code += tab.repeat(lvl) +'if(' + element.get('name') + '){\n'
            var yesCond = targetFlows.filter(f => f.condition === 'YES')[0]
            if (yesCond){
                yesCond = recursive(yesCond.target.id, flows, lvl + 1, [...parents, currentId])
                code += yesCond.code +
                    tab.repeat(lvl) + '} else {\n'
                error = error ? error : yesCond.error
                var noCond = targetFlows.filter(f => f.condition === 'NO')[0]
                if(noCond){
                    noCond = recursive(noCond.target.id, flows, lvl + 1, [...parents, currentId])
                    code += noCond.code +
                        tab.repeat(lvl) + '}\n'
                    error = error?error:noCond.error
                } else {
                    error = {idElement: [currentId], text: "unexpected"}
                }
            } else {
                error = {idElement: [currentId], text: "unexpected"}
            }
        } else if (element.$type === 'bpmn:Gateway' && targetFlows.length === 1) {
            if (!element.get('name')) error = { idElement: currentId, text: "condition activity without condition code" }
            var cond = targetFlows[0].condition === 'NO' ? '!(' + element.get('name') + ')' : element.get('name')
            code += tab.repeat(lvl) + 'if(' + cond + '){\n'
            var yesCond = recursive(targetFlows[0].target.id, flows, lvl + 1, [...parents, currentId])
            code += yesCond.code +
                tab.repeat(lvl) + '}\n'
            error = error ? error : yesCond.error
        } else if (element.$type === 'bpmn:Gateway' && targetFlows.length === 0) {
            error = { idElement: [currentId], text: "condition without outgoing" }
        }
    } else if(targetFlows.length === 1){
        var ret = recursive(targetFlows[0].target.id, flows, lvl, [...parents, currentId])
        code += ret.code // write returned code
        error = ret.error
    }
    return { code, error }
}

/**
 * Singleton class managing the function calls and definitions
 * for the different categories
 */
class FunctionManager {

    // Singleton and list initialisation
    constructor(){
        this.categories = {}
        this.infoHeader = {}
        this.updater = []
    }

    /**
     * Adds or creates a function in the manager and returns the id of the function
     * @param {*} category The category of the function
     * @param {*} id The id of the function (-1 if new)
     * @param {*} code The code of the function
     */
    addOrUpdateFunction(category,id,code){
        if(!this.categories[category]) this.categories[category] = {}
        if(id===-1) {
            id=create_UUID();
        }
        let info = this.extractHeaderCppFunction(code);

        this.setInfo(id,info)
        this.categories[category][id] = code;
        if (!isCppCodeValid(code))
            longToast.fire({
                icon: 'warning',
                title: 'It seems your C++ code for '+this.extractHeaderCppFunction(code).functionName + ' may be faulty.'
            })
        this.update();
        SearchManager.updateIndexFunction(id, category, info.functionName+'(...)',code)
        return id
    }

    /**
     * Checks fot function prototype availability and modifies the info and code if necessary
     * @param {*} info the info of the function to check
     * @param {*} category the category of the function to check
     * @param {*} code the full code of the function to check
     */
    checkFunctionAvailability(id, category, code){
        let info = this.extractHeaderCppFunction(code);
        let regex = /(?:(?<value>.+)_(?<count>\d+$))|(?<value2>.+)/;
        let oldName = info.functionName.replaceAll(/[^\w\d_]/g, '\\$&');
        info.functionName = info.functionName.replaceAll(/[^\w\d_]/g,'_');

        let checkFuncNameInfo = info.functionName.match(regex).groups;
        checkFuncNameInfo.value = checkFuncNameInfo.value ? checkFuncNameInfo.value : checkFuncNameInfo.value2;
        let copyCount = -1;
        let nameExisting = false;
        for(let funcId in this.categories[category]){
            if(funcId !== id){
                let funcInfo = this.getInfo(funcId);
                let funcNameInfo = funcInfo.functionName.match(regex).groups;
                funcNameInfo.value = funcNameInfo.value ? funcNameInfo.value : funcNameInfo.value2;

                nameExisting |= (funcInfo.functionName === info.functionName);
                if(funcNameInfo.value === checkFuncNameInfo.value){
                    let count = (funcNameInfo.count === undefined) ? 0 : Number(funcNameInfo.count);
                    copyCount = Math.max(copyCount,count);
                }
            }
        }

        if(copyCount >= ((checkFuncNameInfo.count === undefined) ? -1 : Number(checkFuncNameInfo.count))){
            copyCount++;
            let checkedFuncName = info.functionName;
            if(nameExisting)
                checkedFuncName = checkFuncNameInfo.value+((copyCount>0) ? "_"+copyCount : "");

            let replaceRegex = new RegExp('(?<=\\s|\\*|&|^)'+oldName+'(?=\\(|<)','g');
            info.functionName = checkedFuncName;
            code = code.replaceAll(replaceRegex, checkedFuncName);
        }

        return {info, code};
    }

    /**
     * Add a function call the the updater
     * @param {*} fun The function to add to the updater
     */
    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        for(var fun of this.updater){
            fun()
        }
    }

    /**
     * Returns the code for a wanted function
     * @param {*} category The category to which the wanted function belongs
     * @param {*} id The id of the function
     */
    getCode(cathegory,id){
        return this.categories[cathegory][id]
    }

    /**
     * Returns the info for a wanted function
     * @param {*} id The id of the function
     */
    getInfo(id){
        return this.infoHeader[id]
    }

    /**
     * Sets the info for a function
     * @param {*} id The id of the function
     * @param {*} info The info to set
     */
    setInfo(id,info){
        return this.infoHeader[id] = info
    }
    /**
     * remove the info for a function
     * @param {*} id The id of the function
     */
    removeInfo(id) {
        delete this.infoHeader[id]
    }

    /**
     * Returns the call for the wanted function
     * @param {*} category The category of the function (obsolete as of now)
     * @param {*} id The id of the function
     * @param {*} prevFunCall The call know by the diagram before calling this function
     */
    getFunctionCall(category,id,prevFunCall=''){
        var info = this.getInfo(id)
        if(prevFunCall !== ''){ // If there was a previous call
            try { // Returns the new call without destroying return variables or such
                var regex = / *(?:(?<variable>[^=]+) *[=| ])* *(?<functionName>\w+)\((?<params>.*)\)/m;
                var res = regex.exec(prevFunCall).groups

                res.paramsArr = res.params.split(',').map((val,ind,arr)=>arr[ind] = val.trim());
                if(res.paramsArr[0] === "") res.paramsArr = [];

                var ret = ''
                //ReturnType
                if(info.returnType!=='void')
                    ret += (res.variable?res.variable+'= ':'');
                //FunctionName
                ret += info.functionName+'('
                //Parameters
                let paramDiff = info.params.length - res.paramsArr.length;
                if(paramDiff > 0){
                    let params = [...res.paramsArr];
                    for(let i=res.paramsArr.length; i<info.params.length; ++i)
                        params.push("_"+info.params[i]);
                    ret += params.join(', ');
                }
                else if(paramDiff < 0){
                    let params = [...res.paramsArr];
                    params = params.slice(0, info.params.length);
                    ret += params.join(', ');
                }
                else{
                    ret += res.paramsArr.join(', ');
                }
                //THE END
                ret += ')'
                return ret
            } catch(e) {
                console.error("Error in getFunctionCall", e);
                return undefined;
            }
        } else { // If it's the first time just return the call
            return info.functionName+'('+info.params.join(', ')+')'
        }
    }

    parseFunctionCall(call){
        var regex = / *(?:(?<variable>[^=]+) *[=| ])* *(?<functionName>\w+)\((?<params>.*)\)/m;
        var res = regex.exec(call).groups;
        res.params = res.params.split(',').map((val,ind,arr)=>arr[ind] = val.trim());
        return res;
    }

    /**
     * Returns the header of a C++ function
     * @param {*} code The C++ function to process
     */
    extractHeaderCppFunction(code){
        try {
            // var regex = /(?:\w* )*(?<returnType>.+) +(?<functionName>\w+) *\((?<params>.*)\)/m;
            var regex = /(?<returnType>.+) +(?<functionName>.+) *\((?<params>.*)\)/m; //? To take spaces in types into account ?
            var res = regex.exec(code).groups
            res.returnType = res.returnType
            // if(res.params)res.params = Array.from(res.params.matchAll(/[\w+|*]\s+(\w+)/gm), m => m[1]);
            if(res.params)res.params = Array.from(res.params.matchAll(/(?<=\s|&|\*)\w+(?=\s*,|\s*$)/gm), m => m[0]);
            else res.params = []
            return res
        } catch {
            return undefined;
        }
    }

    /**
     * Returns the processed code {code,error:{idElements,text}}
     * @param {*} idStart The id of the first element of the graph
     * @param {*} flows The complete flow graph
     */
    generateCode(idStart,flows){
        var ret = recursive(idStart,flows,0,[])
        return {code:ret.code,error:ret.error,text:ret.text}
    }

    /**
     * Serialize all the functions in a category
     * @param {*} category The category to serialize
     */
    serializeCategory(category){
        let catJson = this.getCategory(category)
        let str = JSON.stringify(catJson)
        str = str.replaceAll('\ \ \ \ ', '\\t')
        return str
    }

    /**
     * Returns the functions in a category
     * @param {*} category The category to return
     */
    getCategory(category){
        if(this.categories[category]) return this.categories[category]
        else return {}
    }

    /**
     * Returns the functions in a category
     * @param {*} category The category to return
     */
    getCategoryFormatted(category){
        if(this.categories[category])
            return JSON.parse(this.serializeCategory(category))
        else
            return {}
    }

    /**
     * Completes the manager with the functions from the json
     * @param {*} category The category of the functions
     * @param {*} json The serialized json with all the functions from a category
     */
    loadJson(category,json){
        for(let id in json){
            this.addOrUpdateFunction(category,id,json[id])
        }
    }

    forAllFunctions(category, func){
        let functions = this.getCategory(category);
        for(let key of Object.keys(functions))
            func(key, functions[key]);
    }

    /**
     * Removes a function from the manager
     * @param {*} category The category of the function
     * @param {*} id The id of the function
     */
    deleteFunction(category,id){
        SearchManager.removeIndexFunction(id)
        delete this.categories[category][id]
    }

    /**
     * Clears all the functions from the manager
     */
    clear(){
        for(let category in this.categories){
            this.categories[category] = {}
        }
        SearchManager.clearIndexFunction()
    }

    refactorCode(oldValue, newValue, cat) {
        let callRegex = new RegExp("((?<=\\s|\\W|^)" + oldValue + "(?=\\s|\\W|$)(?!\\())", "g");
        if (newValue !== oldValue && oldValue !== '') {
            //* Update behaviorDiagram xml
            stateMachine.forEachElement(cat, (xml, code, element) => {
                let res = {};

                res['xml'] = xml.replaceAll(callRegex, newValue);
                res['code'] = code.replaceAll(callRegex, newValue);
                return res;
            }, false)

            //* Update Functions
            this.forAllFunctions(cat, (id, code) => {
                let newCode = code.replace(callRegex, newValue);
                if (newCode !== code)
                    this.addOrUpdateFunction(cat, id, newCode);
            })

            //* Update stateMachine Labels
            if (cat === 'cellFunction')
                stateMachine.forEachLabel((label) => {
                    let oldLabel = label.businessObject.name
                    let newLabel = oldLabel.replace(callRegex, newValue);

                    if (newLabel !== oldLabel) {
                        label.businessObject.name = newLabel;
                        stateMachine.modeling.updateProperties(label, {
                            name: newLabel
                        });
                    }
                })

            if (behaviorDiagram.currentState !== undefined && behaviorDiagram.category === cat)
                stateMachine.viewElement(behaviorDiagram.currentState, cat);
        }
    }
}

const functionManager = new FunctionManager();
export default functionManager;
