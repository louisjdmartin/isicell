import ModalWindows from "../guiComponent/modalWindow";
import { fetchW, getLoader } from "../utils/misc";
import ansiToHtml from 'ansi-to-html';
import Parameters from "./ParametersManager";
import loggingManager from "./LoggingManager";


const ansiToHtmlConverter = new ansiToHtml({ colors: { 0: '#222222', 1: '#fc618d', 2: '#7bd88f', 11: '#fd9353', 5: '#5ad4e6', 4: '#948ae3', 6: '#847ac3', 7: '#AAA', 8: '#888', 9: '#fd698f', 10: '#7edf9f', 3: '#fce566', 12: '#9f8fef', 13: '#9f6fef', 14: '#5fdfef', 15: '#F7F1FF' } });

//const ansiToHtmlConverter = new ansiToHtml();
function term2html(msg,debug){
    const htmlCode = ansiToHtmlConverter.toHtml(msg.replaceAll("<", "&lt;").replaceAll(">", "&gt;"))
    if(debug){
        return '<code style="white-space: pre;">' + htmlCode + "</code>"
    }
    let domTmp = document.createElement("code")
    domTmp.innerHTML = htmlCode
    let txtBrut = domTmp.innerText
    txtBrut = [...txtBrut.matchAll(/(?<file>PrimoCell|Scenario)\.hpp: In member function .(?<funcName>.*?)\S:\n.*?error:(?<error1>.*?)\n\S|(?<file2>PrimoCell|Scenario)\.hpp:(?<line2>\d+):\d+: error:(?<error2>.*?)\n\S/gs)].map(i => i.groups);
    //txtBrut.matchAll(/(?:PrimoCell|Scenario)\.hpp: In member function .(.*?)\S:.*?error:(.*?)\n\S(?:.*?(?:PrimoCell|Scenario)\.hpp.*?error:(.*?)\n\S|)/gs)
    let htmlCodeSimple = ""
    for(let f of txtBrut){
        if (f.funcName)
            htmlCodeSimple += '<span style="color:#7bd88f">' + f.file +
            '</span> In function <span style="color:#fd9353">' + f.funcName.replace(/^.*::/, '').replaceAll("<", "&lt;").replaceAll(">", "&gt;") +
             '</span> <span style="color:#fd698f">error :</span>\n<span style="color:#F7F1FF">' +
                f.error1.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replace('\n','</span>\n<span style="color:#fd698f">')+'</span>\n'
        else
            htmlCodeSimple += '<span style="color:#7bd88f">' + f.file2 +
                '.hpp</span> In line <span style="color:#fd9353">'+f.line2+'</span> <span style="color:#fd698f">error :</span>\n<span style="color:#F7F1FF">' +
                f.error2.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replace('\n', '</span>\n<span style="color:#fd698f">') + '</span>\n'
    }
    if(htmlCodeSimple!==''){
        htmlCodeSimple= '<code style="white-space: pre;">'+htmlCodeSimple+"</code>"//.replaceAll('\n', '<br>').replaceAll('  ', '&nbsp;&nbsp;').replaceAll('&nbsp; ', '&nbsp;&nbsp;')
        let btn = '<p><a id="showHideErrorCompile" style="cursor: pointer;color: var(--theme);text-decoration: underline;">Show/hide true compile error</a></p>'
        return htmlCodeSimple+btn+'<code style="white-space: pre;display:none">' + htmlCode + "</code>"
    } else {
        return '<code style="white-space: pre;">' + htmlCode + "</code>"
    }
}
/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class BuilderManager {

    // Singleton initialisation
    constructor() { 
        let d = document.createElement('div')
        this._errModel = new ModalWindows('Compilation Error',d);
        this._errModel.setInfo = (value)=>{
            d.innerHTML = value
        }
    }

    get errModel(){
        return this._errModel
    }

    build(type,token,compiledBy,finalJson,callback,params){
        console.log(loggingManager.username,'==',compiledBy)
        token = loggingManager.username===compiledBy?token:'noToken'
        const loader = getLoader()
        loader.setText('Compiling. This can take several minutes.');
        loader.show();
        fetchW('/api/compile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ data: finalJson, token, type,params })
        }).then(res => res.json()
        ).then((data) => {
            loader.hide();
            if(type === 'debug') {
                this.errModel.setInfo(term2html(data.error+"\n\n"+data.gdb,true), 'code');
                this.errModel.show();
                if (callback) callback(true, data.token)
            } else if (data.error !== undefined) {
                this.errModel.setInfo(term2html(data.error), 'code');
                let btnShowHide = document.getElementById('showHideErrorCompile')
                if(btnShowHide) btnShowHide.onclick = function(){
                    let c=this.parentElement.nextElementSibling.style;
                    c.display=(c.display)?null:'none';
                }
                this.errModel.show();
                if (callback) callback(true, data.token)
            }
            else {
                if (callback) callback(false, data.token)
            }
            Parameters.changeDetected()
        }).catch((err) => { loader.hide(); console.error(err) });
    }

    isFailToBuild(){
        return this.failToBuild;
    }
}

const builderManager = new BuilderManager();
export default builderManager;