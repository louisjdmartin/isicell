import CustomModulesManager from './customModulesManager';
import '../css/modules.css'


/**
 * Function returning the html for a module item to add in the module selector
 * @param {*} moduleData The module's data
 * @param {*} num The module's index in the modules list
 */
function addItem(moduleData,num){
    let htmlText = '<input type="checkbox" data-num="'+num+'" id="plug-select-'+num+'" class="module-select-input"/>\
    <label for="plug-select-'+num+'" class="module-select-label">\
        <a class="module-select-badge" style="background-color:var(--theme);position:absolute;top:34px;right:0;">⬇</a>\
        <div class="module-select-text">\
        <p class="module-select-text-title">'+moduleData.Name+'</p>\
        <p class="module-select-text-description">'+moduleData.Description+'</p>\
        <p class="module-select-content-badge"><span class="module-select-badge module-color-type">'+moduleData.Type+'</span>';
        for(let dependency of moduleData.Dependencies){
            htmlText+='<span class="module-select-badge module-color-dependencies">'+dependency+'</span>';
        }
        htmlText+='</p>\
        </div>\
    </label>';
    var template = document.createElement('div');
    htmlText = htmlText.replace('\n','').trim();
    template.innerHTML = htmlText;
    template.getElementsByTagName('a')[0].addEventListener('click', (e) => {
        e.preventDefault();
        CustomModulesManager.importExistingModule(moduleData)
    })
    return template;
}

/**
 * Singleton class listing and managing all the modules.
 * It also contains the module selector modal window
 */
class ModuleManager {

    // Singleton initialisation
    constructor(){  }

    /**
     * This function initialises the singleton
     * using modulesData
     * @param {*} modulesData The modules json config listing everything
     */
    init(mainContainer,modulesData){
        this.selectedModules = []
        this.updater=[]

        this.modulesData = modulesData.Modules
        this.defaultData = modulesData.Default
        this.defaultData['Body'] = {'functions':[]}
        this.defaultData['Plugin'] = {'functions':[]}
        this.container = document.createElement('div')
        this.container.id = 'moduleSelector'
        this.container.style="height: calc(100% - 10px);padding: 10px 5px 0 5px;overflow-y: auto;"
        

        let htmlType = ''
        let typesList = []
        let htmlDependencies = ''
        let dependenciesList = []
        for(let i=0; i<this.modulesData.length;i++){
            let type = this.modulesData[i].Type
            if(!typesList.includes(type)){
                htmlType += '<span data-mod-id="'+i+'" class="module-select-badge module-color-type" style="display:none;">'+type+'</span>';
                typesList.push(type);
            }
            for(let dependency of this.modulesData[i].Dependencies){
                if(!dependenciesList.includes(dependency)){
                    htmlDependencies += '<span data-mod-id="'+i+'" class="module-select-badge module-color-dependencies" style="display:none;">'+dependency+'</span>';
                    dependenciesList.push(dependency);
                }
            }
        }

        this.container.innerHTML ='<div class="module-select-badge-group"><div style="font-size:14px; width: 100px;">Types:</div>\
                                                <div class="module-badge-list">'+htmlType+'</div></div>\
                                                <div class="module-select-badge-group"><div style="font-size:14px; width: 100px;">Dependencies:</div>\
                                            <div class="module-badge-list">'+htmlDependencies+'</div></div>'.replace('\n','').trim();
        
        for(let i=0; i<this.modulesData.length;i++){
            this.container.appendChild(addItem(this.modulesData[i], i))/*
            */
        }
        
        this.container.querySelectorAll("input[type='checkbox']").forEach((input)=>{
            input.onchange=()=>{
                let numModule = input.getAttribute('data-num')
                if(input.checked){
                    this.container.querySelectorAll("span[data-mod-id='"+numModule+"']").forEach((span)=>{
                        span.style.display='inline-block';
                    })
                } else {
                    this.container.querySelectorAll("span[data-mod-id='"+numModule+"']").forEach((span)=>{
                        span.style.display='none';
                    })
                }
            }
        })

        mainContainer.appendChild(this.container)
        //this.container.style.display = 'none'
        /*
        function closeCallback(win){this.updateSelectedModules()}
        this.modalWindow = new ModalWindows('',this.container,closeCallback,closeCallback);
        this.modalWindow.main.firstChild.style.height = '94%'
        this.modalWindow.main.style.paddingTop = '2%'*/
    }


    
    /**
     * Returns a list of all the data given
     * from the default json and the selected modules
     */
    getDataSelectedModules(){
        return [...this.container.querySelectorAll("input[type='checkbox']:checked")].map((d)=>{return this.modulesData[d.getAttribute('data-num')]})
    }

    /**
     * Add a function call the the updater
     * @param {*} fun The function to add to the updater
     */
    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        for(var fun of this.updater){
            fun(this.selectedModules)
        }
    }

    /**
     * Checks whether the module selection changed
     * and calls the updater if it has
     */
    updateSelectedModules(){
        let newSelectedModule = this.getDataSelectedModules();
        if(newSelectedModule !== this.selectedModules){
            this.selectedModules = newSelectedModule;
            this.update()
        }
    }

    /** Returns a list of the selected modules names */
    getSelectedModulesNames(){
        return this.selectedModules.map(mod=>mod.Name)
    }

    /**
     * Checks modules in the html checkboxes and updates the manager
     * @param {*} modules A list of module names to check
     */
    importSelectedModules(modules){
        let allModulesName = this.modulesData.map((p)=>{return p.Name})
        let numModules = modules.map((p)=>{return allModulesName.indexOf(p)}).filter((v, i, a) => a.indexOf(v) === i && v >= 0)
        
        numModules.forEach(num => {
            document.getElementById("plug-select-"+num).checked = true;
        });

        this.updateSelectedModules()
        this.refreshHeader()
    }

    /**
     * Clears the Dependencies/Tags header of the module selector
     */
    refreshHeader(){
        this.container.querySelectorAll("input[type='checkbox']").forEach((input)=>{
            let numModule = input.getAttribute('data-num')
            if(input.checked){
                this.container.querySelectorAll("span[data-mod-id='"+numModule+"']").forEach((span)=>{
                    span.style.display='inline-block';
                })
            } else {
                this.container.querySelectorAll("span[data-mod-id='"+numModule+"']").forEach((span)=>{
                    span.style.display='none';
                })
            }
        })
    }

    /**
     * Unselect all the modules and clear the header
     */
    unselectedAll(){
        this.container.querySelectorAll("input[type='checkbox']").forEach((input)=>{
            input.checked = false;
        })
        this.refreshHeader();
    }
}

const moduleManager = new ModuleManager()
export default moduleManager;
