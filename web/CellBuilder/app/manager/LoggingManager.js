import { longToast } from "../guiComponent/notification";
import { fetchW } from "../utils/misc";
import ActivityManager from "./ActivityManager";
import '../css/logging.css'
var checkPassword = function (str) {
    var re = /^(?=.*\d)(?=.*[a-z]).{6,}$/;
    return re.test(str);
};



function domLogging(logged){
    let dom = document.createElement('div')
    dom.innerHTML = `<div class="login-form">
<h1>Login ISiCell</h1>
<form>
<div class="connectSlot">
<input type="text" name="username" placeholder="Username" required>
<input type="password" name="password" placeholder="Password" required>
<a class="btn connect">Connect</a>
</div>
<div class="pwdChangeSlot" style="display:none">
<input type="password" name="newPwd" placeholder="new password" required title="Password must contain at least 6 characters, including characters and numbers.">
<a class="btn pwdChange">Change password</a>
</div>
<a class="btn pwdChangeDisplay">Change password</a>
<a class="btn dashboard" href="/dashboard" target="_blank" style="display:none">Dashboard<a/>
<a class="btn disconnect">Disconnect</a>
<br>
<a class="btn demo">Use demo</a>
<p>Note: you can use the platform without logging in, however you will not be able to compile or perform risky actions for the server. Thank you for your understanding.</p>
</form></div>`
    if(logged) dom.className = 'hide'
    dom.id ='loggingContainer'
    let demoBtn = dom.getElementsByClassName('demo')[0]
    let connectSlot = dom.getElementsByClassName('connectSlot')[0]
    let disconnectBtn = dom.getElementsByClassName('disconnect')[0]
    let connectBtn = dom.getElementsByClassName('connect')[0]
    let changePwdBtn = dom.getElementsByClassName('pwdChange')[0]
    let pwdChangeSlot = dom.getElementsByClassName('pwdChangeSlot')[0]
    let changePwdDisplayBtn = dom.getElementsByClassName('pwdChangeDisplay')[0]
    let title = dom.getElementsByTagName('h1')[0]
    let newPwd = dom.querySelector('[name="newPwd"]')
    let btnDashboard = dom.getElementsByClassName('dashboard')[0]
    newPwd.addEventListener("keyup", function () {
        this.setCustomValidity(!checkPassword(this.value) ? this.title : "");
    }, false);
    if (logged) {
        connectSlot.style.display = "none"
        title.textContent = "Welcome " + loggingManager.username
        if (loggingManager.username === 'admin') btnDashboard.style.display = null
    } else {
        disconnectBtn.style.display = "none"
        changePwdDisplayBtn.style.display = "none"
    }
    let connect = ()=>{
        fetchW('/auth', {
            method: 'POST',
            headers: {'Accept': 'application/json','Content-Type': 'application/json'},
            body: JSON.stringify(getFormInfo(['username', 'password'])),
        }).then(res => res.json()).then((data=>{
            if (!data.error) {
                disconnectBtn.style.display = null
                connectSlot.style.display = "none"
                changePwdDisplayBtn.style.display = null
                if (data.username === 'admin') btnDashboard.style.display = null
                loggingManager.logged = data.username
                title.textContent = "Welcom "+data.username
                dom.querySelectorAll('input').forEach(i => i.setCustomValidity(""))
                dom.classList.add('hide')
            } else{
                dom.querySelectorAll('input').forEach(i =>i.setCustomValidity("Invalid field."))
            }
        }))
    }
    connectSlot.addEventListener('keydown',(e)=>{
        if(e.key === 'Enter') connect()
    })
    let getFormInfo = (infos) => Object.fromEntries([...new FormData(dom.querySelector('form'))].filter(([k, _]) => infos.includes(k)))
    dom.addEventListener('click',(e)=>{
        if(e.target===dom)dom.classList.add('hide')
        else if(e.target===demoBtn){
            ActivityManager.toggleActivity('viewDemos')
            dom.classList.add('hide')
        } else if (e.target === disconnectBtn){
            fetchW('/disconnect').then(res => res.json()).then(() => {
                loggingManager.logged = undefined;
                title.textContent = "Login ISiCell"
                longToast.fire({
                    icon: 'info',
                    title: 'you are logout'
                })
            })
        } else if (e.target === connectBtn) {
            connect()
        } else if (e.target === changePwdDisplayBtn) {
            disconnectBtn.style.display = null
            pwdChangeSlot.style.display = null
            connectSlot.style.display = "none"
            changePwdDisplayBtn.style.display = "none"

        } else if (e.target === changePwdBtn) {
            if(checkPassword(newPwd.value))
            fetchW('/setPwd', {
                method: 'POST',
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                body: JSON.stringify(getFormInfo(['newPwd'])),
            }).then(res => res.json()).then(data => {
                console.log(data)
                if (!data.error) {
                    console.log("valid")
                    pwdChangeSlot.style.display = "none"
                    disconnectBtn.style.display = null
                    changePwdDisplayBtn.style.display = null
                    connectSlot.style.display = "none"
                } else {
                    console.log("not valid")
                    dom.querySelectorAll('input').forEach(i => i.setCustomValidity("Invalid field."))
                }
            })
        }
    })
    return dom
}

/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class LoggingManager {

    // Singleton initialisation
    constructor() { }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(defaultData) {

        this.logged = defaultData.User.username

        this.domLoggin = domLogging(this.logged)
        document.body.appendChild(this.domLoggin)

        document.getElementById('user').addEventListener('click',()=>{
            this.domLoggin.classList.remove('hide')
        })
    }



    /**
     * Add a function call the the updater
     * @param {*} fun The function to add to the updater
     */
    addUpdater(fun) {
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update() {
        let listeType = []
        for (var fun of this.updater) {
            fun(listeType)
        }
    }


    get logged() {
        return this.username?true:false;
    }
    set logged(username) {
        if(this.logged && !username){
            this.domLoggin.getElementsByClassName('disconnect')[0].style.display = "none"
            this.domLoggin.getElementsByClassName('pwdChangeDisplay')[0].style.display = "none"
            this.domLoggin.getElementsByClassName('pwdChangeSlot')[0].style.display = "none"
            this.domLoggin.getElementsByClassName('dashboard')[0].style.display = "none"
            this.domLoggin.getElementsByClassName('connectSlot')[0].style.display = null
        }
        if(username){
            document.getElementById('user').className = 'logo-user-valid'
            document.getElementById('user').title = 'logout'
            longToast.fire({
                icon: 'info',
                title: 'you are logged as ' + username
            })
        } else {
            document.getElementById('user').className = 'logo-user-unvalid'
            document.getElementById('user').title = 'login'
        }
        this.username = username;
    }


}

const loggingManager = new LoggingManager();
export default loggingManager;