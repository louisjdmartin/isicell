import PaletteProvider from 'bpmn-js/lib/features/palette/PaletteProvider';
import BpmnModeler from 'bpmn-js/lib/Modeler';
import CustomBundle from '../customBpmn/bundle/customBundleStateMachine'
import ModalWindows from '../guiComponent/modalWindow'
import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import initialXML from '../../resources/initialStateMachine.bpmn';
import emptyStateXML from '../../resources/emptyState.bpmn';
import scenarioSVG from '../../resources/scenario.bpmn';
import FunctionManager from '../manager/functionManager';
import Parameters from '../manager/ParametersManager';
import SearchManager from '../manager/searchManager';
import BpmnRenderer from 'bpmn-js/lib/draw/BpmnRenderer'
import { cleanString } from '../utils/utilsVaraibleName';
import { quickToast } from '../guiComponent/notification';
import '../../node_modules/bpmn-js/dist/assets/diagram-js.css'
import '../../node_modules/bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'
import '../css/diagram.css'
import addEventPanSVG from '../utils/panSVG';
import ActivityManager from '../manager/ActivityManager';
import { addGridPatternToSVG } from '../utils/misc';
import behaviorDiagram from './behaviorDiagram';

const swapSection = (disp, hide) => { document.getElementById(disp).style.display = null; document.getElementById(hide).style.display = "none"; }

class StateMachine {
  constructor(){}
  async init(idContainer,parentPanelManager) {
    var self = this
    this.parentPanelManager = parentPanelManager; // used to minimize the right panel and list
    this.mainContainer = document.createElement('div')
    this.mainContainer.style.height = '100%'
    document.getElementById(idContainer).appendChild(this.mainContainer)
    this.mainContainerScenario = document.createElement('div')
    this.mainContainerScenario.style = 'height:100%;display:none;user-select: none;'
    //this.mainContainerScenario.style = 'display: flex;justify-content: center;align-items: center;height:100%;'
    this.mainContainerScenario.classList.add('djs-container')
    /*
    let svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" version="1.1">'
    svg += '<g class="viewport" transform="matrix(1,0,0,1,0,0)">'
    svg += `<defs><marker id="sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="stroke-linecap: round; stroke-linejoin: round; stroke: rgb(34, 36, 42); stroke-width: 1px; fill: rgb(34, 36, 42);"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Gateway_116hggv" style="display: block;" transform="matrix(1 0 0 1 235 435)"><g class="djs-visual"><polygon points="25,0 50,25 25,50 0,25" style="stroke-linecap: round; stroke-linejoin: round; stroke: rgb(34, 36, 42); stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="50" height="50" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="62" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_06x7v5u" style="display: block;" transform="matrix(1 0 0 1 165 335)"><g class="djs-visual"><rect x="0" y="0" width="185" height="50" rx="10" ry="10" style="stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 12px; font-weight: normal; fill: rgb(34, 36, 42);"><tspan x="29.73046875" y="28.6">loopInteraction()</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="185" height="50" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="197" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_12mzhbc" style="display: block;" transform="matrix(1 0 0 1 165 135)"><g class="djs-visual"><rect x="0" y="0" width="185" height="50" rx="10" ry="10" style="stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 12px; font-weight: normal; fill: rgb(34, 36, 42);"><tspan x="70.34375" y="28.6">init()</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="185" height="50" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="197" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0w6x04p" style="display: block;" transform="matrix(1 0 0 1 165 235)"><g class="djs-visual"><rect x="0" y="0" width="185" height="50" rx="10" ry="10" style="stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 12px; font-weight: normal; fill: rgb(34, 36, 42);"><tspan x="18.65234375" y="28.6">updateCellBehavior()</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="185" height="50" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="197" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_1ulp3l4" style="display: block;" transform="matrix(1 0 0 1 242 42)"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke-linecap: round; stroke-linejoin: round; stroke: rgb(34, 36, 42); stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="48" height="48" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0r257ha" style="display: block;"><g class="djs-visual"><path data-corner-radius="5" style="fill: none; stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; marker-end: url('#sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca');" d="M260,78L260,135"/></g><path d="M260,78L260,135" class="djs-hit djs-hit-stroke" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="254" y="72" rx="4" width="12" height="69" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0b5wvmx" style="display: block;"><g class="djs-visual"><path data-corner-radius="5" style="fill: none; stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; marker-end: url('#sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca');" d="M260,385L260,435"/></g><path d="M260,385L260,435" class="djs-hit djs-hit-stroke" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="254" y="379" rx="4" width="12" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0770hxe" style="display: block;"><g class="djs-visual"><path data-corner-radius="5" style="fill: none; stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; marker-end: url('#sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca');" d="M260,185L260,235"/></g><path d="M260,185L260,235" class="djs-hit djs-hit-stroke" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="254" y="179" rx="4" width="12" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Gateway_116hggv_label" style="display: block;" transform="matrix(1 0 0 1 295 453)"><g class="djs-visual"><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 11px; font-weight: normal; fill: var(--LSH-fg);"><tspan x="0" y="9.899999999999999">w<tspan class="LSH_a">.</tspan><tspan class="LSH_f">getNbUpdates</tspan><tspan class="LSH_p1">(</tspan><tspan class="LSH_p1">)</tspan> <tspan class="LSH_t">&gt;=</tspan> maxStep</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="195" height="17" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="207" height="29" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0tl7ad2" style="display: block;"><g class="djs-visual"><path data-corner-radius="5" style="fill: none; stroke-linecap: round; stroke-linejoin: round; stroke: black; stroke-width: 2px; marker-end: url('#sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca');" d="M260,285L260,335"/></g><path d="M260,285L260,335" class="djs-hit djs-hit-stroke" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="254" y="279" rx="4" width="12" height="62" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_14bljy7" style="display: block;"><g class="djs-visual"><path data-corner-radius="5" style="fill: none; stroke-linecap: round; stroke-linejoin: round; stroke: rgb(34, 36, 42); stroke-width: 2px; marker-end: url('#sequenceflow-end-white-hsl_225_10_15_-7zgi9ye0e3jt8yt1lpbh11qca');" d="M235,460L135,460C132.5,460,130,457.5,130,455L130,265C130,262.5,132.5,260,135,260L165,260"/></g><path d="M235,460L130,460L130,260L165,260" class="djs-hit djs-hit-stroke" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="124" y="254" rx="4" width="117" height="212" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Flow_14bljy7_label" style="display: block;" transform="matrix(1 0 0 1 193 438)"><g class="djs-visual"><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 11px; font-weight: normal; fill: var(--LSH-fg);"><tspan x="0" y="9.899999999999999">NO</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="14" height="14" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="26" height="26" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_1ulp3l4_label" style="display: block;" transform="matrix(1 0 0 1 222 25)"><g class="djs-visual"><text lineHeight="1.2" class="djs-label" style="font-family: 'Fira Code'; font-size: 11px; font-weight: normal; fill: var(--LSH-fg);"><tspan x="0" y="9.899999999999999">State start</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="75" height="14" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" rx="4" width="87" height="26" class="djs-outline" style="fill: none;"/></g></g>`
    svg += '</g>'
    svg += '</svg>'*/
    this.mainContainerScenario.innerHTML = scenarioSVG

    addGridPatternToSVG(this.mainContainerScenario.querySelector('svg'))
    this.mainContainerScenario.querySelectorAll('[data-element-id^="Activity_"] .djs-visual rect').forEach(activity=>activity.style.fill="#bbb")
    this.mainContainerScenario.querySelectorAll('.djs-element .djs-hit-stroke, .djs-element .djs-hit-click-stroke, .djs-element .djs-hit-all').forEach(activity=>activity.style="fill:none;cursor:default !important")
    this.mainContainerScenario.addEventListener('click',()=>{
      this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1)
      const selectedElement = this.mainContainerScenario.querySelector('.selected')
      if(selectedElement)selectedElement.classList.remove('selected')
    })
    
    
    const initScenario = this.mainContainerScenario.querySelector('[data-element-id="Activity_0xtzkcw"]')
    initScenario.querySelector('rect').style.fill = 'white'
    initScenario.querySelector('rect.djs-hit').style = 'fill:none;cursor:pointer !important'
    initScenario.addEventListener('click',(e)=>{
      e.stopPropagation()
      var element = this.modeler.get('elementRegistry').get('Process_1');
      const selectedElement = this.mainContainerScenario.querySelector('.selected')
      if(selectedElement)selectedElement.classList.remove('selected')
      initScenario.classList.add('selected')
      this.viewElement(element,'scenarioFunction',{complementTitle:'initialisation'})
    })
    const loopScenrio = this.mainContainerScenario.querySelector('[data-element-id="Activity_0222tf2"]')

    loopScenrio.querySelector('rect').style.fill = 'white'
    loopScenrio.querySelector('rect.djs-hit').style = 'fill:none;cursor:pointer !important'
    loopScenrio.addEventListener('click',(e)=>{
      e.stopPropagation()
      var element = this.modeler.get('elementRegistry').get('Process_1');
      const selectedElement = this.mainContainerScenario.querySelector('.selected')
      if(selectedElement)selectedElement.classList.remove('selected')
      loopScenrio.classList.add('selected')
      this.viewElement(element,'scenarioFunction',{complementTitle:'loop'})
    })
    const updateCellBehavior = this.mainContainerScenario.querySelector('[data-element-id="Activity_0w6x04p"]')
    updateCellBehavior.addEventListener('click',(e)=>{
      e.stopPropagation()
      ActivityManager.toggleActivity('cells_behavior')
    })
    updateCellBehavior.querySelector('rect').style.fill = 'white'
    updateCellBehavior.querySelector('rect.djs-hit').style = 'fill:none;cursor:pointer !important'
    document.getElementById(idContainer).appendChild(this.mainContainerScenario)
    addEventPanSVG(this.mainContainerScenario.querySelector('svg'))
    this.mainContainer.tabIndex = 0
    this.listenBusEvent = false;
    this.subDiagram = {}
    /**
     * Modal window for condition editing 
     */
    var editorContainer = document.createElement('div')
    editorContainer.style = 'height: 25px;margin-top: 5px;'
    editorContainer.id = 'conditionEditor'
    this.modalWindowCondtion = new ModalWindows('Condition',editorContainer, (win)=>{
      CodeEditorProvider.TriggerCallBackMonolineEnter('conditionEditor');
    });
    CodeEditorProvider.createEditor('conditionEditor',{monoline:true});

    this.errorDom = document.createElement('div')
    this.errorDom.className = 'panel-error'
    this.errorDom.style.display = 'none'
    this.mainContainer.appendChild(this.errorDom);
    this.hiligthElement = false;

    this.showCellsDiagram = ()=>{
      if(this.mainContainer.style.display === 'none'){
        this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1)
        behaviorDiagram.currentState = undefined;
        behaviorDiagram.currentSup = "";
        swapSection("Cell Attributes","Scenario Attributes")
        swapSection("Bodies Methods","Plugins Methods")
        swapSection("Cell Methods","Scenario Methods")
      }
      this.mainContainerScenario.style.display = 'none'
      this.mainContainer.style.display = null
    }
    this.showScenarioDiagram = ()=>{
      if(this.mainContainerScenario.style.display === 'none'){
        const selectedElement = this.mainContainerScenario.querySelector('.selected')
        if(selectedElement)selectedElement.classList.remove('selected')
        this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1)
        behaviorDiagram.currentState = undefined;
        behaviorDiagram.currentSup = "";
        swapSection("Scenario Attributes","Cell Attributes")
        swapSection("Plugins Methods","Bodies Methods")
        swapSection("Scenario Methods","Cell Methods")
      }
      this.mainContainer.style.display = 'none'
      this.mainContainerScenario.style.display = null
    }
    document.getElementById(idContainer).addEventListener('mousedown', () => {
      behaviorDiagram.behaviorContainer.tabIndex = null
      this.mainContainer.tabIndex = 0
    },true)

    /**
     * Adds the scenario niit and loop buttons on the top right corner
     * of the state diagram
     */
    /*
    var buttonEditScenar = document.createElement('div')
    buttonEditScenar.className = 'panel-button-right'
    var ScenarInitBtn = document.createElement('a')
    ScenarInitBtn.className = "logo-info"
    ScenarInitBtn.title = "protocol initialisation"
    var ScenarLoopBtn = document.createElement('a')
    ScenarLoopBtn.className = "logo-loop"
    ScenarLoopBtn.title = "protocol loop"
    buttonEditScenar.appendChild(ScenarInitBtn)
    buttonEditScenar.appendChild(ScenarLoopBtn)
    this.mainContainer.appendChild(buttonEditScenar);



    ScenarInitBtn.onclick=()=>{
      var element = this.modeler.get('elementRegistry').get('Process_1');

      for(let el of this.modeler.get('selection').get("selectedElements"))
        this.modeler.get('selection').deselect(el)
      
      this.viewElement(element,'scenarioFunction',{complementTitle:'initialisation'})
    }
    ScenarLoopBtn.onclick=()=>{
      var element = this.modeler.get('elementRegistry').get('Process_1');

      for(let el of this.modeler.get('selection').get("selectedElements"))
        this.modeler.get('selection').deselect(el)
      
      this.viewElement(element,'scenarioFunction',{complementTitle:'loop'})
    }*/



    /**
     * Create the modeler and adds all the event functions
     */
    this.modeler = new BpmnModeler({ 
      container: this.mainContainer,
      additionalModules: [CustomBundle],
      keyboard: { bindTo: this.mainContainer },
  textRenderer: {
        defaultStyle: {
          fontFamily: 'Fira Code',
        },
        externalStyle: {

          fontFamily: 'Fira Code',
        }
      }
    });
    var eventBus = this.modeler.get('eventBus');
    this.modeling = this.modeler.get('modeling');
    this.modeling.setColor = function (elements, di) {
      elements.forEach(element => self.modeler.get('commandStack').execute('element.updateProperties', { element, properties: { di } }));
    }
    this.canvas = this.modeler.get('canvas');
    addGridPatternToSVG(this.canvas._svg)

    this.viewElementResize = false;
    this.ignoreDiagramRecenter = true;
    this.lockStateMachine = false;

    /**
     * This event is called when an MPMN object is being dragged from a list or a palette
     */
    eventBus.on(['create.start','autoPlace.start'], function (context) {
      if (context.shape.type === 'bpmn:Task') {// if the object is a function
        let width = 100
        let height = 50
        context.shape.x -= (width-context.shape.width)/2
        context.shape.y -= (height-context.shape.height)/2
        context.shape.width = width;
        context.shape.height = height;
        self.task_created = context.shape;
        self.listenBusEvent = false
      }
    })

      eventBus.on('directEditing.complete', function () {
      if (self.task_created){
        if(self.checkInValidStateName(self.task_created.businessObject.name)){
          setTimeout(()=>self.modeler.get('directEditing').activate(self.task_created),10)
          return;
        }
        for(let el of self.modeler.get('selection').get("selectedElements")){
            self.modeler.get('selection').deselect(el)
          }
        
        self.task_created = undefined;
        self.listenBusEvent = true
      }
    })
    
    eventBus.on(['create.end','autoPlace.end'], function(context){
      const elements = context.type === 'autoPlace.end' ? [context.shape] : context.elements
      for(let element of elements){
        if(element.type === "bpmn:Task") {
          if (self.pastedElements[element.id]) {
            if(self.task_created === element) self.task_created = undefined;
            let idFrom = self.pastedElements[element.id]
            self.subDiagram[element.id] = { behaviorDiagram: self.subDiagram[idFrom].behaviorDiagram, code: self.subDiagram[idFrom].code, variablesList: self.subDiagram[idFrom].variablesList }
            let name = element.businessObject.name
            if(self.modeler.get('elementRegistry').filter((e) => e.type === 'bpmn:Task').map((e)=>e.businessObject.name).filter(e=>e===name).length>1) name += ' copy'
            while (self.modeler.get('elementRegistry').filter((e) => e.type === 'bpmn:Task').map((e)=>e.businessObject.name).filter(e=>e===name).length>0)
              name += ' copy'
            self.modeling.updateProperties(element, {name});
            setTimeout(()=>self.modeler.get('directEditing').activate(element),10)
            //for (let k in toUpdate) element.businessObject[k] = toUpdate[k];
            delete self.pastedElements[element.id];
          } else {
            self.serializeElement("state", "empty", element);
            self.colorState(element);
          }
        } else if (element.type === 'bpmn:EndEvent'){
          self.modeling.updateProperties(element, {name:'Dead Cell'});
        } else if (element.type !== 'bpmn:IntermediateCatchEvent' && element.type !== 'bpmn:SequenceFlow' && element.type !== "bpmn:EndEvent"){
          self.modeling.setColor([element], {
            stroke: 'var(--text-2)',
            fill: 'var(--text-2)'
          });
        }
      }
      self.checkError();
      
    });

    eventBus.on('lock.stateMachine', (event) => {
      this.lockStateMachine = event.isLock;
      if (this.lockStateMachine) this.parentPanelManager.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1)
    });
    
    /**
     * This event is called when a BPMN object is clicked
     */
    eventBus.on('element.click',function (context) {
      if(self.task_created)return false;
      if(!self.lockStateMachine && !self.timeoutCheckDblClick){
          self.timeoutCheckDblClick = setTimeout(()=>{
            self.timeoutCheckDblClick = undefined
           //if ((element.type ===  'bpmn:IntermediateCatchEvent' || element.type === 'bpmn:Task')){// && context.gfx.classList.contains('selected')) {
            self.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1)
            behaviorDiagram.currentState = undefined;
            behaviorDiagram.currentSup = "";
          },250)
      }
    });
    /**
     * This event is called when an BPMN object is double clicked
     */
    eventBus.on('element.dblclick', 10000, (context) => {
      if(self.timeoutCheckDblClick){
        clearTimeout(self.timeoutCheckDblClick)
        self.timeoutCheckDblClick = undefined
      }
      var element = context.element;
      // The element is the start/end event or a label
      if (element.type === 'bpmn:EndEvent' ||
          (element.type === 'label' && element.businessObject.$type !== 'bpmn:SequenceFlow')) {
        return false; // cancel the event
      }
      if (element.type === 'bpmn:SequenceFlow' ||
          (element.type === 'label' && element.businessObject.$type === 'bpmn:SequenceFlow')) { // The element is an arrow or an arrow's label
        CodeEditorProvider.setValue('conditionEditor',element.businessObject.name?element.businessObject.name:'')
        CodeEditorProvider.setCallBackMonolineEnter('conditionEditor',(value) => {
          if(value === '')value=undefined
          this.renameElement(element, value);
          this.modalWindowCondtion.hide()
        });
        setTimeout(()=>{this.modalWindowCondtion.show();CodeEditorProvider.getEditor('conditionEditor').focus()},10);
        return false; // will cancel event
      } else if(element.type === 'bpmn:Task' || element.type==='bpmn:IntermediateCatchEvent'){
        if(!self.lockStateMachine){
          self.viewElement(element,'cellFunction')
          return false
        }
      }
    });
/*
    eventBus.on('connect.end', (context)=>{
      let element = context.hover
      if ((element.type ===  'bpmn:IntermediateCatchEvent' || element.type === 'bpmn:Task'))
        self.viewElement(element,'cellFunction')
    });
*/
    eventBus.on('element.changed', (event) => {
      if (this.listenBusEvent) {
        let element = event.element
        if (element.type === 'bpmn:Task') {
          const elementName = element.businessObject.name
          if(self.checkInValidStateName(elementName)){
            self.task_created = element
            setTimeout(()=>self.modeler.get('directEditing').activate(element),10)
            return false;
          }
          //if(element.businessObject.name)
          SearchManager.updateInfoState(element.id,elementName)
        } else if (element.type === 'bpmn:SequenceFlow') {
          SearchManager.updateIndexStateTransition(element.id, element.businessObject.get('name'))
        }
        this.checkError();
        Parameters.changeDetected() 
      }
    });
    eventBus.on('shape.removed', (event) => {
      if (this.listenBusEvent) {
        let element = event.element
        if (element.type === 'bpmn:Task') {
          SearchManager.removeInfoState(element.id)
          if(behaviorDiagram.currentState)
          if(behaviorDiagram.currentState.id === element.id)
            this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1);
          delete this.subDiagram[element.id]
        }
      }
    });
    eventBus.on('connection.removed', (event) => {
      if (this.listenBusEvent) {
        let element = event.element
        if (element.type === 'bpmn:SequenceFlow') {
          SearchManager.removeIndexStateTransition(element.id)
        }
      }
    });
    eventBus.on('import.done', (context)=>{
      this.modeler.get('elementRegistry').forEach((element)=>{
        if(element.type === 'bpmn:Task'){
          this.colorState(element);
          SearchManager.updateInfoState(element.id, element.businessObject.name)
        } else if(element.type === 'bpmn:SequenceFlow'){
          SearchManager.updateIndexStateTransition(element.id,element.businessObject.name)
        } else if (element.type === 'bpmn:IntermediateCatchEvent') {
          SearchManager.updateInfoState(element.id, element.businessObject.name)
        }
      })
      this.listenBusEvent = true
    });



    this.pastedElements = {}
    eventBus.on('copyPaste.pasteElement', (context) => {
      if (context.descriptor.type === "bpmn:Task")
      this.pastedElements[context.descriptor.businessObject.id] = context.descriptor.id
    });

    eventBus.on('copyPaste.canCopyElements',context => {
      return context.elements.filter(e => e.type !== 'bpmn:IntermediateCatchEvent')
    })

    await this.loadIntialDiagram();
  }

  checkInValidStateName(name){
    let ret = false;
    if(!name)
      ret = true
     else if(name !== cleanString(name) || !isNaN(name[0]))
      ret = true
     else if(this.modeler.get('elementRegistry').filter((e) => e.type === 'bpmn:Task').map((e)=>e.businessObject.name).filter(e=>e===name).length>1)
      ret = true
    if(ret)
      quickToast.fire({
        icon: 'error',
        title: 'invalid state name'
      });
      return ret
  }

  /**
   * Opens the behavior diagram
   * @param {*} element The state/scenarioInit/scenarioLoop to open
   * @param {*} category The category of the element (cellFunction or scenarioFunction)
   * @param {*} options Complementary options
   */
  async viewElement(element,category,options={}){
    /**
     * Initializes the behavior diagram if element is new before opening it
     */
    var sup = options.complementTitle?options.complementTitle:''; // initialisation, loop, timeline or empty string
    let prevSup = behaviorDiagram.currentSup;
    let prevState = behaviorDiagram.currentState;
    behaviorDiagram.currentSup = sup;
    var enteriesDefault;

    //* INITIALISATION, LOOP, UNDEFINED
    //behaviorDiagram.showBehavior();
    if(element.id !== "Process_1"){
      behaviorDiagram.currentState = element;
      // this.colorState(element);
    }else{
      behaviorDiagram.currentState = undefined;
    }
    let xml;
    const subDiagKey = sup?sup:element.id
    if (!this.subDiagram[subDiagKey]) this.subDiagram[subDiagKey] = {}
    if (!this.subDiagram[subDiagKey].behaviorDiagram){
      xml = emptyStateXML
      if (category === 'cellFunction' && element.type !=="bpmn:IntermediateCatchEvent") {
        xml = xml.replace('name="State start" />', 'name="State start" call="' + FunctionManager.getFunctionCall('cellFunction', 'commonBehaviorFunction') + '" idFunc="commonBehaviorFunction" />')
      }
    }
    enteriesDefault = { behaviorDiagram: xml,code:"",variablesList:[]}
    this.subDiagram[subDiagKey] = Object.assign(enteriesDefault,this.subDiagram[subDiagKey])
    
    
    // Opens the  behavior diagram and localList panels
    this.parentPanelManager.unminimize([behaviorDiagram.mainContainer.id,'panelLocalList'],-1)

    /**
     * Sets the correct functions and attributes in the lists and the correct completion
     * for the code editor depending on the category of the element
     */
    var title;
    if(category==='cellFunction'){
      title = 'Behavior for : ' + element.businessObject.name
      swapSection("Cell Attributes","Scenario Attributes")
      swapSection("Bodies Methods","Plugins Methods")
      swapSection("Cell Methods","Scenario Methods")
      swapSection("Cell functions","Scenario functions")
      swapSection("Default functions cell","Default functions scenario")
      var globalCell = ['Cell Types','Cell Attributes','Bodies Methods','Cell Methods','state','enums','Cell functions','staticMethods']
      var localCell = ['Local Variables']
      CodeEditorProvider.setEditorCompletionProvider('conditionEditor',globalCell)
      CodeEditorProvider.setEditorCompletionProvider('conditionBehaviorEditor',globalCell)
      CodeEditorProvider.setEditorCompletionProvider('editorCall',[...globalCell,...localCell])
      CodeEditorProvider.setEditorCompletionProvider('editorFunction',globalCell)
    }
    else if(category==='scenarioFunction'){
      title = 'Protocol '+options.complementTitle
      swapSection("Scenario Attributes","Cell Attributes")
      swapSection("Plugins Methods","Bodies Methods")
      swapSection("Scenario Methods","Cell Methods")
      swapSection("Scenario functions","Cell functions")
      swapSection("Default functions scenario","Default functions cell")
      var globalScenario = ['Cell Types', 'Scenario Attributes', 'Plugins Methods', 'Scenario Methods', 'enums', 'staticMethods']
      var localScenario = ['Local Variables','Scenario functions','Default functions cell']
      CodeEditorProvider.setEditorCompletionProvider('conditionEditor',globalScenario)
      CodeEditorProvider.setEditorCompletionProvider('conditionBehaviorEditor',[...globalScenario,'Scenario functions'])
      CodeEditorProvider.setEditorCompletionProvider('editorCall',[...globalScenario,...localScenario])
      CodeEditorProvider.setEditorCompletionProvider('editorFunction',[...globalScenario,'Scenario functions'])
    }
    behaviorDiagram.setTitle(title);

      /**
       * Gives to the behavior diagram the XML and local variables that it can change
       * and waits to update any changes in the callback function
       */
    await behaviorDiagram.updateDiagram(category,this.subDiagram[subDiagKey].behaviorDiagram, this.subDiagram[subDiagKey].variablesList, (xmlCallback, code, variablesList) => {
        if(this.subDiagram[subDiagKey])
          Object.assign(this.subDiagram[subDiagKey],{ behaviorDiagram: xmlCallback, code, variablesList })
      });
    

    //* Recenter state diagram
    /*if(element.id !== "Process_1" && !this.ignoreDiagramRecenter){
      if(prevState === undefined && prevSup === "") this.viewElementResize = true;
      // let currentViewbox = this.canvas.viewbox();
      let newWidth = this.canvas._container.offsetWidth;
      let newHeight = this.canvas._container.offsetHeight;

      this.canvas.viewbox({
        x: (element.x+element.width/2)-newWidth/2,
        y: (element.y+element.height/2)-newHeight/2,
        // y: currentViewbox.y, //? If it's too awkaward to center on the y
        width: newWidth,
        height: newHeight
      });
    }*/
  }

  forEachElement(category, func, ignoreCurrent=true){
    let allElements = {};
    if(category==='cellFunction'){
      allElements.elements = this.modeler.get('elementRegistry').filter(element=>((element.type === 'bpmn:IntermediateCatchEvent') || (element.type === 'bpmn:Task')));
      allElements.sups = [''];
    }
    else{
      allElements.elements = [this.modeler.get('elementRegistry').get('Process_1')];
      allElements.sups = ['initialisation','loop'];
    }
    
    for(let element of allElements.elements)
      for(let sup of allElements.sups)
        if(!ignoreCurrent || ((behaviorDiagram.currentSup !== sup) ||
          (behaviorDiagram.currentSup === "" && element?.id !== behaviorDiagram.currentState?.id))){
          const subDiagKey = sup?sup:element.id
          if (!this.subDiagram[subDiagKey]) this.subDiagram[subDiagKey] = { behaviorDiagram:'', code:'',variablesList:[]}
          const xml = this.subDiagram[subDiagKey]?.behaviorDiagram || '', 
                code = this.subDiagram[subDiagKey]?.code || ''
          let rep = func(xml, code, element, sup);

          if(rep.xml !== xml)
            this.subDiagram[subDiagKey].behaviorDiagram = rep.xml;
          
          if(rep.code !== code)
            this.subDiagram[subDiagKey].code = rep.code;
        }
  }

  forEachLabel(func){
    let allElements = this.modeler.get('elementRegistry').filter(element=>(element.type === 'label' && element.id.includes('Flow')));
    for(let element of allElements)
      func(element);
  }

  setStateChanged(element){
    if(element){
      this.serializeElement("state", "on going", element);
      this.colorState(element);
    }
  }

  renameElement(element,name){
    if (element.businessObject.name !== name)
      this.modeler.get('commandStack').execute('element.updateLabel', { element, newLabel: name })
  }

  /**
   * Return all the states from the state diagram as an object (stateName,stateCode)
   */
  getStates(){
    return this.modeler.get('elementRegistry').filter(function(element) {
      return (element.type === 'bpmn:Task');
    }).map((element)=>{
      return {name:element.businessObject.name,code:this.subDiagram[element.id]?.code || ''}
    })
  }

  /**
   * Returns all the transitions from the diagram as an object (source,target,condition)
   */
  getFlow(){
    return this.modeler.get('elementRegistry').filter(function(element) {
      return (element.type === 'bpmn:SequenceFlow');
    }).map((element)=>{return {
        source:element.source.businessObject.name,
        target:element.target.businessObject.name,
        condition:element.businessObject.name
      }})
  }

  /**
   * Returns all the transitions from the diagram formatted in an object with unique sources and several (destination,condition)
   */
  getTransitions(){
    let transitions=this.modeler.get('elementRegistry').filter(function(element) {
        return (element.type === 'bpmn:SequenceFlow');
      }).map((element)=>{return {
          source:element.source.businessObject.name,
          target:element.target.businessObject.name,
          condition:element.businessObject.name===undefined?"":element.businessObject.name
        }})
    let sources = transitions.map((e)=>{return e.source}).filter((v, i, a) => a.indexOf(v) === i);
    return sources.map((s)=>{return {from:s,destinations:transitions.filter((v)=>v.source===s).map((v)=>{return {to:v.target,condition:v.condition}})}})
  }

  /**
   * Returns the code from the initialisation event
   */
  getInitializeCode(){
    const idStart = this.modeler.get('elementRegistry').filter(element => element.type === 'bpmn:IntermediateCatchEvent')[0].id;
    return this.subDiagram[idStart]?.code || '';
  }

  serializeElement(entry, value, element, stringify=true){
    if(element){
      if(stringify) value = JSON.stringify(value);
      element.businessObject[entry] = value;
      var update = {}
      update[entry] = value
      this.modeling.updateProperties(element, update);
    }
  }

  getSerializedElement(entry, element, defaultValue=[]){
    if(element === undefined) return defaultValue;
    if (element.businessObject.get(entry) !== undefined) {
      element.businessObject[entry] = element.businessObject.get(entry)
    }
    if(element.businessObject[entry] === undefined){
      return defaultValue;
    }
    else{
      try {
        return JSON.parse(element.businessObject[entry]);
      } catch {
        return JSON.parse(JSON.stringify(element.businessObject[entry]));
      }
    }
  }

  /**
   * Returns a serialized value
   * @param {*} entry The entry to get
   * @param {*} defaultValue Default value to return if the entry is not set
   */
  getSerialized(entry,defaultValue=[]){
    var element = this.modeler.get('elementRegistry').get('Process_1');
    if(element === undefined) return defaultValue;
    if (element.businessObject.get(entry) !== undefined) {
      element.businessObject[entry] = element.businessObject.get(entry)
    }
    if(element.businessObject[entry] === undefined){
      return defaultValue;
    }
    else{
      try {
        return JSON.parse(element.businessObject[entry]);
      } catch {
        return JSON.parse(JSON.stringify(element.businessObject[entry]));
      }
    }
  }

  async loadIntialDiagram(){
    await this.importXML(initialXML);
  }

  async importXML(xml_data,subDiagram={}) {
    this.listenBusEvent =false
    this.subDiagram = subDiagram;
    try {
      await this.modeler.importXML(xml_data);
    } catch (err) {
      console.error(err.message, err.warnings);
    }

  }
  
  loadElement(element, hideAfterwards=false){
    if(element.type === 'bpmn:Task')
      this.viewElement(element,'cellFunction');

      if(hideAfterwards){
        this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1)
    }
  }

  async loadAllElements(hideAfterwards=false){
    this.ignoreDiagramRecenter = true;

    behaviorDiagram.currentState = undefined;
    var prec1 = this.modeler.get('elementRegistry').get('Process_1');
    for(let compl of ['loop','initialisation']) await this.viewElement(prec1,'scenarioFunction',{complementTitle:compl});
    
    var activities = this.modeler.get('elementRegistry').filter((element)=>(element.type === 'bpmn:Task' || element.type === 'bpmn:IntermediateCatchEvent')).sort((a,b)=>(a.type === 'bpmn:IntermediateCatchEvent')?1:-1);

    behaviorDiagram.currentState = undefined;
    for (let activity of activities) await this.viewElement(activity,'cellFunction');
    
    behaviorDiagram.currentState = undefined;
    behaviorDiagram.currentSup = "";
    if(hideAfterwards)
      this.parentPanelManager.minimize(['panelLocalList',behaviorDiagram.mainContainer.id],-1);
    
    this.ignoreDiagramRecenter = false;
  }

  colorState(element){
    if(this.getSerializedElement("state", element) == "empty"){
      this.modeling.setColor([element], {
          stroke: 'var(--empty-activity-border)',
          fill: 'var(--empty-activity)'
      });
    }
    else if(this.getSerializedElement("state", element) == "on going"){
      this.modeling.setColor([element], {
          stroke: 'var(--on-going-activity-border)',
          fill: 'var(--on-going-activity)'
      });
    }
    else if(this.getSerializedElement("state", element) == "completed"){
      this.modeling.setColor([element], {
          stroke: 'var(--completed-activity-border)',
          fill: 'var(--completed-activity)'
      });
    }
  }

  checkError(){
    let flow = this.modeler.get('elementRegistry').filter(function (element) {
      return (element.type === 'bpmn:SequenceFlow');
  }).map((element) => {
      return {
          source: element.source.businessObject,
          target: element.target.businessObject,
          condition: element.businessObject.name,
          idFlow: element.businessObject.id
      }
  })
    let idStates = flow.map((f)=>{return f.source.id}).filter((v, i, a) => a.indexOf(v) === i)
    let idElement = idStates.map((s)=>{return flow.filter((f)=>{return f.source.id===s && f.condition===undefined}).map((e)=>{return e.idFlow})})
    idElement = idElement.filter((e)=>{return e.length>1}).flat()
    if (idElement.length>1) {
      this.errorDom.style.display = 'block'
      this.errorDom.innerText = "too many undefined conditions"
      this.hiligthElement = true;
      this.modeler.get('elementRegistry').forEach(function (element, gfx) {
          if (element.type === 'bpmn:Task') {
              if (idElement.includes(element.businessObject.id)) {
                  gfx.querySelector('g rect').style.stroke = 'red'
              } else {
                  gfx.querySelector('g rect').style.stroke = 'black'
              }
          } else if (element.type === 'bpmn:SequenceFlow') {
              if (idElement.includes(element.businessObject.id)) {
                  gfx.querySelector('g path').style.setProperty('stroke','red','important')
              } else {
                  gfx.querySelector('g path').style.stroke = 'blue'
              }
          }
      });
      return undefined
  } else { // if no error is detected go back to normal and return the generated code
      if (this.hiligthElement) {
          this.errorDom.style.display = 'none'
          this.modeler.get('elementRegistry').forEach(function (element, gfx) {
              if (element.type === 'bpmn:Task') {
                  gfx.querySelector('g rect').style.stroke = 'black'
              } else if (element.type === 'bpmn:SequenceFlow') {
                  gfx.querySelector('g path').style.stroke = 'black'
              }
          })
          this.hiligthElement = false;
      }
  }
}
  
}

const stateMachine = new StateMachine();
export default stateMachine;