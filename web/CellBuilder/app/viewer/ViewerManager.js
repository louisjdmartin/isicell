import dat from '../guiComponent/dat.gui.js'
import PanelManager from "../guiComponent/panelManager";
import BufferSimulation from "./viewerComponent/BufferSimulation";
import WidgetPlayer from "./viewerComponent/widgetPlayer";
import DebugPanel from "./viewerComponent/DebugPanel";
import Viewer3D from "./viewerComponent/Viewer3D";
import Viewer2D from "./viewerComponent/Viewer2D";
import Plots from "./viewerComponent/Plots";
import WebMWriter from "webm-writer"
import { downloadFile , streamJson } from '../utils/misc.js';
import Parameters from '../manager/ParametersManager.js';
import BuilderManager from '../manager/BuilderManager.js';
import ActivityManager from '../manager/ActivityManager.js';
import '../css/viewer.css'
import { getDefaultColor, gradientPalette, gradientPaletteCSS } from '../utils/color.js';

function getColorGradient(palette,min,max,reverse,values){
    const ret = new Array(values.length*3)
    let nbColor = (palette.length - 1)
    if(reverse){
        values.forEach((value,i)=>{
            let pc = (1-Math.min(Math.max((value - min) / (max - min), 0), 1));
            const val = nbColor * pc
            const iMin = Math.floor(val)
            const c1 = palette[iMin];
            const c2 = palette[Math.ceil(val)];
            pc = val - iMin;
            ret[i*3]   = c1.r + (c2.r - c1.r) * pc
            ret[i*3+1] =  c1.g + (c2.g - c1.g) * pc
            ret[i*3+2] =  c1.b + (c2.b - c1.b) * pc
        })
    } else {
        values.forEach((value, i) => {
            let pc = Math.min(Math.max((value - min) / (max - min), 0), 1);
            const val = nbColor * pc
            const iMin = Math.floor(val)
            const c1 = palette[iMin];
            const c2 = palette[Math.ceil(val)];
            pc = val - iMin;
            ret[i*3]   = c1.r + (c2.r - c1.r) * pc
            ret[i*3+1] = c1.g + (c2.g - c1.g) * pc
            ret[i*3+2] = c1.b + (c2.b - c1.b) * pc
        })
    }
    return  ret;
}
function getColorDiscret(palette, values) {
    let colorDict = {}
    const ret = new Array(values.length * 3)
    palette.forEach((p, i) => colorDict[i] = p.match(/[A-Fa-f0-9]{2}/g).map(v => parseInt(v, 16) / 255.))
    values.forEach((v, i) => { ret[i * 3] = colorDict[v][0]; ret[i * 3 + 1] = colorDict[v][1]; ret[i * 3 + 2] = colorDict[v][2]; })
    return ret
}

function createEventLeavePage(token) {
    let deleteSimu = true
    window.addEventListener('beforeunload', (e) => {
        if (deleteSimu) {
            let confirmationMessage = "Are you sure to exit page without saving the simulation ?";
            e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
            return confirmationMessage;              // Gecko, WebKit, Chrome <34
        }
    });
    window.addEventListener('unload', () => {
        navigator.sendBeacon("/api/kill/" + token, JSON.stringify({ event: "beforeunload event", deleteSimu: deleteSimu }))
    });
}



function generateDomColorPicker(colors){
    const container = document.createElement('div')
    if (colors.palette) {
        container.classList.add('colorPickerGradient')
        const containMinMax = document.createElement('div')
        const minInput = document.createElement('input')
        minInput.type = 'text'
        minInput.value = colors.min
        minInput.onchange = () => {
            if (!isNaN(minInput.value)) { colors.min = parseFloat(minInput.value); viewerManager.player.forceUpdateView()}}
        containMinMax.appendChild(minInput)
        const maxInput = document.createElement('input')
        maxInput.type = 'text'
        maxInput.value = colors.max
        maxInput.onchange = () => { if (!isNaN(maxInput.value)) { colors.max = parseFloat(maxInput.value); viewerManager.player.forceUpdateView() }}
        containMinMax.appendChild(maxInput)
        container.appendChild(containMinMax)
        const el = document.createElement('select-gradient')
        container.appendChild(el)
        el.setGradientOptions(gradientPaletteCSS)
        el.value = colors.palette
        el.isReverse = colors.reverse
        el.onchange = (p) => { colors.palette = p; colors.reverse = el.isReverse; viewerManager.player.forceUpdateView() }
    } else {
        container.classList.add('colorPickerDiscret')
        for(let name of Object.keys(colors).sort()){
            const label = document.createElement('label')
            const spanColor = document.createElement('span')
            const inputColor = document.createElement('input')
            inputColor.type = 'color'
            const text = document.createTextNode(name)
            label.appendChild(spanColor)
            label.appendChild(inputColor)
            label.appendChild(text)
            spanColor.addEventListener('click',()=>inputColor.click())
            inputColor.addEventListener('change', () => { spanColor.style.backgroundColor = inputColor.value; colors[name] = inputColor.value; viewerManager.player.forceUpdateView()})
            container.appendChild(label)
            inputColor.value = colors[name]
            spanColor.style.backgroundColor = colors[name]
        }
    }
    return container
}

function runSimulation(builderToken, param, callBackToken, callBackDebug,callBackNotFound) {
    streamJson('/api/launch/' + builderToken, {
        method: 'POST',
        headers: {'Accept': 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify(param)
    },{
        callBackouput:(datas)=>{
            for (let data of datas) {
                if (data.hasOwnProperty("token")) {
                    createEventLeavePage(data.token)
                    callBackToken(data.token);
                } else if (data.hasOwnProperty("errLog")) {
                    callBackDebug(data.errLog, true);
                } else if (data.hasOwnProperty("outLog")) {
                    callBackDebug(data.outLog, false);
                } else if (data.hasOwnProperty("buildNotFound")) {
                    callBackNotFound()
                } else {
                    console.log("RECEIVED UNEXPECTED DATA : ", data);
                }
            }
        }
    }).catch((err) => console.log(err))
}


function updateColorItemBuilder(buildItem){
    if (!buildItem.color) buildItem.color = {};
    let availableInfo = buildItem.finalJson.CellAttributesRecorded.map(i => i.value).filter((v, i, a) => a.indexOf(v) === i)
    buildItem.finalJson.CellAttributesRecorded.forEach(i => {
        const isEnum = buildItem.finalJson.enums?buildItem.finalJson.enums.hasOwnProperty(i.type):false
        if (isEnum) {
            let defaultColorEnum = getDefaultColor(buildItem.finalJson.enums[i.type])
            if (buildItem.color[i.value])
                Object.keys(defaultColorEnum).forEach((v) => defaultColorEnum[v] = buildItem.color[i.value][v] ? buildItem.color[i.value][v] : defaultColorEnum[v])
            buildItem.color[i.value] = defaultColorEnum
        } else if (!buildItem.color[i.value]) buildItem.color[i.value] = getDefaultColor()
    })
    let colorType = getDefaultColor(buildItem.finalJson.cellTypes)
    if (buildItem.color['type']) {
        Object.keys(colorType).forEach((i) => colorType[i] = buildItem.color['type'][i] ? buildItem.color['type'][i] : colorType[i])    
    }
    buildItem.color['type'] = colorType
    let colorState = getDefaultColor(buildItem.finalJson.cell.States.map(i => i.name))
    if (buildItem.color['state']) {
        Object.keys(colorState).forEach((i) => colorState[i] = buildItem.color['state'][i] ? buildItem.color['state'][i] : colorState[i])    
    }
    buildItem.color['state'] = colorState
    return ['state', 'type', ...availableInfo]
}

/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class ViewerManager {

    // Singleton initialisation
    constructor() {  }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer, defaultData) {

        //content
        const container = document.createElement('div')
        container.id = 'contentViewer'
        container.style.width = '100%'
        container.style.height = '100%'
        document.getElementById('contentView').appendChild(container)
        this.playerWidgetParentDom = document.createElement('div')
        container.appendChild(this.playerWidgetParentDom)

        PanelManager.createSplitterContainer('contentViewer', 'graphSceneViewerSplit', true);
        this.graphScenePanelManager = new PanelManager("graphSceneViewerSplit", true);
        const plotDom = this.graphScenePanelManager.createPanel("graph");
        this.graphScenePanelManager.createPanel("sceneDebugerSplit");
        this.graphScenePanelManager.setWidthPanel([350, 100], [true, false])
        this.graphScenePanelManager.minimize(['graph'],1,true)

        PanelManager.createSplitterContainer('sceneDebugerSplit', 'sceneDebugViewer', false);
        this.sceneDebugPanelManager = new PanelManager("sceneDebugViewer", false);
        this.sceneDebugPanelManager.createPanel("sceneViewer");
        document.getElementById('sceneViewer').style.height = '100%'
        const debugDom = this.sceneDebugPanelManager.createPanel("debugViewer");
        this.sceneDebugPanelManager.setWidthPanel([100, 200], [false, true])
        this.sceneDebugPanelManager.minimize(['debugViewer'], -1)

        this.debugPanel = new DebugPanel(debugDom);
        this.plotPanel = new Plots(plotDom);

        // side Panel

        const sidePanel = document.createElement('div')
        sidePanel.id = 'viewerActivity'
        mainContainer.appendChild(sidePanel)
        this.options = {
            info:'type',
            targetFPS:10
        }
        this.gui = new dat.GUI({ width: '100%' , autoPlace: false });
        sidePanel.appendChild(this.gui.domElement)
        this.gui.domElement.style.width = null

        this.colorPicker = document.createElement('div')
        sidePanel.appendChild(this.colorPicker)
    }


    run(paramItem,is2D){
        this.reset()
        this.is2D = is2D
        this.currentParamItem = paramItem;
        const buildItem = paramItem.parent;
        //console.log({ finalJson: buildItem.finalJson, param: paramItem.param});
        let availableInfo = updateColorItemBuilder(buildItem)

        
        const rs = (token)=> runSimulation(token, Parameters.getParamFromItemParam(paramItem), (token) => {
            this.tokenRun = token
            this.currentParamItem.addItem(Parameters.createViewerItem(token,is2D))

            this.player = new WidgetPlayer(this.playerWidgetParentDom, this.currentParamItem.param.Scenario.dt, this.currentParamItem.param.Scenario.maxStep);
            this.player.addEvent('plots', (e) => this.plots.update())
            this.buffer = new BufferSimulation(this.tokenRun);
            this.buffer.addEvent('updateBufferViewer', () => this.player.updateBufferView())
            if (this.is2D) this.viewer = new Viewer2D(document.getElementById('sceneViewer'));
            else this.viewer = new Viewer3D(document.getElementById('sceneViewer'));

            const f2 = this.datGUI.addFolder('Update');
            f2.add(this.options, 'targetFPS').min(1).max(60).step(1);
            f2.add(this.options, 'info', availableInfo).onChange(()=>{
            this.colorPicker.innerHTML = ''
            this.colorPicker.appendChild(generateDomColorPicker(this.currentParamItem.parent.color[this.options.info]))
                this.player.forceUpdateView()
            })
            f2.open();
            this.plots.generateHeader()
            console.log(buildItem.plots)
            if(buildItem.plots)
                buildItem.plots.forEach((data)=>this.plots.addPlot(data))
            this.colorPicker.appendChild(generateDomColorPicker(this.currentParamItem.parent.color[this.options.info]))
        }, (msg, err) => {
            this.debugPanel.write(msg, err)
        }, () => {
            BuilderManager.build('sql',buildItem.token,buildItem.compiledBy, buildItem.finalJson, (err, token) => {
                buildItem.token = token
                buildItem.failedCompile = err
                if(!err) rs(token)
                else ActivityManager.toggleActivity('parambtn')
            })
        });
        rs(buildItem.token)
    }

    view(viewItem) {
        this.reset()
        this.is2D = viewItem.is2D
        const paramItem = this.currentParamItem = viewItem.parent; 
        const token = viewItem.token
        
        const buildItem = paramItem.parent;
        let availableInfo = updateColorItemBuilder(buildItem)
            this.tokenRun = token

            this.player = new WidgetPlayer(this.playerWidgetParentDom, this.currentParamItem.param.Scenario.dt, this.currentParamItem.param.Scenario.maxStep);
            this.player.addEvent('plots', (e) => this.plots.update())
            this.buffer = new BufferSimulation(this.tokenRun);
            this.buffer.addEvent('updateBufferViewer', () => this.player.updateBufferView())
            if (this.is2D) this.viewer = new Viewer2D(document.getElementById('sceneViewer'));
            else this.viewer = new Viewer3D(document.getElementById('sceneViewer'));

            const f2 = this.datGUI.addFolder('Update');
            f2.add(this.options, 'targetFPS').min(1).max(60).step(1);
            f2.add(this.options, 'info', availableInfo).onChange(() => {
                this.colorPicker.innerHTML = ''
                this.colorPicker.appendChild(generateDomColorPicker(this.currentParamItem.parent.color[this.options.info]))
                this.player.forceUpdateView()
            })
            f2.open();
            this.plots.generateHeader()
            this.colorPicker.appendChild(generateDomColorPicker(this.currentParamItem.parent.color[this.options.info]))
    }


    reset() {
        if (this.player) this.player.isPlaying = false
        this.debugPanel.reset()
        this.plotPanel.reset()
        if (this.buffer)this.buffer.flush()
        if (this.viewer && !this.is2D) this.viewer.renderer.setAnimationLoop(null)
        delete this.player
        delete this.viewer
        delete this.buffer
        this.gui.destroy()
        document.getElementById('sceneViewer').innerHTML = ''
        this.colorPicker.innerHTML=''
        this.playerWidgetParentDom.innerHTML = ""
        this.options.info = 'type'
    }

    colorize(values) {
        const c = this.colors[this.options.info]
        if (c.palette) return getColorGradient(gradientPalette[c.palette], c.min, c.max, c.reverse, values)
        else return getColorDiscret(Object.keys(c).sort().map(n => c[n]), values) //Object.values(c), values)
    }


    get datGUI(){
        return this.gui;
    }

    get token() {
        return this.tokenRun
    }
    get colors() {
        return this.currentParamItem.parent.color
    }
    get enums() {
        return this.currentParamItem.parent.enums
    }
    get plots() {
        return this.plotPanel
    }
    updateSavedPlots() {
        this.currentParamItem.parent.plots = this.plotPanel.plots.map((p)=>{
            let dataDC = JSON.parse(JSON.stringify(p.data))
            dataDC.forEach(e => {
                delete e.x;
                delete e.y;
            });
            return { data: dataDC,layout:p.layout}
        })
    }

    get continousInfo(){
        return [...this.buffer.coordColumn[this.is2D], ...this.currentParamItem.parent.finalJson.CellAttributesRecorded.map(i => i.value).filter(i => i !== 'type' && i !== 'state')]
    }

    toggleDebug(){
        if (this.debugPanel.displayed) this.sceneDebugPanelManager.minimize(['debugViewer'], -1)
        else this.sceneDebugPanelManager.unminimize(['debugViewer'])
        this.debugPanel.displayed = !this.debugPanel.displayed
        window.dispatchEvent(new Event('resize'));
    }
    togglePlot() {
        if (this.plotPanel.displayed) this.graphScenePanelManager.minimize(['graph'])
        else this.graphScenePanelManager.unminimize(['graph'])
        this.plotPanel.displayed = !this.plotPanel.displayed
        window.dispatchEvent(new Event('resize'));
    }

    toggleRecordVideo() {
        if (!this.viewer.capturer) {
            this.viewer.capturer = new WebMWriter({
                quality: 0.80,    // WebM image quality from 0.0 (worst) to 0.99999 (best), 1.00 (VP8L lossless) is not supported
                fileWriter: null, // FileWriter in order to stream to a file instead of buffering to memory (optional)
                fd: null,         // Node.js file handle to write to instead of buffering to memory (optional)

                // You must supply one of:
                frameDuration: null, // Duration of frames in milliseconds
                frameRate: 30,     // Number of frames per second

                transparent: false,      // True if an alpha channel should be included in the video
                alphaQuality: undefined, // Allows you to set the quality level of the alpha channel separately.
                // If not specified this defaults to the same value as `quality`.
            });
            this.player.btn.record.classList.add("viewerBtnRecord-on")
        } else {
            this.viewer.capturer.complete().then(function(webMBlob) {
                downloadFile(URL.createObjectURL(webMBlob),'video.webm')
                this.viewer.capturer = null;
            });
            this.player.btn.record.classList.remove("viewerBtnRecord-on")
        }
    }
}

const viewerManager = new ViewerManager();
export default viewerManager;
