import * as THREE from 'three';
//import { GUI } from 'three/examples/jsm/libs/dat.gui.module.js';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { SSAOPass } from 'three/examples/jsm/postprocessing/SSAOPass.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import CellRenderer from './CellRenderer3D.js';
import ViewerManager from '../ViewerManager.js';

export default class Viewer3D {

  constructor(container) {
    this.container = container;
    this.cellRenderer;
    this.options = {
      SSAO:!window.mobileAndTabletCheck(),
      cut:{
        enable:false,
        x:40,
        y:40,
        z:40
    }};
    this.valueCutChanged = false;
    this.lastColorMode = undefined;
    this.idleTime = 0;
    this.idle = false;
    this.paused = false;
    this.capturer = null;
    this.idleInterval = setInterval(()=>{
      if(this.idle){
        this.paused = (this.paused || (this.idleTime++ > 6));
      }
      else{
        this.idleTime = 0;
        this.paused = false;
      }
    }, 10000); // 1 minute
    window.addEventListener('blur', ()=>{this.idle = true;})
    window.addEventListener('focus', ()=>{this.idle = false;this.idleTime = 0;})

    window.addEventListener('resize', () => {
      this.onWindowResize()
    });

    this.scene = new THREE.Scene();
    this.addBackground();
    this.createCamera();
    this.createControls();
    this.createRenderer();
    //this.private.renderer.vr.enabled = true;
    //this.container.appendChild(WEBVR.createButton(this.private.renderer));

    this.setupWorld();
    this.cellRenderer = new CellRenderer(this.world,12);

    this.renderer.setAnimationLoop(() => {
      if (!this.paused && document.getElementById('contentViewer').style.display!=='none'){
        this.update();
        this.render();
        if(this.capturer) this.capturer.addFrame(this.renderer.domElement);
      }
    });


    // Choose from named values
    const f1 = ViewerManager.datGUI.addFolder('Cut');
    f1.add(this.options.cut, 'enable').onChange(()=>{this.valueCutChanged = true;})
    f1.add(this.options.cut, 'x').min(-40).max(40).step(1).onChange(() => {
      this.valueCutChanged = true;
    });
    f1.add(this.options.cut, 'y').min(-40).max(40).step(1).onChange(() => {
      this.valueCutChanged = true;
    });
    f1.add(this.options.cut, 'z').min(-40).max(40).step(1).onChange(() => {
      this.valueCutChanged = true;
    });
    // f1.open();

    f1.add(this.options, 'SSAO');
    this.addSAO();


  }


  setupWorld(){
    this.world = new THREE.Object3D();
    this.world.position.y =this.world.position.x = this.world.position.z = 0;
    this.world.scale.y =this.world.scale.x = this.world.scale.z = 0.25;
    this.scene.add(this.world);
  }

  updateView(data){
    if(!this.paused){
      try{
        this.cellRenderer.updateCells(data);
        this.updateCutGui(data.x, data.y, data.z);
        this.cellRenderer.applyCut(this.options.cut);
        this.valueCutChanged = false;
      }catch(e){
        console.error(e);
      }
      
    }
  }
  update(){
    if (this.valueCutChanged) {
      this.cellRenderer.applyCut(this.options.cut);
    }
  }

  render(){
    if(!this.options.SSAO){
      this.renderer.render(this.scene, this.camera);
    } else {
      this.composer.render();
    }
  }

  onWindowResize() {
    //if (!this.private.renderer.vr.isPresenting()) {
      this.camera.aspect = this.container.clientWidth / (this.container.offsetHeight-0);
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(this.container.clientWidth, this.container.offsetHeight-0);
			this.composer.setSize(this.container.clientWidth, this.container.offsetHeight-0);
    //}
  }

  updateCutGui(allX, allY, allZ){
    var newCut = (cut,controller,newCoords) => {
      try{
        var pc;
        if (cut > 0) {
          pc = cut / ViewerManager.datGUI.__folders.Cut.__controllers[controller].__max;
          ViewerManager.datGUI.__folders.Cut.__controllers[controller].max(Math.max(...newCoords)+1);
          cut = pc * ViewerManager.datGUI.__folders.Cut.__controllers[controller].__max;
        }
        else if(cut < 0){
          pc = cut / ViewerManager.datGUI.__folders.Cut.__controllers[controller].__min;
          ViewerManager.datGUI.__folders.Cut.__controllers[controller].min(Math.min(...newCoords)-1);
          cut = pc * ViewerManager.datGUI.__folders.Cut.__controllers[controller].__min;
        }
      }catch(e){
        console.error(e);
      }
    }

    // controller 0 is the enable boolean
    newCut(this.options.cut.x, 1, allX);
    newCut(this.options.cut.y, 2, allY);
    newCut(this.options.cut.z, 3, allZ);

    ViewerManager.datGUI.updateDisplay();
  }

  addBackground(){
    const loader = new THREE.CubeTextureLoader();
    const texture = loader.load( [
      './textures/pos-x.png',
      './textures/neg-x.png',
      './textures/pos-y.png',
      './textures/neg-y.png',
      './textures/pos-z.png',
      './textures/neg-z.png'
    ] );
    this.scene.background = texture;
  }

  createCamera(){
    this.camera = new THREE.PerspectiveCamera(50, this.container.clientWidth / (this.container.offsetHeight-0), 1, 10000);
    this.camera.position.set(0, 0, 350);
  }

  createControls(){
    this.controls = new OrbitControls(this.camera, this.container);
    this.controls.enablePan = true;
    this.controls.screenSpacePanning = true;
    this.controls.panSpeed = 1;
    this.controls.minDistance = 25;
    this.controls.maxDistance = 700;
    this.controls.update();
  }


/*
  createLights(){
    let hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.5 );
		//hemiLight.color.setHSL( 0.3, 1., 0.6 );
		hemiLight.position.set( 10, 50, 10 );
		this.scene.add( hemiLight );

    let dirLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
    this.mainLight = dirLight;
    dirLight.color.setHSL( 0.1, 1, 0.95 );
    dirLight.position.set( - 1, 1.75, 1 );
    dirLight.position.multiplyScalar( 30 );
    this.scene.add( dirLight );

    dirLight.castShadow = true;

    dirLight.shadow.mapSize.width = 2048;
    dirLight.shadow.mapSize.height = 2048;

    let d = 50;

    dirLight.shadow.camera.left = - d;
    dirLight.shadow.camera.right = d;
    dirLight.shadow.camera.top = d;
    dirLight.shadow.camera.bottom = - d;

    dirLight.shadow.camera.far = 350;
    dirLight.shadow.bias = - 0.0001;

  }*/


  addSAO(){
    this.composer = new EffectComposer( this.renderer );
    let renderPass = new RenderPass( this.scene, this.camera );
    this.composer.addPass( renderPass );

    let ssaoPass = new SSAOPass( this.scene, this.camera, this.container.clientWidth, this.container.offsetHeight-0 );
    ssaoPass.kernelRadius = 12;
    ssaoPass.minDistance = 0.0026;
    ssaoPass.maxDistance = 0.2

    //ViewerManager.datGUI.add( ssaoPass, 'kernelRadius' ).min( 0 ).max( 32 );
    //ViewerManager.datGUI.add( ssaoPass, 'minDistance' ).min( 0.001 ).max( 0.02 );
    //ViewerManager.datGUI.add( ssaoPass, 'maxDistance' ).min( 0.01 ).max( 0.3 );
		this.composer.addPass( ssaoPass );

  }

  createRenderer(){
    this.renderer = new THREE.WebGLRenderer({
      antialias: !window.mobileAndTabletCheck()
    });
    this.renderer.setSize(this.container.clientWidth, this.container.offsetHeight-0);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.container.appendChild(this.renderer.domElement);

  }

}
