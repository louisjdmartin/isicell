import ViewerManager from '../ViewerManager.js';
import * as PIXI from 'pixi.js';

function coordSplit(x,y,w,h,nb){
    let res = []
    if(nb == 1) return [{x,y,w,h}]
    let nb2 = Math.floor(nb/2)
    let nb1 = nb2+nb%2
    if(w>h){
        res.push(coordSplit(x,y,w/2,h,nb1))
        if(nb2>0) res.push(coordSplit(x+w/2,y,w/2,h,nb2))
    } else {
        res.push(coordSplit(x,y,w,h/2,nb1))
        if(nb2>0) res.push(coordSplit(x,y+h/2,w,h/2,nb2))
    }
    return res.flat(1)
}

function convertDataToGrid(data){
    let dataG = {}
    let key;
    let d;
    let c = ViewerManager.colorize(data.info)
    for (let i = 0; i < data.x.length; i++) {
        key = data.x[i]+','+data.y[i]
        d = { x: data.x[i], y: data.y[i], c: convertRGBToInt(c[i * 3], c[i * 3 + 1], c[i * 3 + 2]) }
        if(!dataG[key]) dataG[key] = [d];
         else dataG[key].push(d);
    }
    return Object.values(dataG);
}

function convertRGBToInt(r,g,b){
    return (Math.floor(255*r) << 16) + (Math.floor(255*g) << 8) + Math.floor(255*b)
}
export default class CellRenderer {
    constructor(stage, colorMode,colorModes){
        this.stage = stage;
        this.sprites  = new PIXI.ParticleContainer(200000, {
              scale: true,
              position: true,
              rotation: true,
              uvs: true,
              alpha: true,
          });
        this.stage.addChild(this.sprites);
        this.cells = []
        this.computedCoord = {}
        this.data;
        this.colorModes = colorModes;
        this.colorMode = colorMode;
        }

    updateCells = (data) =>{
      this.data = convertDataToGrid(data);
      this.refresh();
    }

    refresh = () => {
        let coord;
        let i=0;
        for (let cellG of this.data) {
            // create a new Sprite
            if(!this.computedCoord.hasOwnProperty(cellG.length)) this.computedCoord[cellG.length] = coordSplit(0,0,1,1,cellG.length)
            coord = this.computedCoord[cellG.length]


            for(let j=0; j<cellG.length;j++){
                if(i<this.cells.length){
                    this.cells[i].scale.set(coord[j].w,coord[j].h);
                    this.cells[i].x = (cellG[j].x+coord[j].x) *48;
                    this.cells[i].y = (cellG[j].y+coord[j].y) *48;
                    this.cells[i].tint = cellG[j].c;
                    if(i>=this.sprites.children.length) this.sprites.addChild(this.cells[i]);
                } else {
                    const cell = PIXI.Sprite.from("textures/cell.png");
                    cell.anchor.set(0);

                    cell.scale.set(coord[j].w,coord[j].h);
                    cell.x = (cellG[j].x+coord[j].x) *48;
                    cell.y = (cellG[j].y+coord[j].y) *48;

                    cell.tint = cellG[j].c;
                    this.cells.push(cell);
                    this.sprites.addChild(cell);
                }
                i++;
            }
        }
        if(i<this.sprites.children.length){
        this.sprites.removeChildren(i,this.sprites.children.length)
        }
    }
}