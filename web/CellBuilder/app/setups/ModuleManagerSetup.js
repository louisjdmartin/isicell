import { longToast } from '../guiComponent/notification';
import CustomModulesManager from '../manager/customModulesManager';
import FunctionManager from '../manager/functionManager'
import ModuleManager from '../manager/moduleManager'
import { regexVariableDeclarationSplit, utf8_to_b64, firstLetterToLower } from '../utils/misc';
import { getTitleCoreFonction } from '../utils/utilsVaraibleName';

function getDocFromFile(fileName,str) {
  const isHook = f=>['beginUpdate','preBehaviorUpdate','onAddCell','preDeleteDeadCellsUpdate','postBehaviorUpdate','endUpdate'].reduce((a,v)=>a||f.includes(v),false)

  let iterator = new RegExp(/[^;\}]*?(\{|\/\*|private[^\S\r\n]*:|public[^\S\r\n]*:|protected[^\S\r\n]*:|\}|\*\/|\/\/.*)/, "g"),
  bracketCount, match, capture, comment, tree,currentNamespace;
  let current = { child: [] }
  tree = current
  bracketCount = 0;
  comment = {isOpen:false}
  try{
    while (match = iterator.exec(str)) {
      const token = match[1]
        if(token.startsWith('//')){

        } else if (token === '*/' && comment.isOpen) {
            comment.isOpen = false
            comment.content = str.slice(comment.matchStartIndex, iterator.lastIndex-2)
        } else if(comment.isOpen){
        
        }else if(token === '/*'){
          comment = {isOpen:true, matchStartIndex: iterator.lastIndex, parent:current};
        }  else if (current.type === 'class' && /^private|^protected/.test(token)) {
          current.isPublic = false
        } else if (current.type === 'class' && /^public/.test(token)) {
          current.isPublic = true
        } else if (token==='}') {
          comment = {isOpen:false}
          bracketCount--;
          if (bracketCount === current?.bracketNum) {
            current.matchEndIndex = match.index
            current.content = str.slice(current.matchStartIndex, match.index);
            const parent = current.parent
            delete current.parent
            parent.child.push(current)
            current = parent
          }
        } else if (token==='{') {
          if(current.type !== 'function'){
            //capture = match[0].match(/(?<type1>namespace)\s+(?<name1>\w+)\s*\{|(?<type2>class|struct)\s+(?<name2>\w+).*\n*.*?\{/)
            capture = match[0].match(/(?<type2>class|struct)\s+(?<name2>\w+).*\n*.*?\{/)
            if (capture) {
              const g = capture.groups
              const type = g.type1 || g.type2
              const name = g.name1 || g.name2
              if(type === 'namespace') currentNamespace = name
              else if (/^Body|^Plugin/i.test(name)) {
              //if (/^Body|^Plugin/i.test(name) || type === 'namespace') {
                current = { type: type, name: name, namespace:currentNamespace, matchStartIndex: iterator.lastIndex, bracketNum: bracketCount, parent: current,isPublic:(type==='struct'), child: [] }
              }
            } else if ((current.type === 'class' && current.isPublic ) || current.type === 'struct') {
              capture = match[0].match(/(?<proto>.*\([\s\S]*?\))[\s\S]*?\{/)
              if (capture) {
                const g = capture.groups
                const prototype = g.proto.replaceAll(/\s*inline\s/g, '')
                const description = (comment.parent===current?comment.content?.replaceAll(/[^\S\r\n]*[*!][^\S\r\n]*|@brief|\\brief/g, ''):'')//|@param.*|@return.*|\s{2}
                comment = {isOpen:false}
                if (!prototype.includes(current.name)) {
                  current = { type: 'function', prototype, description, isHook: isHook(prototype) ,matchStartIndex: iterator.lastIndex, bracketNum: bracketCount, parent: current, child: [] }
                }
              }/* else {
                console.log(match[0])
              }*/
            }
          }
          bracketCount++;
        }
    }
  }catch(e){console.error(e)}
  if(tree.child.length!==1){
    longToast.fire({
        icon: 'error',
        title: 'error too many class detected in file '+fileName
    })
    console.error('error: too many class detected in file '+fileName)
  } else if(bracketCount>0){
    longToast.fire({
        icon: 'error',
        title: 'error: too many "{" in file '+fileName
    })
    console.error('too many {')
  } else if(bracketCount<0){
    longToast.fire({
        icon: 'error',
        title: 'error: too many "}" in file '+fileName
    })
    console.error('too many }')
  } else return tree.child[0];
}


function wrapperFunctionInfo(json, completion_prefix) {
  return json.map((i) => {
    let info = FunctionManager.extractHeaderCppFunction(i.code ? i.code : i["prototype"])
    if (info) return {header:{ type: info.returnType, valueTextNoIntegrityCheck: info.functionName + '()', createdByModule: true, title: getTitleCoreFonction(Object.assign({ completion_prefix: completion_prefix, description: i.description }, info))},code:i.code }
  })
}
export default function setupModuleManager(){
  ModuleManager.addUpdater((modules)=>{
    let doDelete = item => {if(item.createdByModule) item.delete()};
    let catsModule = ["Default functions cell", "Default functions scenario", "Bodies Methods", "Plugins Methods", "Scenario Attributes"]
    
    catsModule.forEach(id => document.getElementById(id).items.forEach(doDelete))
    catsModule.forEach(id => document.getElementById(id)._disabledSorting = true)



    for(let mod of modules){

      let sec = document.getElementById("Default functions cell")
      
      if(mod.hasOwnProperty('cellFunctionTemplate'))
      wrapperFunctionInfo(mod['cellFunctionTemplate']).forEach(info => {if (info) {
        const item = sec.itemBuilder()
        item.dataset.template = utf8_to_b64(info.code)
        sec.addItem(item.applyOptions(info.header))
      }})

      sec = document.getElementById("Default functions scenario")
      wrapperFunctionInfo(mod['scenarioFunction']).forEach(info => {if (info) {
        const item = sec.itemBuilder()
        item.dataset.template = utf8_to_b64(info.code)
        sec.addItem(item.applyOptions(info.header))
      }})

      if ('scenarioAttributes' in mod) {
        let sec = document.getElementById("Scenario Attributes")
        for (let info of mod['scenarioAttributes'].map((v) => v.match(regexVariableDeclarationSplit).groups)){
            const item = sec.itemBuilder()
            item.type = info.type
            item.valueTextNoIntegrityCheck = info.value
            item.createdByModule = true
            sec.addItem(item)
        }
      }

      sec = document.getElementById("Bodies Methods")
      wrapperFunctionInfo(mod['Body']['functions'],'this->getBody().').forEach(info => { if(info){
        const item = sec.itemBuilder()
        sec.addItem(item.applyOptions(info.header))
      }})
      if(mod.hasOwnProperty('cellFunctions')) {
        wrapperFunctionInfo(mod['cellFunctions'],'this->').forEach(info => { if(info){
          const item = sec.itemBuilder()
          info.header['completion_prefix'] = 'this->'
          sec.addItem(item.applyOptions(info.header))
        }})
      }

      sec = document.getElementById("Plugins Methods")
      wrapperFunctionInfo(mod['Plugin']['functions'], 'w.cellPlugin.'+firstLetterToLower(mod.Plugin.className) + '.').forEach(info => { if(info){
        const item = sec.itemBuilder()
        item.completion_prefix += firstLetterToLower(mod.Plugin.className) +'.'
        sec.addItem(item.applyOptions(info.header))
      }})

    }
  catsModule.forEach(id => {let sec = document.getElementById(id); sec._disabledSorting = false; sec.sort()})
  })
  ModuleManager.updateSelectedModules() // Now update to setup everything




  CustomModulesManager.addUpdater((modules)=>{
    let doDelete = item => {if(item.createdByCustomModule) item.delete()};
    let catsModule = ["Default functions cell", "Default functions scenario", "Bodies Methods", "Plugins Methods", "Scenario Attributes"]
    
    catsModule.forEach(id => document.getElementById(id).items.forEach(doDelete))
    catsModule.forEach(id => document.getElementById(id)._disabledSorting = true)


    function wrapperFunctionInfo(json, completion_prefix) {
      return json.map((i) => {
        let info = FunctionManager.extractHeaderCppFunction(i.code ? i.code : i["prototype"])
        if (info) return {header:{ type: info.returnType, valueTextNoIntegrityCheck: info.functionName + '()', createdByCustomModule: true, title: getTitleCoreFonction(Object.assign({ completion_prefix: completion_prefix, description: i.description }, info))},code:i.code }
      })
    }
    let sec,classDoc;
    try{
      for(const [nameModule,files] of Object.entries(modules)){
        for(const [nameFile,file] of Object.entries(files)){
          if(file.type === 'Body'){
            classDoc = getDocFromFile(nameFile,file.code)
            if(classDoc){
              sec = document.getElementById("Bodies Methods")
              wrapperFunctionInfo(classDoc.child,'this->getBody().').forEach(info => { if(info){
                const item = sec.itemBuilder()
                sec.addItem(item.applyOptions(info.header))
              }})
            }
          } else if(file.type ==='Plugin'){
            classDoc = getDocFromFile(nameFile,file.code)
            if(classDoc){
              sec = document.getElementById("Plugins Methods")
              const varNameModule = firstLetterToLower(nameFile).split('.')[0] + '.'
              wrapperFunctionInfo(classDoc.child.filter((f)=>!f.isHook), 'w.cellPlugin.'+varNameModule).forEach(info => { if(info){
                const item = sec.itemBuilder()
                item.completion_prefix += varNameModule
                sec.addItem(item.applyOptions(info.header))
              }})
            }
          }
        }
      }
    } catch(e){console.error(e)}
  catsModule.forEach(id => {let sec = document.getElementById(id); sec._disabledSorting = false; sec.sort()})
  })
}