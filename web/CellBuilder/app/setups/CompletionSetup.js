import stateMachine from "../diagrams/stateMachine"
import CodeEditorProvider from "../guiComponent/codeEditorProvider"
import CppTypeManager from "../manager/CppTypeManager"
import FunctionManager from "../manager/functionManager"

export default function setupCompletion(defaultData){
	//! Adds the cell states completion provider
	CodeEditorProvider.addCompletionProvider('state',()=>{
		return stateMachine.getStates().map((state)=>{
			return {
				label:'State::'+state.name,
				insertText: 'State::'+state.name,
				documentation: 'State'}
		})
	})
	CodeEditorProvider.addCompletionProvider('enums', () => {
		const enums = CppTypeManager.getEnums();
		return Object.keys(enums).map((enumName) => enums[enumName].map(value=>{
			return {
				label: enumName +'::' + value,
				insertText: enumName +'::' + value,
				documentation: 'Enum'
			}
		})).flat()
	})

	let staticMethods = defaultData.Default.staticMethods.map((i) => {
		const info = FunctionManager.extractHeaderCppFunction(i['prototype'])
		return {
			label: info.functionName + i['prototype'].split(info.functionName)[1],
			insertText: info.functionName + '()',
			documentation: i.description
		}
	})

	CodeEditorProvider.addCompletionProvider('staticMethods', () => {
		return staticMethods
	})
	
	//! Adds all the completion provider comming from the local list
	for (let sl of document.getElementById('localList').children){
		CodeEditorProvider.addCompletionProvider(sl.id,()=>{
			return sl.items.map((item)=>{
				return {
					label: (item.completion_prefix !== undefined ? item.completion_prefix : '') + item.valueText,
					insertText: (item.completion_prefix !== undefined ? item.completion_prefix : '') + item.valueText,
					documentation: sl.titleText
				}
			})
		})
	}
	//! Adds all the completion provider comming from the global list
	for (let sg of document.getElementById('globalList').children) {
		CodeEditorProvider.addCompletionProvider(sg.id, () => {
			return sg.items.map((item) => {
				return {
					label: (item.completion_prefix !== undefined ? item.completion_prefix : '') + item.valueText,
					insertText: (item.completion_prefix !== undefined ? item.completion_prefix : '') + item.valueText,
					documentation: sg.titleText
				}
			})
		})
	}
	// Sets the available completions for each editor
	var globalCell = ['Cell Types','Cell Attributes','Bodies Methods','state','Cell functions','enums','staticMethods']
	var localCell = ['Local Variables']
	CodeEditorProvider.setEditorCompletionProvider('conditionEditor',globalCell)
	CodeEditorProvider.setEditorCompletionProvider('conditionBehaviorEditor',globalCell)
	CodeEditorProvider.setEditorCompletionProvider('editorCall',[...globalCell,...localCell])
	CodeEditorProvider.setEditorCompletionProvider('editorFunction',globalCell)
}