import CppTypeManager from '../manager/CppTypeManager';
import FunctionManager from '../manager/functionManager'
import { regexVariableDeclarationSplit } from '../utils/misc';
import { getTitleCoreFonction, callBackCheckIntegrity } from '../utils/utilsVaraibleName';
import Parameters from '../manager/ParametersManager';

export default function setupGlobalList(defaultData){


  var globalList = document.getElementById('globalList')
  
  function createSection(id, title, sectionOptions = {}, ItemOptions = {}, CoreItems) {
    ItemOptions.taggable = true
    if (ItemOptions.type && (sectionOptions.addable || ItemOptions.editable)) ItemOptions['typeProvider'] = () =>CppTypeManager.getAllTypes()
    const itemBuilder = () => document.createElement('custom-item').applyOptions(ItemOptions)
    if (sectionOptions.addable) sectionOptions.addable = itemBuilder
    const section = document.createElement('custom-section').applyOptions(sectionOptions);
    section.itemBuilder = itemBuilder
    section._disabledSorting = true
    let ItemOptionsCore = { ...ItemOptions}
    delete ItemOptionsCore.deletable
    delete ItemOptionsCore.editable
    CoreItems.forEach(item => {
      const i = document.createElement('custom-item')
      i.valueText = item.value
      i.applyOptions(ItemOptionsCore)
      i.deletable = false
      if (item.type) i.type = item.type
      if (item.title) i.title = item.title
      if (item.completion_prefix) i.completion_prefix = item.completion_prefix
      i.addTag('CORE')
      section.addItem(i)
    });
    delete section._disabledSorting
    section.sort()
    section.titleText = title
    section.classList.add('title')
    section.id = id
    globalList.appendChild(section)
    section.updateSize()
    return section
  }


  var cbCell = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'cellFunction'); return newVal }
  var cbScenario = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'scenarioFunction'); return newVal }

  let sec;
  createSection("Cell Types", 'Cell Types', { addable: true, toggleable: true }, { editable: () => { Parameters.changeDetected() }, deletable: true, intergrityCheck:cbCell,completion_prefix:'Type::' }, [])
 
  createSection("Cell Attributes", 'Cell Attributes', { addable: true, toggleable: true }, { ioable: true, editable: () => { Parameters.changeDetected()}, deletable: true, type: 'int', intergrityCheck: cbCell, completion_prefix:'this->' },
    defaultData.Default.cellAttributes.map((v) => v.match(regexVariableDeclarationSplit).groups))
  sec = createSection("Scenario Attributes", 'Protocol Attributes', { addable: true, toggleable: true }, { noviewTag:true,ioable: true, editable: () => { Parameters.changeDetected() }, deletable: true, type: 'int', intergrityCheck: cbScenario, completion_prefix: 'this->' },
    defaultData.Default.scenarioAttributes.map((v) => v.match(regexVariableDeclarationSplit).groups))
  sec.style.display = 'none'
  
  function wrapperFunctionInfo(idx,completion_prefix){
    return defaultData.Default[idx].map((i) => { 
      let info = FunctionManager.extractHeaderCppFunction(i["prototype"])
      return { type: info.returnType, value: info.functionName + '()', completion_prefix: completion_prefix, title: getTitleCoreFonction(Object.assign({ completion_prefix: completion_prefix, description: i.description }, info))}})
  }
  createSection("Bodies Methods",'Module Cell Methods', { toggleable: true }, { completion_prefix: 'this->getBody().' }, [])
  sec = createSection("Plugins Methods",'Module Protocol Methods', { toggleable: true }, { completion_prefix:'w.cellPlugin.' }, [])
  sec.style.display = 'none'

  createSection("Cell Methods",'Cell Methods', { toggleable: true }, { completion_prefix: 'this->' }, [...wrapperFunctionInfo('cellMethods',''), ...wrapperFunctionInfo('modelMethods', 'model->')])
  sec = createSection("Scenario Methods",'Protocol Methods', { toggleable: true }, { completion_prefix: 'this->' }, [...wrapperFunctionInfo('scenarioMethods', ''), ...wrapperFunctionInfo('modelMethods', 'm.')])
  sec.style.display = 'none'

} 

