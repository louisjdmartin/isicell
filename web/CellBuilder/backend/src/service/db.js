// https://node-postgres.com/
const {Pool} = require('pg');
const format = require('pg-format');
  let client;

async function connectToDb(sqlinfo) {
  let retries=5;
  client = new Pool({
    user: sqlinfo.user,
    host: sqlinfo.host,
    database: sqlinfo.database,
    password: sqlinfo.password,
    port: sqlinfo.port,
  })

  while (retries) {
    try {
      await client.connect();
      break;
    } catch (err) {
      console.log(err);
      retries -= 1;
      console.log(`retries left: ${retries}`);
      // wait 5 seconds
      await new Promise(res => setTimeout(res, 5000));
    }
  }
}

async function getMaxStep(token){
  try {
    const { rows } = await client.query(format("SELECT MAX(step) FROM %I;", token +"_world"))
    return rows[0].max;
  } catch (error) {
    throw error
  }
} 

async function getStep(token, step, info) {
  try {
    let maxStep = await getMaxStep(token);
    if(step <= maxStep){
      const query = {
        text: 'SELECT x,y,z,radius,'+info+' FROM '+token+'_cells WHERE step = '+step,
        values: [],
        rowMode: 'array',
      }
      let {rows} =  await client.query(query);
      rows = rows[0].map((col, i) => rows.map(row => row[i]));
      rows = {maxStep,x:rows[0],y:rows[1],z:rows[2],radius:rows[3],color:rows[4]};
      return rows;
    } else {
      return {maxStep};
    }
  } catch (error) {
    throw error
  }
}

async function getMultiStep(token, infos, infosOnTime) {
  let dataRet = { data: {}, dataOnTime: {} }
  try {
    const QuerryPool = []
    let maxStep = await getMaxStep(token);
    if(maxStep){
      Object.keys(infos).filter(step => step > maxStep).forEach(stepToDelete => delete infos[stepToDelete]);
      const commonData = {}
      Object.entries(infos).forEach(([s, a]) => {
        const k = a.join(',')
        if (!commonData[k]) commonData[k] = [a,[]]
        commonData[k][1].push(parseInt(s))
      })

      for (const [allDataNeed,steps] of Object.values(commonData)) {
        if (allDataNeed.length>0){
          const query = {
            //text: 'SELECT step,' + allDataNeed.join(',') + ' FROM ' + token + '_cells WHERE step IN (' + steps.join(',') + ') ORDER BY cell_id',
            text: format('SELECT step,%I FROM %I WHERE step IN (%L) ORDER BY cell_id', allDataNeed.map(i=>i.toLowerCase()), token + '_cells', steps),
            rowMode: 'array'
          }
          QuerryPool.push(client.query(query).then(({ rows })=>{
            if (rows.length > 0) {
              steps.forEach(s => {
                if (!dataRet.data[s]) dataRet.data[s] = {}
                const rowsOneStep = rows.filter(row => row[0] === s)
                allDataNeed.forEach((info, i) => dataRet.data[s][info] = rowsOneStep.map(row=>row[i+1]))
              })
            }
          }))
        }
      }
      let colsDiscret = infosOnTime.discret.join(',')
      if (infosOnTime.currentMaxStep < maxStep) {
        let cols = [...infosOnTime.exist.map(i => "sum(" + i.toLowerCase() + ")"), "count(*)"].join(',')
                
        const query = {
          text: format('SELECT step,' + colsDiscret+','+cols+' FROM %I WHERE step > %L AND step <= '+maxStep+' GROUP BY step,'+colsDiscret+' ORDER BY step, '+colsDiscret, token + '_cells', infosOnTime.currentMaxStep),
          rowMode: 'array'
        }
        const columns = ['step', ...infosOnTime.discret, ...infosOnTime.exist,'count']
        QuerryPool.push(client.query(query).then(({ rows }) => {
          if(rows.length>0) rows[0].map((col, i) => rows.map(row => row[i])).forEach((row, i) => dataRet.dataOnTime[columns[i]] = columns[i]==='count'?row.map(j=>parseInt(j)):row)
        }))
      }
      if (infosOnTime.new.length > 0) {
        const addStep = (infosOnTime.currentMaxStep < maxStep || infosOnTime.exist.length ===0)?'step,':''
        cols = infosOnTime.new.map(i => "sum(" + i.toLowerCase() + ")").join(',')
        const query = {
          text: format('SELECT '+addStep+cols+' FROM %I WHERE step <= ' + maxStep +' GROUP BY step,'+colsDiscret+' ORDER BY step, '+colsDiscret, token + '_cells'),
          rowMode: 'array'
        }
        QuerryPool.push(client.query(query).then(({ rows }) => {
          if (rows.length > 0) rows[0].map((col, i) => rows.map(row => row[i])).forEach((row, i) => dataRet.dataOnTime[infosOnTime.new[i]] = row)
        }))
      }
      await Promise.all(QuerryPool)
    }
    dataRet.maxStep = maxStep
    return dataRet
    //console.log(dataRet)
    
  } catch (error) {
    console.error(error)
    throw error
  }
}

async function deleteSimu(token){
  const query = {
    text: 'DROP TABLE IF EXISTS ' + token + '_cells, ' + token + '_world CASCADE',
    values: [],
    rowMode: 'array',
  }
  await client.query(query);

}

async function getListTokenTable(){
  const query = {
    text: "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE';",
    rowMode: 'array',
  }
  return (await client.query(query)).rows.map(r=>r[0].substring(0,r[0].lastIndexOf("_"))).filter((t,i,a)=>a.indexOf(t)!==i);
}

module.exports = {
  connectToDb,
  getStep, 
  getMultiStep,
  getMaxStep,
  deleteSimu,
  getListTokenTable
};
