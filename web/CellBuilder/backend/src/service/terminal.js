
const pty = require("node-pty");


module.exports = function TerminalAPI(io, lr, users) {
    io.use((socket, next) => {
        const session = socket.request.session;
        if (session.loggedin) {
            next();
        } else {
            console.log('unauthorized socket connection')
            next(new Error("unauthorized"));
        }
    });
    io.on('error', (error) => {
        console.error('Socket.io Error:', error);
        // Vous pouvez gérer l'erreur ici, par exemple, envoyer une réponse d'erreur au client socket.
      });
    io.on('connection', (socket) => {

        lr.record(socket.request, "terminal")
        const session = socket.request.session;
        console.log('new Terminal session for ', session.username)
        const pty = new PTY(socket, { uid: users.getUID(session.username), gid: users.isiCellGID, env: users.getEnv(session.username) });

        socket.on('disconnect', () => {
            pty.kill()
            console.log('end terminal for', session.username);
        });
        
    });
}

class PTY {
    constructor(socket, userOpt) {
        // Setting default terminals based on user os
        this.socket = socket;

        this.ptyProcess = pty.spawn("zsh", [], Object.assign({
            name: "xterm-color",
            cwd: userOpt.env.HOME,
        }, userOpt));

        this.ptyProcess.on("data", (data) => {
            this.socket.emit("output", data);
        });
        this.ptyProcess.onExit(() => {
            this.socket.disconnect(true)
            this.kill()
        });
        socket.on("input", (data) => {
            this.ptyProcess.write(data);
        });
        socket.on("resize", (input) => {
            console.log('resize',input)
            this.ptyProcess.resize(input.cols, input.rows);
        });
    }


    kill() {
        this.ptyProcess.kill()
        delete this;
    }
}