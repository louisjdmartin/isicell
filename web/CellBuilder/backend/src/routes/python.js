const path = require('path');
const fs = require('fs');
const multer = require('multer');
const si = require('systeminformation');

const { spawn, exec } = require('child_process');
const { userFolder, simulationsPath, scriptsPath } = require("../service/isiCellPath");
const zipDir = require('zip-dir');

function generateToken() {
    let dt = new Date();
    return 'sim_'.concat(dt.getFullYear(), (dt.getMonth() + 1), dt.getDate(), "_", dt.getHours(), dt.getMinutes(), '_', Math.floor(Math.random() * 1000));
}

function splitPath(path) {
    let i = path.lastIndexOf('/') + 1
    return [path.slice(0, i), path.slice(i)]
}

function isSubDir (parent, dir) {
    const relative = path.relative(parent, dir);
    return relative && !relative.startsWith('..') && !path.isAbsolute(relative);
   }

module.exports = function(app,lr,users){
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            let ownFolder = path.join(userFolder, req.session.username)
            let destPath = path.join(ownFolder,req.body.path)
            if(!isSubDir(ownFolder,destPath))destPath = ownFolder
            cb(null, destPath)
        },
        filename: function (req, file, cb) {
            console.log(req.session.username, 'upload',file.originalname)
            cb(null, file.originalname)
        }
    })
    var upload = multer({ storage: storage,limits:{fieldSize: 2_000_000_000} })

    app.use('/api/uploadfiles', (req, res, next) => {
        if (!req.session.loggedin) {
            res.status(401).send({ message: "unauthorized action, please login" });
            return;
        }
        next();
    })
    app.post('/api/uploadfiles', upload.array('files'), (req, res, next) => {
        const files = req.files
        if (!files) {
            res.status(500).send({err:'error'})
        }
        files.forEach(f=>fs.chownSync(f.path, users.getUID(req.session.username), users.isiCellGID))
        res.send(files)
    })

    app.post('/api/runPython/:builderToken', async (req, res) => {
        lr.record(req, "explorer")
        if (!req.session.loggedin) {
            res.status(401).send({ message: "unauthorized action, please login" });
            return;
        }
        try {
            let builderToken = req.params.builderToken;
            if (builderToken.includes('/')) {
                res.status(500).send({ message: err });
                return;
            }
            let params = req.body;
            let pythonRun = spawn('python', [path.join(scriptsPath, 'paramatersExploration.py'), path.join(simulationsPath, builderToken), JSON.stringify(params)],{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)})

            res.writeHead(200, { 'Content-Type': 'application/json' });

            pythonRun.stdout.on('data', function (data) {
                dataStr = data.toString();
                res.write(dataStr); res.flush();
            });

            pythonRun.stderr.on('data', function (data) {
                dataStr = data.toString();
                res.write(JSON.stringify({ err: dataStr })); res.flush();
            });

            pythonRun.on('close', (code) => {
                console.log('exploreClose', code);
                res.end();
            });
            pythonRun.on('error', (err) => {
                console.log('exploreErr', err);
                res.end();
            });
            console.log("api : [ip:" + req.session.realip + "] new simulation launch with token :" + builderToken + "\t\t pid=" + pythonRun.pid);
            // return token
        } catch (err) {
            console.error(err);
            res.status(500).send({});
        }
    });

    app.get('/api/monitor', async (req, res) => {
        if (!req.session.loggedin) {
            res.status(401).send({ err: "unauthorized action, please login" });
            return;
        }
        Promise.all([si.currentLoad(),si.mem()]).then((values)=>{
            res.send({ cpu: values[0].currentLoad, totalMem: values[1].total / 1073741824, activeMem: values[1].active / 1073741824 });
        }).catch(()=>res.send({err:true}))/*
        exec("top - bn1 | awk '/^%Cpu/{print $2, $4, $6, $8, $10, $12, $14}/^MiB Mem/{print $4}/^MiB Swap/{print $9}'", (err, out) => {
            let [totalCPU,totalMem,availableMem] = out.split('\n')
            totalCPU = totalCPU.split(' ').map(str => Number(str))
            let idleCPU = totalCPU[3]
            totalCPU = totalCPU.reduce((a, b) => a + b, 0)
            res.send({ cpu: err ? -1 : totalCPU - idleCPU, totalMem: Number(totalMem) / 1024, availableMem: Number(availableMem) / 1024 });
        })*/

    })


    app.get('/api/statesnohup', async (req, res) => {
        if (!req.session.loggedin) {
            res.status(401).send({ err: "unauthorized action, please login" });
            return;
        }
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        if (!fs.existsSync(hidenDir)) {
            fs.mkdirSync(hidenDir, { recursive: true });
        }
        let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) { res.status(200).send({data:{ finish: [], interrupt: [], running: [] }}); return }
            let json = JSON.parse(data)
            let runningProcess = Object.values(json).filter((v)=>v.status==='running')
            let pidList = runningProcess.map((v) => v.pid)
            if(pidList.length>0) {
                exec("ps --no-heading -o pid "+pidList.join(' '),{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)},(err,out)=>{
                    if(!err){
                        let activeProcess = out.split('\n').map(v=>Number(v))
                        for (let entry of runningProcess){
                            if(!activeProcess.includes(entry.pid)) entry.status = 'finish'
                        }
                    } else {
                        for (let entry of runningProcess) {
                            entry.status = 'finish'
                        }
                    }
                    fs.writeFile(backgroundRunPath, JSON.stringify(json), () => { })
                    let ret = { finish: [], interrupt: [], running: [] }
                    Object.entries(json).forEach(([token, entry]) => ret[entry.status].push({ date: entry.date, fileRun: entry.fileRun, token: token }))
                    res.send({ data: ret })
                })
            } else {
                let ret = { finish: [], interrupt:[],running:[]}
                Object.entries(json).forEach(([token, entry]) => ret[entry.status].push({ date: entry.date, fileRun: entry.fileRun, token: token }))
                res.send({ data: ret })
            }

        })

    })


    app.post('/api/nohup/:builderToken', async (req, res) => {
        if (!req.session.loggedin) {
            res.status(401).send({ err: "unauthorized action, please login" });
            return;
        }
        let builderToken = req.params.builderToken
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        if (!fs.existsSync(hidenDir)) {
            fs.mkdirSync(hidenDir, { recursive: true });
        }
        let token = generateToken()
        exec(`cd ${path.join(userFolder, req.session.username)};nohup nice python3 -u ${req.body.path} ${path.join(simulationsPath, builderToken)} > ${path.join(hidenDir,token + ".log")} 2>&1 & echo $!`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)}, (err, out) => {
            //create hiden folder with information about running (pid, timestamp start)
            const entry = { date: Date.now(), pid: parseInt(out), logFile: token + '.log', fileRun: req.body.path.slice(req.body.path.lastIndexOf('/')+1), status: 'running' }
            let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
            fs.readFile(backgroundRunPath, function (err, data) {
                let json;
                if (err) json = {}
                else json = JSON.parse(data)
                json[token] = entry;
                fs.writeFile(backgroundRunPath, JSON.stringify(json),()=>{})
            })/*
            let killcmd = "{echo " + pid + "; ps -o pid --ppid " + pid + " --no-heading} | xargs kill -9"
            let killcmd = "{echo 182787; ps -o pid --ppid 182787 --no-heading} | xargs kill -9"
            let timeRuningInSecond = "ps -p " + pid + " -o etimes="*/
            res.send({ err, token });
        })
    })

    app.get('/api/killnohup/:token', async (req, res) => {
        let token = req.params.token
        let backgroundRunPath = path.join(userFolder, req.session.username, '.backgroundRun', "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) {
                res.send({ err })
                return
            }
            let json = JSON.parse(data)
            if (!json[token]) {
                res.send({ err: true })
                return
            }

            exec("{ echo " + json[token].pid + "; ps -o pid --ppid " + json[token].pid + " --no-heading; } | xargs kill -9",{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)}, (err) => {
                console.log(err)
                if (!err) {
                    json[token].status = 'interrupt'
                    fs.writeFile(backgroundRunPath, JSON.stringify(json),()=>{})
                    res.send({})
                } else {
                    res.send({ err })
                }
            })
        })
    })
    app.get('/api/removenohup/:token', async (req, res) => {
        let token = req.params.token
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) { res.send({ err });return}
            let json = JSON.parse(data)
            if (!json[token]) {
                res.send({ err: true })
                return
            }
            if(json[token].status !== 'running')
                fs.rm(path.join(hidenDir, token+'.log'), function (err) {
                    delete json[token]
                    fs.writeFile(backgroundRunPath, JSON.stringify(json), () => { })
                    res.send({})
                });
            else res.send({ err: true })
        })
    })

    app.get('/api/userFolder/download/:path/:name', async (req, res) => {
        const path_utf8 = Buffer.from(req.params.path,'base64').toString('utf8')
        console.log('download', path_utf8 + req.params.name)
        if (!req.session.loggedin) {
            res.status(401).send({ message: "unauthorized action, please login" });
            return;
        }
        let ownFolder = path.join(userFolder, req.session.username)
        const filePath = path.join(ownFolder, path_utf8, req.params.name)
        if(!isSubDir(ownFolder,filePath)){
            res.status(401).send({ message: "unauthorized action" });
            return;
        }
        if (!filePath.startsWith(ownFolder)) { res.status(500).send(''); return; }
        if (!fs.existsSync(filePath)) { res.status(500).send(''); return; }
        const st = fs.statSync(filePath)
        if (st.isFile()) {
            res.download(filePath)
        } else if(st.isDirectory()){
            zipDir(filePath, function (err, buffer) {
                console.log(buffer, err)
                if (err !== null)
                    res.status(500).send({ message: err });
                else {
                    res.setHeader('Content-disposition', 'attachment; filename=' + req.params.name + '.zip');
                    // res.setHeader('Content-type', buffer.mime_type);
                    res.type('bin');
                    res.send(buffer);
                }
            });
        }
    })

    app.post('/api/userFolder/:action', async (req, res) => {
        console.log(req.params.action)
        if (!req.session.loggedin) {
            res.status(401).send({ err: "unauthorized action, please login" });
            return;
        }
        console.log(userFolder, req.session.username)
        let ownFolder = path.join(userFolder, req.session.username)
        let reqPath;
        switch (req.params.action) {
            case 'delete':
                reqPath = path.join(ownFolder, req.body.path);
                if(!isSubDir(ownFolder,reqPath)){
                    res.send({ err: true })
                    return;
                }
                fs.rm(path.join(ownFolder, req.body.path), { recursive: true }, function (err) {
                    res.send({ err })
                });
                break;
            case 'newDir':
                reqPath = path.join(ownFolder, req.body.path);
                if(!isSubDir(ownFolder,reqPath)){
                    res.send({ err: true })
                    return;
                }
                exec(`mkdir '${reqPath}'`,{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                    if(err){
                        console.log("error")
                        res.send({ err })
                    } else {

                    }
                });
                break;
            case 'refresh':
                if (!fs.existsSync(ownFolder)){
                     fs.mkdirSync(ownFolder);
                     fs.chownSync(ownFolder, users.getUID(req.session.username), users.isiCellGID)
                }
                async function getFiles(cdir) {
                    const dirents = await fs.promises.readdir(path.join(ownFolder, cdir), { withFileTypes: true });
                    let files = await Promise.all(dirents.map((dirent) => {
                        if (dirent.name.startsWith('.')) return undefined
                        if (dirent.name === '__pycache__') return undefined
                        return dirent.isDirectory() ? getFiles(path.join(cdir, dirent.name)) : dirent.name;
                    }));
                    files = files.filter((f) => f !== undefined)
                    return { path: cdir, files };
                }
                getFiles('.').then((files) => {
                    res.send({ files })
                })
                break;
            case 'write':
                reqPath = path.join(ownFolder, req.body.path);
                if(!isSubDir(ownFolder,reqPath)){
                    res.send({ err: true })
                    return;
                }
                fs.writeFile(path.join(ownFolder, req.body.path), req.body.data, (err) => {
                    res.send({ err })
                    fs.chownSync(path.join(ownFolder, req.body.path), users.getUID(req.session.username), users.isiCellGID)
                });
                break;
            case 'rename':
                const dest = path.join(ownFolder, req.body.newPath)
                if (fs.existsSync(dest)) {
                    res.send({ err:'destination already exist' })
                } else {
                    exec("mv -n '" + path.join(ownFolder, req.body.path) + "' '" + dest +"'",{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                        if (fs.existsSync(dest)) {
                            console.log("no error")
                            res.send({})
                        } else {
                            console.log("error")
                            res.send({ err })
                        }
                    });
                }
                break;
            case 'copy':
                let destcp = path.join(ownFolder, req.body.newPath)
                var [p, n] = splitPath(destcp)
                while (fs.existsSync(destcp)) {
                    if(n.includes('.')){
                        const lastDotPos = n.lastIndexOf('.');
                        n = n.slice(0, lastDotPos)+' copy'+n.slice(lastDotPos)
                    } else {
                        n += ' copy'
                    }
                    destcp = p+n
                }
                exec("cp -r '" + path.join(ownFolder, req.body.path) + "' '" + destcp + "'",{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                    if (fs.existsSync(destcp)) {
                        console.log("no error")
                        res.send({})
                    } else {
                        console.log("error")
                        res.send({ err })
                    }
                });
                break;
            case 'move':
                let destmv = path.join(ownFolder, req.body.newPath)
                var [p, n] = splitPath(destmv)
                while (fs.existsSync(destmv)) {
                    if (n.includes('.')) {
                        const lastDotPos = n.lastIndexOf('.');
                        n = n.slice(0, lastDotPos) + ' copy' + n.slice(lastDotPos)
                    } else {
                        n += ' copy'
                    }
                    destmv = p + n
                }
                exec(`mv -n '${path.join(ownFolder, req.body.path)}' '${destmv}'`,{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                    if (fs.existsSync(destmv)) {
                        console.log("no error")
                        res.send({})
                    } else {
                        console.log("error")
                        res.send({ err })
                    }
                });
                break;
            case 'read':
                reqPath = path.join(ownFolder, req.body.path);
                if(!isSubDir(ownFolder,reqPath)){
                    res.send({ err: true })
                    return;
                }
                if ((/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i).test(req.body.path)) {
                    const ext = req.body.path.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)[1].toLowerCase() 
                    fs.readFile(reqPath, 'base64', (err, data) => res.send({ err, isImg:true, data: 'data:image/' + ext +';base64,'+data }))
                } else {
                    fs.readFile(reqPath, 'utf8', (err, data) => {
                        res.send({ err, data })
                    });
                }
                break;
            default:
                console.log('error', '/api/userFolder/' + req.params.action)
                res.send({ err: true })
        }
    });
}