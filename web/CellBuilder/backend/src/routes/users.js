const path = require('path');
const { v4: uuidv4 } = require('uuid');

module.exports = function(app,lr,users){

    app.post('/auth', function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        console.log('/auth ' + username)
        if (username && password) {
            if (users.isValidLogin(username, password)) {
                req.session.loggedin = true;
                req.session.username = username;
                req.session.ip = req.session.realip
                res.send({ username: req.session.username, logged: req.session.loggedin });
                return;
            }
        }
        res.send({ error: true });
    });

    app.get("/setPwd/:pwd/:name", function (req, res) {
        var pwd = req.params.pwd;
        var username = req.params.name;
        console.log('newUser', username, pwd);
        if (users.isValidLoginNewUser(username, pwd)) {
            const nonce = uuidv4(); // Génère un nonce aléatoire
            // Ajoutez le nonce à l'en-tête CSP
            res.setHeader('Content-Security-Policy', `script-src 'self' 'nonce-${nonce}'`);          
            res.render(path.join(__dirname,'..', 'views', 'newUser.html'), { nonce });
        } else {
            res.redirect('/');
        }
    })

    app.post("/setPwd", function (req, res) {
        if (req.session.loggedin && !req.body.lastPwd) {
            users.setPwd(req.session.username, req.body.newPwd, 'active');
            res.send({ error: false });
        } else if (users.isValidLoginNewUser(req.body.username, req.body.lastPwd)) {
            users.setPwd(req.body.username, req.body.newPwd, 'active');
            req.session.loggedin = true;
            req.session.username = req.body.username;
            res.redirect('/app.html')
        } else res.redirect('/')
    })

    app.get("/dashboard", function (req, res) {
        lr.record(req, "dashboard")
        let ok = false;
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                ok = true;
                const nonce = uuidv4(); // Génère un nonce aléatoire
                // Ajoutez le nonce à l'en-tête CSP
                res.setHeader('Content-Security-Policy', `script-src 'self' 'nonce-${nonce}'`);
                res.render(path.join(__dirname,'..', 'views', 'dashBoard.html'), { nonce });
            }
        }
        if (!ok) res.redirect('/')
    })


    app.get("/api/user/newUser/:name", function (req, res) {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'url': 'setPwd/' + users.newUser(req.params.name) + '/' + req.params.name }));
            }
        }
    })
    app.get("/api/user/resetPwd/:name", function (req, res) {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'url': 'setPwd/' + users.resetPassword(req.params.name) + '/' + req.params.name }));
            }
        }
    })



    app.get("/api/usersInfo", function (req, res) {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify(users.getInfo()));
            }
        }
    })

    app.get("/api/getLog", function (req, res) {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'data': lr.getData() }));
            }
        }
    })

    app.get("/api/user/delete/:name", function (req, res) {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                console.log('deleteUser', req.params.name)
                users.deleteUser(req.params.name)
                res.end()
            }
        }
    })

    app.get("/disconnect", function (req, res) {
        req.session.destroy();
        res.send({ ok: true });
        //res.redirect("/login")
    })
}