const { simulationsPath, isicellPath, demosPath, scriptsPath, userFolder } = require("../service/isiCellPath");
const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const zipDir = require('zip-dir');
function generateToken() {
    let dt = new Date();
    return 'sim_'.concat(dt.getFullYear(), (dt.getMonth() + 1), dt.getDate(), "_", dt.getHours(), dt.getMinutes(), '_', Math.floor(Math.random() * 1000));
}

demosToken = []
module.exports = function(app,lr,users,dbInfo){

    if (process.env.NODE_ENV !== 'dev')
        Promise.all(fs.readdirSync(demosPath).map(file => new Promise((resolve, reject) => {
            let finalJson = JSON.parse(fs.readFileSync(path.join(demosPath, file))).finalJson
            let cellJson = Buffer.from(JSON.stringify(finalJson, 'utf8')).toString('base64')
            const token = file.split('.')[0] 
            demosToken.push(token)
            exec(`cd ${simulationsPath} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${token} sql "${cellJson}"`,{uid:users.guestUID,gid:users.isiCellGID,env:users.getEnv('guest')} ,(error, stdout, stderr) => {
                if (error) {
                    console.warn(error);
                }
                resolve(stdout ? stdout : stderr);
            });
        }))).then(() => {
            console.log('demos compilation finish',demosToken)
        })


    app.post('/api/compile', async (req, res) => {
        if (!req.session.loggedin) {
            res.status(401).send({ error: "unauthorized action, please login",
                                   token: req.body.token });
            return;
        }
        lr.record(req, "compileSimu")
        users.using(req.session.username, 'builder')
        console.log("Compiling")
        let config = "";
        let error = undefined
        let token = undefined
        let type = undefined
        try {
            config = req.body.data;
            // run scripts
            token = req.body.token;
            if(req.body.type==='debug') type='debug'
            else type = req.body.type==='sql'?'sql':(req.body.type==='python'?'python':'all');
            if (token.includes('/')) {
                res.status(500).send({ message: err });
                return;
            }
            console.log('currentToken', token)
            if (!token || !fs.existsSync(path.join(simulationsPath, token)) || demosToken.includes(token)) {
                token = generateToken();
                console.log('new token', token)
            }
            cellJson = Buffer.from(JSON.stringify(config, 'utf8')).toString('base64')
            exec(`cd ${path.join(userFolder,req.session.username)} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${token} ${type} "${cellJson}"`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username,true)}, (err, stdout, stderr) => {
                if(type !== 'debug'){
                    console.log(err ? "Error" : "Done", token)
                    res.send(JSON.stringify({
                        error: err ? stderr.replaceAll(path.join(simulationsPath, token),'') : undefined,
                        token: token
                    }, 'utf8'));
                } else {
                    console.log(err ? "Error" : "Done", token)
                    if(err){
                        res.send(JSON.stringify({
                            error: stderr,
                            token: token
                        }, 'utf8'));
                    } else {
                        let params = { main: { ...dbInfo } };
                        params.input = req.body.params;
                        params.main.id = 'debug';
                        let currentParamsPath = path.join(simulationsPath, token, 'config/runsParams/debug.json')
                        fs.writeFileSync(currentParamsPath, JSON.stringify(params, 'utf8', 2));

                        exec(`cd ${path.join(simulationsPath, token,'build')}; script -eqc 'gdb -ex "set pagination off" -ex "set confirm off" -ex "run ../config/runsParams/debug.json" -ex "bt" -ex "quit" --args ./console' .typescript; rm .typescript`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username,true)}, (errGDB, stdoutGDB, stderrGDB) => {
                            res.send(JSON.stringify({
                                error: stdout.replaceAll(path.join(simulationsPath, token),''),
                                gdb: (stdoutGDB+'\n\n'+stderrGDB).replaceAll(path.join(simulationsPath, token),''),
                                token: token
                            }, 'utf8'));
                        })
                    }
                }
            })
        }
        catch (err) {
            console.log("Error", token)
            console.log(err)
            res.send(JSON.stringify({
                error: err,
                token: token
            }, 'utf8'));
        }
    });


    app.post('/api/generateDemo', async (req, res) => {
        if (req.session.loggedin) {
            if (req.session.username === 'admin') {
                console.log("api : [ip:" + req.session.realip + "] generate demo " + req.body.name);
                let name = req.body.name.replace(' ', '_').replace('.', '_')
                demosToken.push(name)
                let jsonDemoPath = path.join(demosPath, req.body.name + '.json')
                fs.writeFileSync(jsonDemoPath, JSON.stringify({ description: req.body.description, xml: req.body.xml, finalJson: req.body.finalJson, svg: req.body.svg, D3: req.body.D3 }, 'utf8'));
                fs.chownSync(jsonDemoPath, users.guestUID, users.isiCellGID)
                let cellJson = Buffer.from(JSON.stringify(req.body.finalJson, 'utf8')).toString('base64')
                
                exec(`cd ${simulationsPath} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${name} sql "${cellJson}"`,{uid:users.guestUID,gid:users.isiCellGID,env:users.getEnv('guest')} ,(error, stdout, stderr) => {
                    if (error) {
                        console.error(error)
                    }
                });
            }
        }
        res.end()
    });

    app.get('/api/checksum/:token', async (req, res) => {
        let token = req.params.token
        let folderSim = path.join(simulationsPath, token)
        if (!fs.existsSync(folderSim)) { res.send({err:'token not exist'}); return; }
        fs.readFile(path.join(folderSim, 'checksum.txt'), 'utf8', function (err, data) {
            if (err) { res.send({ err:'checksum file not exist' }); return;}
            res.send({checksum:data});
        })
    })


    app.get('/api/download/:token', async (req, res) => {
      if (!req.session.loggedin) {
        res.status(401).send({ message: "unauthorized action, please login" });
        return;
      }
      var token = req.params.token
      if (token.includes('/')) {
        res.status(500).send({ message: 'no permission' });
        return;
      }
      lr.record(req,"downloadProject")
      console.log("Downloading project " + token)
      // var error = undefined
      
      if(token !== undefined){
        simPath = 'simulations/';
        // try{
          zipDir(path.join(isicellPath,simPath,token), { filter: (path, stat) => !/(.*?\/build\/.*)/.test(path) },  function (err, buffer) {
            console.log(buffer, err)
            if(err !== null)
              res.status(500).send({message: err});
            else {
              res.setHeader('Content-disposition', 'attachment; filename='+token+'.zip');
              // res.setHeader('Content-type', buffer.mime_type);
              res.type('bin');
              res.send(buffer);
            }
          });
      }
      else{
        res.send(JSON.stringify({error: 'token undefined'},'utf8'))
      }
      console.log("DONE");
    });
}