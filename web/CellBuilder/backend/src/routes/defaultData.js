const { demosPath, defaultJsonPath, officialModulesJsonPath, cellModulesPath } = require("../service/isiCellPath");
const path = require('path');
const fs = require('fs');

module.exports = function(app,lr,users){
    app.get("/api/getDemo/:name", async (req, res) => {
        lr.record(req, "getDemo")
        let name = req.params.name
        if (name.includes('/')) {
            res.status(500).send({ message: err });
            return;
        }
        name += '.json'
        try {
            let xml = JSON.parse(fs.readFileSync(path.join(demosPath, name))).xml;
            res.send({ xml })
        } catch (e) {
            console.error(e)
        }
    })

    app.get('/api/defaultJson', async (req, res) => {
        lr.record(req, "defaultJson")
        let frontData;
        let error;

        try {
            frontData = {}
            frontData['User'] = { username: req.session.username, logged: req.session.loggedin }
            frontData['Demos'] = fs.readdirSync(demosPath).map(f => {
                let json = JSON.parse(fs.readFileSync(path.join(demosPath, f)))
                return { name: f.split('.')[0], svg: json.svg, D3: json.D3, description: json.description }
            })
            frontData['Default'] = JSON.parse(fs.readFileSync(defaultJsonPath));
            officialModulesJson = JSON.parse(fs.readFileSync(officialModulesJsonPath));

            const moduleKeysToKeep = ['Name', 'Description', 'Type', 'Dependencies', 'DirName'];
            frontData['Modules'] = officialModulesJson['Modules'].map(m =>
                Object.assign({},
                    JSON.parse(fs.readFileSync(path.join(cellModulesPath, m['DirName'], 'config.json'))),
                    moduleKeysToKeep.reduce((acc, curr) => { if (curr in m) acc[curr] = m[curr]; return acc }, {})
                )
            )
        } catch (err) {
            error = err
        }
        res.send(JSON.stringify({
            info: "Getting the front.json (return it directly as script output?)",
            error: error,
            defaultJson: frontData
        }));
    });

    app.get('/api/module/:dirName', async (req, res) => {
        lr.record(req, "getModule")
        let dirName = req.params.dirName
        let pathModule = path.join(cellModulesPath, dirName)
        if (dirName.includes('/')) {
            res.status(500).send({ message: err });
            return;
        }
        let files = {}
        try {
            fs.readdirSync(pathModule).forEach(file => {
                if (['h', 'cpp', 'hpp'].indexOf(file.split('.').pop()) !== -1) {
                    files[file] = fs.readFileSync(path.join(pathModule, file)).toString()
                }
            })
            res.send(JSON.stringify(files));
        } catch (err) {
            console.error(err)
            res.status(500).send({ message: err });
        }
    });
}