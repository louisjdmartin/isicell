import warnings
warnings.simplefilter("ignore", UserWarning)
import sys
import os
import base64
from io import BytesIO
from io import StringIO
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import json
import random
import multiprocessing as mp
from SALib.sample import latin
from json_flatten import unflatten
from jsonmerge import merge
import seaborn as sns
import pandas as pd
from copy import deepcopy
sys.path.append(sys.argv[1]) #path with token
from build.swig.isiCellPy import *


def getExplorationParameterJson(params):
    rangeParams = params['exploration']['range']
    nb = params['exploration']['nb']
    if len(rangeParams) > 1:
        space = {'num_vars': len(rangeParams), 'names':list(rangeParams.keys()),'bounds': list(rangeParams.values())}
        return [deepcopy({'input':merge(params['input'],unflatten(dict(zip(rangeParams.keys(),p)))),'index':i}) for i,p in enumerate(latin.sample(space, nb))]
    else:
        return [deepcopy({'input':merge(params['input'],unflatten({list(rangeParams.keys())[0]:p})),'index':i})  for i,p in enumerate(np.linspace(*list(rangeParams.values())[0],nb))]

jsonData = json.loads(sys.argv[2])

exec(jsonData['exploration']['functionPYTHON'],globals())

if 'range' in jsonData['exploration']:
    params = getExplorationParameterJson(jsonData)
else:
    params = [{'input':jsonData['input'],'index':0}]


def plotComplet(params):
    return { 'input': params['input'], 'image': export_image(plot(params)), 'index': params['index'] }

def generate(allParams,plot):
    with mp.Pool(mp.cpu_count()) as p:
        iterator = p.imap_unordered(plotComplet,allParams)
        for res in iterator:
            yield json.dumps(res)

def export_image(fig):
    img = BytesIO()
    fig.tight_layout()
    fig.savefig(img, format='png')
    plt.close()
    plt.figure().clear()
    img.seek(0)
    return base64.b64encode(img.getvalue()).decode('ascii')

for res in generate(params,plot):
    print(res)