import sys
import os
import pprint
from .communication import sendOutput
from .matplotlib_backend import show

pp = pprint.PrettyPrinter(indent=2, width=100, compact=False)

def importISiCell(interpreter,path):
    sys.path.append(path)
    exec("from build.swig.isiCellPy import Simu",interpreter.scope)
    interpreter.scope['display'] = display

def setWorkingDir(path):
    sys.path.append(path)
    os.chdir(path)

def display(obj,max_rows=20, max_cols=10):
    """Display a Python object in all frontends.\n
    By default all representations will be computed and sent to the frontends.\n
    **max_rows** and **max_cols** are only used to display DataFrame"""
    if obj.__class__.__name__ == 'DataFrame':
        sendOutput("display_data","text/html",[obj.to_html(max_rows=max_rows, max_cols=max_cols)])
    elif isAnImage(obj):
        show()
    else:
        pp.pprint(obj)

def displayHook(ob):
    if obj.__class__.__name__ == 'DataFrame':
        sendOutput("execute_result","text/html",[obj.to_html(max_rows=max_rows, max_cols=max_cols)])
    elif isAnImage(obj):
        show()
    else:
        sendOutput("execute_result","text/plain",[pp.pformat(obj)])

def isAnImage(obj):
    try:
        from seaborn.axisgrid import _BaseGrid
        if isinstance(obj,_BaseGrid): return True
        if len(obj)>0:
            if isinstance(obj[0],_BaseGrid):
                return True
    except: pass
    try:
        from matplotlib.artist import Artist
        if isinstance(obj,Artist): return True
        if len(obj)>0:
            if isinstance(obj[0],Artist):
                return True
    except: pass
    return False