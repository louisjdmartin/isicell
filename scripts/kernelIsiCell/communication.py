import json
import sys
import re
from .completion_manager import getCompletion

token = {}


def sendOutput(outputType,dataType,data):
    sys.stdout.flush()
    response = {"output_type": outputType, "data":{}}
    response['data'][dataType] = data
    print(token['block']+json.dumps(response)+token['block'],flush=True,end='')

def sendCompletion(scopes):
    sys.stdout.flush()
    print(token['block']+json.dumps(getCompletion(scopes))+token['block'],flush=True,end='')


def sendError(e,tb):
    print(token['block']+json.dumps({"ename": e.__class__.__name__,"evalue": str(e),"output_type": "error","traceback":tb})+token['block'],flush=True,end='')

def sendEndCellExcution():
    sys.stdout.flush()
    print(token['command'],flush=True,end='')
    
def listenInput(interpreter):
    data =""
    for line in sys.stdin:
        if token['command'] in line:
            line,numCell = line.split(token['command'])
            data+=line
            interpreter.run(data[:-1])
            data =""
        elif token['stop'] in line:
            interpreter.kill()
            print(token['stop'],flush=True,end='')
            data=""
        else:
            data+=line