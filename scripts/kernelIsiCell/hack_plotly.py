import os
import sys
from .communication import sendOutput
os.environ["PLOTLY_RENDERER"] = "plotly_mimetype"

class IPython():
    @staticmethod
    def display(*objs,include=None,exclude=None,metadata=None,transient=None,display_id=None,raw=False,clear=False,**kwargs):
        data = objs[0]
        for data in objs:
            for k,v in data.items():
                sendOutput("data_display",k,[v])


def load_fake_ipython():
    ip = IPython()
    sys.modules['IPython.display'] = ip
    #sys.modules['IPython'] = ip
