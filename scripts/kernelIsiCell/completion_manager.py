import inspect
import re

def getType(v):
    t = type(v)
    n = t.__name__
    return t if inspect.isclass(t) and n != 'type' and n != 'method' and n != 'function' and n != 'module' else v

def getKind(v):
    if isinstance(v, str): return 18
    if inspect.ismodule(v): return 8
    if inspect.isclass(v): return 5
    if inspect.ismethod(v): return 0
    if inspect.isfunction(v): return 1
    return 4 #variable

def getDoc(name, variable):
    if not name.startswith('_'):
        signatureShort = ''
        signatureLong = ''
        doc = ''
        sig = None
        retType = None
        try:
            try:
                sig = inspect.signature(getType(variable))
                retType = sig.return_annotation
                if isinstance(retType, str):
                    retType = retType if retType != 'None' else None
                else:
                    retType = retType.__name__
                    retType = retType if retType != '_empty' else None
            except: pass
            if callable(variable):
                signatureShort = '()'
                try:
                    signatureLong = str(sig)
                except: 
                    signatureLong = '()'
        except: pass
        try: 
            doc = re.sub(r'(>>>[\s\S]*?(?=\n\n|\Z))',r'```python\n\1\n```',inspect.cleandoc(inspect.getdoc(variable)))
        except: pass
        return {'label': name,
                'insertText': name+signatureShort,
                'return_type':getType(variable).__name__ if (retType =='_empty') or (retType is None) else retType,
                'documentation': {'value':'```python\n'+name+signatureLong+'\n```\n\n___\n\n'+doc,'isTrusted': True},
                'kind':getKind(variable) }
    return False

alreadySendType = []

def getCompletion(scopes):
    newType = {}
    variables = {}
    for name,variable in scopes.items():
    # for name in scopes.updated:
    #     variable = scopes[name]
        if name.startswith('_'): continue
        if inspect.isfunction(variable):
            variables[name] = getDoc(name,variable)
            continue
        vType = getType(variable).__name__
        variables[name] = vType
        if vType in alreadySendType: continue
        comp = getDoc(vType, variable)
        if(comp): 
            newType[vType]={'current':comp, 'members': []}
            for k in inspect.getmembers(variable):
                comp = getDoc(k[0], k[1])
                if(comp): newType[vType]['members'].append(comp)
        alreadySendType.append(vType)
    # scopes.reset_tracker()
    return {'completion':{'variables':variables,'newType':newType}}