from io import StringIO
from contextlib import redirect_stdout
import re,sys

class Isigen:

	def __init__(self, _dataTemplate):
		self.__dataTemplate = _dataTemplate
		self.__keepIsigen = True

	def templateFile(self,filePath,keepIsigen=True):
		self.__keepIsigen=keepIsigen
		with open(filePath, "r+") as f :
			fileContent = f.read()
			f.seek(0)

			try:
				regex = r"(?P<insertTabs>[\t ]*?)(?P<pre>/\*isigen:)(?P<mode>\w*)(?:@(?P<name>\w+))?(?:'{3}\s*)(?P<isicodeTabs>\n[\t ]*)(?P<isicode>[\w\W]*?)(?P<post>\s+?'{3}\*/)"
				fileContent = re.sub(regex, self.__replaceBlockMatch, fileContent)
			except Exception as ex:
				ex.message = 'in file "'+filePath+'" '+ex.message
				print('Error', ex.message, file=sys.stderr)
				raise

			f.write(fileContent)
			f.truncate()

	def __replaceBlockMatch(self,blockMatch):
		mode = blockMatch.group('mode')
		name = blockMatch.group('name')
		name = name if name else 'Undefined'
		isicodeTabs = blockMatch.group('isicodeTabs')
		isicode = blockMatch.group('isicode')
		insertTabs = '\n'+blockMatch.group('insertTabs')
		endStatement = '//isigen:end'

		ret = blockMatch.group(0) if self.__keepIsigen else ''
		try:
			if mode == 'eval':
				self.__getBlockOutput(isicode.replace(isicodeTabs, '\n'))
			else:
				ret += (self.__getBlockOutput(isicode.replace(isicodeTabs, '\n'))+endStatement).replace('\n', insertTabs)
		except Exception as ex:
			ex.message = 'on block "'+name+'"'
			raise

		return ret

	def __getBlockOutput(self,generationCode):
		f = StringIO()
		with redirect_stdout(f):
			exec(generationCode, self.__dataTemplate)
		ret = f.getvalue()
		return '\n' if ret.replace(r'\s','') == '' else '\n'+ret
