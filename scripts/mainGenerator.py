from Isigen import Isigen
import json
import os
import sys
import shutil
import base64
import re
import hashlib

from utils import *

def copytree(src,dst):
  if os.path.exists(dst):shutil.rmtree(dst)
  shutil.copytree(src, dst)

isicellPath = os.path.join(os.path.dirname(os.path.abspath(__file__)),'..')
scriptsPath = os.path.join(isicellPath,"scripts")

tokenParam = sys.argv[1]

isSWIG = (sys.argv[2] == 'python') | (sys.argv[2] == 'all')
isSQL = (sys.argv[2] == 'sql') | (sys.argv[2] == 'all') | (sys.argv[2] == 'debug')
isALL = (sys.argv[2] == 'all')
isDEBUG = (sys.argv[2] == 'debug')

jsonStr = base64.b64decode(str(sys.argv[3]).encode('ascii')).decode('utf-8')
md5Hash = hashlib.md5(jsonStr.encode('utf8')).hexdigest()

builderDataJson = json.loads(jsonStr)
simPath = os.path.join(isicellPath,"simulations/{}/".format(tokenParam))
if os.path.exists(simPath):shutil.rmtree(simPath)

isicellConfigPath = os.path.join(isicellPath,"config")
officialModulesPath = os.path.join(isicellConfigPath,"officialModules.json")

simConfigPath = os.path.join(simPath, "config")
cellModulesPath = "core/modules/cellModules"
srcPath = os.path.join(isicellPath,'core/src')

buildPath = simPath + "build"

#
# Json load
#
officialModulesJson = loadJson(officialModulesPath)

modulesConfigsJson = {}
for m in officialModulesJson["Modules"] :
  if (m["Name"] in builderDataJson['moduleSelected']) :
    jsonFile = open( os.path.join(isicellPath,cellModulesPath, m["DirName"], "config.json"), "r")
    configJson = json.load(jsonFile)
    modulesConfigsJson[m["DirName"]] = configJson
    jsonFile.close()

### Sim folder copy
copytree(srcPath, os.path.join(simPath,'src'))
os.makedirs(os.path.join(simConfigPath,'runsParams'))
copytree(os.path.join(isicellPath,'core/external/MecaCell'), os.path.join(simPath,'external/MecaCell'))
copytree(os.path.join(isicellPath,'core/modules/extraModules'), os.path.join(simPath,'modules/extraModules'))
if(isSWIG):
  copytree(os.path.join(isicellPath,'core/external/isicellpy/swig'), os.path.join(simPath,'swig'))

shutil.copy(os.path.join(isicellPath,'core/CMakeLists.txt'), simPath)

### modules copy
for moduleName in modulesConfigsJson.keys():
  copytree(os.path.join(isicellPath,cellModulesPath,moduleName), os.path.join(simPath,cellModulesPath,moduleName))



### custom modules
for moduleName,files in builderDataJson['customModule'].items():
  os.makedirs(os.path.join(simPath,cellModulesPath,moduleName))
  modulesConfigsJson[moduleName] = {}
  for fileName,info in files.items():
    namespace = re.search(r"\s*namespace\s+(\w+)\s*\{",info['code'])
    if(namespace):modulesConfigsJson[moduleName]["Namespace"] = namespace.group(1)
    if(info['type']=='Body'):modulesConfigsJson[moduleName]["Body"] = {"name":fileName.split('.')[0],"cellIncludes":[] }
    if(info['type']=='Plugin'):modulesConfigsJson[moduleName]["Plugin"] = {"className":fileName.split('.')[0]}
    f = open(os.path.join(simPath,cellModulesPath,moduleName,fileName), "w")
    f.write(info['code'])
    f.close()

### src generation
dataTemplate = {}
dataTemplate['builderData'] = builderDataJson
dataTemplate['modulesConfigs'] = modulesConfigsJson
dataTemplate['cellModulesPath'] = cellModulesPath

# print(os.path.join(simPath, srcPath))
for root,_,files in os.walk(os.path.join(simPath, 'src')):
  for f in files:
    Isigen(dataTemplate).templateFile(os.path.join(root,f),keepIsigen=False)
# Scheme sql gen
if(isSQL):
  schemeSQLPath = "modules/extraModules/sql/include/pluginSQL/SchemeSQL.hpp"
  Isigen(dataTemplate).templateFile(os.path.join(simPath,schemeSQLPath),keepIsigen=False)

f = open(os.path.join(simPath,"checksum.txt"), "w")
f.write(md5Hash)
f.close()
#
# Compilation
#
exec(f"cd {simPath} && mkdir -p build && cd build && cmake -DSQL={'ON'if isSQL else 'OFF'} -DPYTHON={'ON'if isSWIG else 'OFF'} -DDEBUG={'ON'if isDEBUG else 'OFF'} .. && make -j", 2)