import sys as _sys
import kernelIsiCell as _kisc



if __name__ == '__main__':
    _kisc.inter.setScope(globals())
    (_kisc.token['command'],
     _kisc.token['block']  ,
     _kisc.token['stop']    ) = _sys.argv[1:4]
    _kisc.load_backend()
    _kisc.importISiCell(_kisc.inter,_sys.argv[4])
    _kisc.setWorkingDir(_sys.argv[5])
    _kisc.load_fake_ipython()
    _kisc.listenInput(_kisc.inter)