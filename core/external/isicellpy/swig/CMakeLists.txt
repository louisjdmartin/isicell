# Set up swig
find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})

# Add subdirectories for each language if desired
option(BUILD_PYTHON "Build Python SWIG module" ON)
if(BUILD_PYTHON)
    # Include python
    find_package(PythonLibs REQUIRED)
    include_directories(${PYTHON_INCLUDE_PATH})


    set(CMAKE_SWIG_FLAGS "")
    set_source_files_properties(${PROJECT_NAME}.i PROPERTIES CPLUSPLUS ON)
    include_directories(..)

    # Add swig module
    swig_add_module(isiCellPy python ${PROJECT_NAME}.i)
    #swig_add_library(isiCellPy python ${PROJECT_NAME}.i)
    swig_link_libraries(isiCellPy ${PROJECT_NAME} ${PYTHON_LIBRARIES})

endif()
