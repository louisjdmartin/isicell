%module isiCellPy
// Add necessary symbols to generated header
%{
#include "src/cells/PrimoCell.hpp"
#include "src/simu.cpp"
%}

%pythonbegin %{
  import json
%}

%pythonprepend Simu::Simu(string,int) %{
  if isinstance(args[0], dict):
    args = list(args)
    args[0] = json.dumps(args[0])
%}

%include "std_string.i"
%include "std_vector.i"
%include "cpointer.i"
%include "exception.i"      
%allowexception; 
%exception {
  try {
    $action
  } catch(const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  } catch(...) {
    SWIG_exception(SWIG_UnknownError, "Unknown exception");
  }
}
%include "external/MecaCell/mecacell/geometry/basis.hpp"
%include "external/MecaCell/mecacell/geometry/rotation.h"
%include "external/MecaCell/mecacell/geometry/vector3D.h"
%inline %{
namespace MecaCell {
using Vec = Vector3D;
}
%}
%include "src/core/Model.hpp"
%include "src/cells/PrimoCell.hpp"
%template(Cell) PrimoCell<CellBody>;

namespace std {
   %template(V3Vector) vector<MecaCell::Vec>;
   %template(IntVector) vector<int>;
   %template(IntVectorVector) vector<vector<int>>;
   %template(DoubleVector) vector<double>;
   %template(DoubleVectorVector) vector<vector<double>>;
   %template(BoolVector) vector<bool>;
   %template(BoolVectorVector) vector<vector<bool>>;
   %template(StringVector) vector<string>;
   %template(StringVectorVector) vector<vector<string>>;
   %template(CellVector) vector<PrimoCell<CellBody>*>;
   %template(CellVectorVector) vector<vector<PrimoCell<CellBody>*>>;
}

// Process symbols in header
%include "src/simu.cpp"