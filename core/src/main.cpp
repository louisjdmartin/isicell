#include <mecacell/mecacell.h>
// #include <mecacellviewer/viewer.h>
#include "scenarios/Scenario.hpp"
#include "../modules/extraModules/sql/include/pluginSQL/PluginSQL.hpp"
#include <fstream>

using namespace std;

nlohmann::json loadJsonConfig(const string &inputFileName)
{
  //cout << "Reading Configuration File: " << inputFileName << endl;
  nlohmann::json config;
  ifstream i(inputFileName,ifstream::in);
  i >> config;
  i.close();
  return config;
}

int main(int argc, char *argv[]) {
  using scenario_t = Scenario;

  string configPath = argc > 1 ? argv[1] : "../config/viewer.json";
  nlohmann::json configuration = loadJsonConfig(configPath);

  scenario_t scenario;

  PluginSQL::PluginSQL<Cell> pluginSQL(configuration["main"], true);
  scenario.getWorld().registerPlugin(pluginSQL);

  scenario.init(configuration);

  while(!scenario.stop()){
    scenario.loop();
  }
  return 0;

  // MecacellViewer::Viewer<scenario_t> viewer(scenario);
  // return viewer.exec();
}