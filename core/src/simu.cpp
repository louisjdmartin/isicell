#include <mecacell/mecacell.h>
#include "scenarios/Scenario.hpp"
#include <fstream>
#include <numeric>
 
using namespace std;
using scenario_t = Scenario;

class Simu {
    private:
        scenario_t scenario;
    public:
        Simu(string params) { // Constructor with parameters
            nlohmann::json configuration;
            configuration = nlohmann::json::parse(params);
            scenario.init(configuration);
        }
        Simu(string params,int seed) { // Constructor with parameters
            nlohmann::json configuration;
            configuration = nlohmann::json::parse(params);
            MecaCell::Config::globalRand().seed(seed);
            scenario.init(configuration);

        }
        void update(){
            scenario.loop();
        }

        void update(int nbSteps){
            for(int i=0;i<nbSteps;i++)
                scenario.loop();
        }

        int getCurrentStep(){
            return scenario.getWorld().getNbUpdates();
        }


        vector<string> getStateNames(){
            /*isigen:insert@stateNames'''
            print("vector<string> stateNames({" + ','.join(sorted(['"'+s['name']+'"' for s in builderData["cell"]["States"]])) + "});\n")
            '''*/   
            return stateNames;
        }


        vector<string> getTypeNames(){
            /*isigen:insert@typeNames'''
            print("vector<string> typeNames({" + ','.join(sorted(['"'+s+'"' for s in builderData["cellTypes"]])) + "});\n")
            '''*/   
            return typeNames;
        }

        vector<int> getStates(){
            vector<int> ret;
            ret.resize(scenario.getWorld().cells.size());
            transform(scenario.getWorld().cells.begin(), scenario.getWorld().cells.end(), ret.begin(), [](auto* c){return static_cast<int>(c->getState());});
            return ret;
        }

        vector<int> getTypes(){
            vector<int> ret;
            ret.resize(scenario.getWorld().cells.size());
            transform(scenario.getWorld().cells.begin(), scenario.getWorld().cells.end(), ret.begin(), [](auto* c){return static_cast<int>(c->getType());});
            return ret;
        }
        
        vector<MecaCell::Vec> getPositions(){
            vector<MecaCell::Vec> ret;
            ret.resize(scenario.getWorld().cells.size());
            transform(scenario.getWorld().cells.begin(), scenario.getWorld().cells.end(), ret.begin(), [](auto* c){return c->getPosition();});
            return ret;
        }

        

        /*isigen:eval@SimuUtilityDeclarations'''
            from string import Template
            cellAttributeGettersTemplate = Template("""
            vector<$type> get$nameUpper(){
            \tvector<$type> ret;
            \tret.resize(scenario.getWorld().cells.size());
            \ttransform(scenario.getWorld().cells.begin(), scenario.getWorld().cells.end(), ret.begin(), [](auto* c){return $name;});
            \treturn ret;
            }
            """)
            attributesToIgnore = ['type', 'state']
            validType = ['bool','double','int','void']
            validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
            validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
            def checkValidType(type):
                return type.replace(' ','') in validType
            cellAttributeGetters = [a for a in builderData["cell"]["attributes"] if checkValidType(a['type']) and a['value'] not in attributesToIgnore]
            if "enums" in builderData:
                cellAttributeGettersEnum = [a for a in builderData["cell"]["attributes"] if a['type'] in builderData["enums"] and a['value'] not in attributesToIgnore]
            '''*/


        /*isigen:insert@simuGetters'''
            simuGetters = ''
            for cag in cellAttributeGetters:
                simuGetters += cellAttributeGettersTemplate.substitute({'name':'c->'+cag['value'], 'nameUpper': cag['value'][0].upper()+cag['value'][1:], 'type': cag['type']})
            print(simuGetters)
            '''*/

        
        /*isigen:insert@simuGettersEnum'''
            simuGetters = ''
            if "enums" in builderData:
                for cag in cellAttributeGettersEnum:
                    simuGetters += cellAttributeGettersTemplate.substitute({'name':'static_cast<int>(c->'+cag['value']+')', 'nameUpper': cag['value'][0].upper()+cag['value'][1:], 'type': 'int'})
            print(simuGetters)
            '''*/


        /*isigen:insert@simuGettersEnumName'''
            if "enums" in builderData:
                for name, listV in builderData["enums"].items():
                    print("vector<string> get"+name[0].upper()+name[1:]+"Names(){\n\tvector<string> enumNames({" + ','.join(sorted(['"'+n+'"' for n in listV])) + "});\n\treturn enumNames;\n}")
            '''*/


        /*isigen:insert@functionsScenarioWrapper'''
            import re
            scenarioFunWrapper = ''
            for fun in builderData["scenario"]["functions"]:
                res = re.search(r"(?P<ret>.+) +(?P<fun>.+) *\((?P<params>.*)\)",fun)
                if checkValidType(res.group('ret')):
                    scenarioFunWrapper+="{ret} {funName}({params}){{\n\t{content}\n}}\n".format(ret=res.group('ret'),funName=res.group('fun'),params=res.group('params'),content=('return 'if res.group('ret')!='void' else '')+'scenario.'+res.group('fun')+'('+ ','.join([ s.split(' ')[-1] for s in res.group('params').split(',')])+');')
            print(scenarioFunWrapper)
            '''*/

        vector<PrimoCell<CellBody>* > getCells(){
            return scenario.getWorld().cells;
        }
};