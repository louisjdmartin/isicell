//'''
#ifndef BASEPROJECT_BASESCENARIO_HPP
#define BASEPROJECT_BASESCENARIO_HPP

#include <mecacell/mecacell.h>
#include "../cells/PrimoCell.hpp"
#include "../core/CellBody.hpp"
#include "../tools/TimeConvert.hpp"
#include "../tools/RandomManager.hpp"


using Cell = PrimoCell<CellBody>;
using World = MecaCell::World<Cell>;
using Vec = MecaCell::Vec;

class Scenario {
private:

	World w;
	Model m;


public:

	double dt = 360.;
	int maxStep = 999999;

	/*isigen:insert@functions'''
		print('\n'.join(builderData["scenario"]["functions"]))
		'''*/


	/*isigen:insert@attributes'''
		print('\n'.join(["{type} {value};".format(type=a['type'],value=a['value']) for a in builderData["scenario"]["attributes"] if a['source'] != 'core']))
		'''*/

	World &getWorld(){
		return w;
	}

	void addCell(Cell *c) { w.addCell(c); }
	double getDt() const { return w.getDt(); }
	void setDt(double dt) { w.setDt(dt); }
	void setUpdateBehaviorPeriod(int n) { w.setUpdateBehaviorPeriod(n); }

	Cell * createCell(MecaCell::Vec pos, Type t){
		Cell * c = new Cell(pos, &m);
		c->setType(t);
		c->init();
		w.addCell(c);
		return c;
	}

	void init(nlohmann::json config){
		m.loadModel(config["input"]);
		m.initScenario(this);
		w.setDt(dt);

		/*isigen:insert@init'''
			print(builderData["scenario"]["codeInit"])
			'''*/
	}

	void loop(){
		w.update();
		/*isigen:insert@loop'''
			print(builderData["scenario"]["codeLoop"])
			'''*/
	}

	bool stop() { return w.getNbUpdates() >= maxStep; }
};

#endif //BASEPROJECT_BASESCENARIO_HPP
//'''