//'''
#ifndef _MODEL_HPP
#define _MODEL_HPP

#include <mecacell/mecacell.h>
#include <time.h>

/*isigen:insert@enumDeclarations'''
    print("enum class State { " + ','.join(sorted([s['name'] for s in builderData["cell"]["States"]])) + "};")
    print("enum class Type { " + (','.join(sorted(builderData["cellTypes"])) if len(builderData["cellTypes"])>0 else ' Common ') + "};\n")
    if "enums" in builderData:
        for name, listV in builderData["enums"].items():
            print("enum class "+name+" { " + ','.join(sorted(listV)) + "};")
    '''*/


struct Protocol {
            double defaultValue = 0;
            double currentValue;
            int currentStep = -1;
			int i = 0;
			std::vector<std::vector<double>> data;
            Protocol(){}
			Protocol(std::vector<std::vector<double>> _data):
			data(_data){
			}
			template <class World> double getValue(World  &w){
                if(currentStep==w.getNbUpdates()) return currentValue;
				double value = defaultValue;
                double currentTime =  w.getDt() * (double)w.getNbUpdates() / 3600.;
				if(i<data.size() && currentTime>=data[i][0]-MecaCell::Config::DOUBLE_EPSILON){
					value = data[i][1];
					i++;
				}
                currentStep = w.getNbUpdates();
                currentValue = value;
				return value;
			}
		};

class Model {
    
    private :
    nlohmann::json cellConfig;
    nlohmann::json scenarConfig;
    std::uniform_real_distribution<double> probaGen;

    string typeToString(Type type) {

        /*isigen:insert@typeToString'''
            if len(builderData["cellTypes"])>0:
                type2string = 'if(type == Type::{type}) return "{type}";\n'.format(type=builderData["cellTypes"][0])
                for ct in builderData["cellTypes"][1:]:
                    type2string += 'else if(type == Type::{type}) return "{type}";\n'.format(type=ct)
                print(type2string)
            '''*/

        return "Common";

    }


    double getParamNumber(string type, string entry){
        if(cellConfig[type].find(entry) != cellConfig[type].end()){
            return cellConfig[type][entry];
        } else if(cellConfig["Common"].find(entry) != cellConfig["Common"].end()){
            return cellConfig["Common"][entry];
        } else {
            return -1;
        }
    }

    bool getParamBoolean(string type, string entry){
        if(cellConfig[type].find(entry) != cellConfig[type].end()){
            return cellConfig[type][entry];
        } else if(cellConfig["Common"].find(entry) != cellConfig["Common"].end()){
            return cellConfig["Common"][entry];
        } else {
            return -1;
        }
    }

    public :

    Model():probaGen(0.0, 1.0) {}

    void loadModel(nlohmann::json config){
        cellConfig = config["Cell"];
        scenarConfig = config["Scenario"];
    }

    void initRandomSeed(int seed = clock()) {MecaCell::Config::globalRand().seed(seed);}

    double getProba(){
        return probaGen(MecaCell::Config::globalRand());
    }

    double getUniformDouble(double min, double max){
        std::uniform_real_distribution<double> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    double getUniformInt(int min, int max){
        std::uniform_int_distribution<int> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    double getNormalDouble(double mean, double var){
        std::normal_distribution<double> dist(mean, var);
        return dist(MecaCell::Config::globalRand());
    }

    double getExponentialDouble(double lambda){
        std::exponential_distribution<double> dist(lambda);
        return dist(MecaCell::Config::globalRand());
    }

    double getBoundedNormalDouble(double mean, double bound){
        std::normal_distribution<double> dist(mean, bound/3.);
        return std::min(dist(MecaCell::Config::globalRand()), bound);
    }

    double getLinearDouble(std::vector<std::pair<double,double>> values){
        std::vector<double> i,w;
        for (auto it = std::make_move_iterator(values.begin()),
            end = std::make_move_iterator(values.end()); it != end; ++it){
            i.push_back(std::move(it->first));
            w.push_back(std::move(it->second));
        }
        std::piecewise_linear_distribution<double> dist(i.begin(), i.end(), w.begin());
        return dist(MecaCell::Config::globalRand());
    }

    template <class Cell> void initCell(Cell* c){
        string typeStr;
        typeStr = typeToString(c->getType());
	
        /*isigen:insert@paramSettings'''
            print('\n'.join(['c->{attribute} = {func}(typeStr,"{attribute}");'.format(attribute=a['value'],func='getParamBoolean' if a['type']=='bool' else 'getParamNumber') for a in builderData["paramInputs"]["Cell"]]))
            '''*/
    }

	template <class Scenario> void initScenario(Scenario* s){
        /*isigen:insert@scenarioSettings'''
            print('\n'.join(['s->{attribute} = {funcStart}scenarConfig[\"{attribute}\"]{funcEnd};'.format(attribute=a['value'],funcStart='Protocol(' if a['type']=='Protocol' else '',funcEnd='.get<std::vector<std::vector<double>>>())' if a['type']=='Protocol' else '') for a in builderData["paramInputs"]["Scenario"]]))
            '''*/
    }

};

#endif
//'''
