//'''
#ifndef BASEPROJECT_CELLPLUGIN_HPP
#define BASEPROJECT_CELLPLUGIN_HPP

/*isigen:eval@lowerFirstLetter'''
	def lowerFirstLetter(s):
		return s[0].lower() + s[1:]
	'''*/

/*isigen:insert@includes'''
	print('\n'.join(["#include \"../../{modulesFolder}/{folderName}/{pluginFileName}.hpp\"".\
        format(modulesFolder=cellModulesPath,folderName=m,pluginFileName=configJson["Plugin"]["className"]) for m,configJson in modulesConfigs.items() if "Plugin" in configJson]))
 	'''*/

/*isigen:insert@namespaces'''
	print('\n'.join(["using namespace {namespace};".format(namespace=configJson["Namespace"]) for configJson in modulesConfigs.values() if "Namespace" in configJson]))
 	'''*/

template<typename cell_t>
struct CellPlugin {
	CellPlugin() = default;

	/*isigen:insert@modulesDefinition'''
		print('\n'.join(["\t{pluginClassName}<cell_t> {varName};".format(pluginClassName=configJson["Plugin"]["className"],varName=lowerFirstLetter(configJson["Plugin"]["className"])) for configJson in modulesConfigs.values() if "Plugin" in configJson]))
		'''*/

	template <typename world_t>
	void onRegister(world_t *w)
	{
		/*isigen:insert@modulesRegister'''
			print('\n'.join(["\t\tw->registerPlugin({varName});".format(varName=lowerFirstLetter(configJson["Plugin"]["className"])) for configJson in modulesConfigs.values() if "Plugin" in configJson]))
	 		'''*/
	}
};


#endif //BASEPROJECT_CELLPLUGIN_HPP
//'''