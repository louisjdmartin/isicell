//'''
#ifndef BASEPROJECT_Cell_HPP
#define BASEPROJECT_Cell_HPP

#include <mecacell/mecacell.h>
#include "../core/Model.hpp"
#include "../tools/TimeConvert.hpp"
#include "../tools/RandomManager.hpp"

/*isigen:eval@utilityDeclarations'''
	from string import Template
	from functools import cmp_to_key
	caseTemplate = Template("""
	case State::$name : {
	\t$code
	\tbreak;
	}""")
	def buildConditionsCode(destinations):
		destinations = sorted(destinations, key=cmp_to_key(lambda a, b: (a["condition"] == '')-(b["condition"] == '')))
		allCode = ''
		for i,d in enumerate(destinations):
			action = 'this->state = State::{state}'.format(state=d["to"]) if d['to'] != 'DeadCell' else 'this->die()'
			if (i == 0) & (d["condition"] != ''): code = 'if({condition}) {action};\n'
			elif (i == 0) & (d["condition"] == ''): code = '{action};\n'
			elif d["condition"] == '': code = 'else {action};\n'
			else: code = 'else if({condition}) {action};\n'
			allCode += code.format(condition=d["condition"],action=action);
		return allCode;
	'''*/

/*isigen:insert@includes'''
	includes = ''
	for m,configJson in modulesConfigs.items():
		if "Body" in configJson:
			includes += '\n'.join(["#include \"../../{cellModulesPath}/{moduleName}/{cellInclude}\""\
				.format(cellModulesPath=cellModulesPath,moduleName=m,cellInclude=ci) for ci in configJson["Body"]["cellIncludes"] ])+'\n'
	print(includes)
	'''*/

using Vec = MecaCell::Vec;

template<template<typename> class B>
class PrimoCell : public MecaCell::ConnectableCell<PrimoCell<B>, B> {
	using Base = MecaCell::ConnectableCell<PrimoCell<B>, B>;

private:

	Model * model;
	Type type;
	State state;


	template<typename world_t>
	void behave(world_t &w) {
		switch(this->state) {
			/*isigen:insert@behave'''
				behaviour = ''
				for s in builderData["cell"]["States"]:
					behaviour += caseTemplate.substitute({'name':s['name'], 'code': s['code'].replace('\n','\n\t')})
				print(behaviour)
				'''*/
		default:
		{
			cout << "UNSPECIFIED CELL STATE : " << static_cast<unsigned int>(this->state) << endl;
			break;
			}
		}
	}

	template<typename world_t>
	void nextState(world_t &w) {
		switch(this->state){
			/*isigen:insert@nextState'''
				transition = ''
				for t in builderData["cell"]["Transitions"]:
					transition += caseTemplate.substitute({'name': t['from'], 'code': buildConditionsCode(t['destinations']).replace('\n','\n\t')})
				print(transition)
				'''*/
		default:
		{
			//cout << "UNSPECIFIED CELL STATE : " << static_cast<unsigned int>(this->state) << endl;
			break;
			}
		}
	}

	PrimoCell * divideCell(){
		auto *daughter = new PrimoCell(this);
		return daughter;
	}

public:
	double minRadius = 10.;
	/*isigen:insert@attributes'''
		print('\n'.join(["\t{type} {value};".format(type=a['type'], value=a['value']) for a in builderData["cell"]["attributes"] if a['source'] != 'core']))
		'''*/


	/*isigen:insert@functions'''
		print('\n'.join(builderData["cell"]["functions"]))
		'''*/

	
	/*isigen:insert@cellModuleFunctions'''
		print('\n'.join([ '\n'.join([c['code'] for c in v['cellFunctions']]) for v in modulesConfigs.values() if 'cellFunctions' in v]))
		'''*/

	PrimoCell(const MecaCell::Vec &v, Model * m)
	:Base(v), model(m), type((Type)0)
	{
		this->getBody().setPosition(v);
	}

	PrimoCell(Model * m)
	:Base(MecaCell::Vec(0.,0.,0.)), model(m), type((Type)0)
	{
		this->getBody().setPosition(MecaCell::Vec(0.,0.,0.));
	}

	PrimoCell(PrimoCell<B> *mother)
	:Base(mother->getBody().getPosition() /*+ MecaCell::Vec::randomUnit() * minRadius*/), 
	model(mother->getModel()), type((Type)0)
	{
		this->getBody().setPosition(mother->getBody().getPosition() /*+ MecaCell::Vec::randomUnit() * minRadius*/);
	}

	void init() {
		model->initCell(this);
		/*isigen:insert@init'''
			initCode = builderData["cell"]["initializationCode"]
			if len(builderData["cell"]["initializationTransitions"])>0:
				destinations = builderData["cell"]["initializationTransitions"][0]["destinations"].copy()
				print(initCode+'\n'+buildConditionsCode(destinations))
			'''*/
	}

	Model * getModel() const { return model; }

	Type getType() const { return type; }

	void setType(Type t) { type = t; }

	State getState() { return state; } 

	void setState(State s) { state = s; } 
	
	double getRadius(){ return this->getBody().getBoundingBoxRadius(); }

	void setRadius(const double &_radius){ this->getBody().setRadius(_radius); }

	void setPosition(const MecaCell::Vec &_pos) { this->getBody().setPosition(_pos); }

	bool isType(Type t) {return this->type == t;}

	bool isInContactWith(Type t){ 
		for(auto *c : this->getConnectedCells())
			if(c->isType(t)) return true; 
		return false;
	}

	int countNeighborType(Type t){
		int count = 0;
		for(auto *c : this->getConnectedCells())
			if(c->isType(t)) count++;
		return count;
	}

	bool isInContact(){ return (this->getConnectedCells().size() > 0); }

	template<typename world_t>
	void updateBehavior(world_t &w){
		behave(w);
		nextState(w);
	}
};


#endif //BASEPROJECT_Cell_HPP
//'''
