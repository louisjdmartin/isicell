#ifndef RANDOMMANAGER
#define RANDOMMANAGER

#include <mecacell/mecacell.h>
#include <time.h>

namespace Rand{

    static std::uniform_real_distribution<double> probaGen(0.,1.);

    static inline void initRandomSeed(int seed = clock()) {MecaCell::Config::globalRand().seed(seed);}

    static inline double getProba(){return probaGen(MecaCell::Config::globalRand());}

    static inline double getUniformDouble(double min, double max){
        std::uniform_real_distribution<double> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    static inline int getUniformInt(int min, int max){
        std::uniform_int_distribution<int> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    static inline double getNormalDouble(double mean, double var){
        std::normal_distribution<double> dist(mean, var);
        return dist(MecaCell::Config::globalRand());
    }

    static inline double getExponentialDouble(double lambda){
        std::exponential_distribution<double> dist(lambda);
        return dist(MecaCell::Config::globalRand());
    }

    static inline double getBoundedNormalDouble(double mean, double bound){
        std::normal_distribution<double> dist(mean, bound/3.);
        return std::min(dist(MecaCell::Config::globalRand()), bound);
    }

    static inline double getLinearDouble(std::vector<std::pair<double,double>> values){
        std::vector<double> i,w;
        for (auto it = std::make_move_iterator(values.begin()),
            end = std::make_move_iterator(values.end()); it != end; ++it){
            i.push_back(std::move(it->first));
            w.push_back(std::move(it->second));
        }
        std::piecewise_linear_distribution<double> dist(i.begin(), i.end(), w.begin());
        return dist(MecaCell::Config::globalRand());
    }

    static inline double getLogNormalDouble(double mu, double sigma2){
        std::lognormal_distribution<double> dist(mu, sigma2);
        return dist(MecaCell::Config::globalRand());
    }

    static inline double getLogNormalDoubleMeanStd(double mean, double std){
        double sigma2 = log(1+(std * std)/(mean * mean));
        double mu = log(mean) - 0.5*sigma2;
        return getLogNormalDouble(mu, sigma2);
    }
}

#endif