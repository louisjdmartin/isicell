#ifndef TIMECONVERT
#define TIMECONVERT

namespace TimeConvert{

    static const double mnInS = 60.;
    static const double hInS = 3600.;
    static const double hInMn = 60.;

    static inline double sToMn(double s){ return s/mnInS; }
    static inline double sToH(double s){ return s/hInS; }
    static inline double mnToS(double mn){ return mn*mnInS; }
    static inline double mnToH(double mn){ return mn/hInMn; }
    static inline double hToS(double h){ return h*hInS; }
    static inline double hToMn(double h){ return h*hInMn; }
}

#endif